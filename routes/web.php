<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('test', 'JasperController@test');

Route::get('check', 'CheckController@index');
Route::get('send/email', 'MailController@sendemail');

Route::get('/register_view', 'Auth\RegisterController@register_view');
Auth::routes();
Auth::routes(['verify' => true]);

//For All Logged in Users
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('dashboard');
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::resource('settings', 'SettingController')->only(['index', 'store']);
    Route::resource('saleagent', 'SaleAgentController');
    Route::resource('admin', 'AdminController');
    Route::resource('client', 'ClientController');
    Route::get('edit-account', 'UserController@edit_account')->name('edit-account');
    Route::put('update-account', 'UserController@update_account')->name('update-account');
    Route::get('user/{id}/profile', 'UserController@show_profile')->name('profile.show');
    Route::get('accont/{id}', 'Homecontroller@account_detial')->name('account.show');
    Route::prefix('client')->group(function () {
        Route::get('get-usage/{id}', 'ClientController@get_usage');
    });


    Route::resource('devices', 'DeviceController');
    Route::resource('connections', 'ConnectionController');

    Route::prefix('connections')->group(function () {
        Route::get('{id}/add-deal-info', 'ConnectionController@deal_info')->name('connection.deal_info');
    });
    Route::get('device-connections/{slug}', 'DeviceController@connections')->name('device.connections');
    Route::put('add-connection/{id}', 'DeviceController@store_connection')->name('connection.store');
    Route::put('remove-connection/{id}', 'DeviceController@remove_connection')->name('connection.remove');


    Route::resource('packages', 'PackageController')->except(['show', 'destroy']);


//Admin only routes
    Route::group(['middleware' => ['role:admin|superuser', 'auth']], function () {
        Route::get('sync-devices', 'DeviceController@sync_devices')->name('sync');
        Route::get('sync-devices-ajax', 'DeviceController@sync_devices_ajax')->name('sync-ajax');

        Route::get('quote/{quote_id}/attach_connection', 'DealController@attach_connections')->name('attach.deal_conn');
        Route::post('quote/{quote_id}/attach_connection', 'DealController@store_attach_connections')->name('store.deal_conn');

        Route::get('bulk-deal-info/create', 'ConnectionController@deal_info_bulk')->name('deal_info_bulk');
        Route::get('ajax/client_deals/{client_id}', 'ConnectionController@client_deals');
        Route::put('add-bulk-deal-info', 'ConnectionController@store_deal_info_bulk')->name('store_deal_info');

        Route::get('admin-notifications', 'NotificationController@admin')->name('admin.notifications')->middleware('ajax');
        Route::get('admin-notification-read/{id}', 'NotificationController@admin_read')->middleware('ajax');
        Route::get('admin-notification-readall', 'NotificationController@admin_readall')->name('admin.readall')->middleware('ajax');
        Route::get('assign/sims', 'AssignSimController@create')->name('assign.sim.create');
        Route::post('assign/sims', 'AssignSimController@store')->name('assign.sim.store');
        Route::get('assign/sims/{id}/edit', 'AssignSimController@edit')->name('assign.sim.edit');
        Route::post('assign/sims/{id}/update', 'AssignSimController@update')->name('assign.sim.update');
        Route::get('assign/list', 'AssignSimController@index')->name('assign.sim.index');
        Route::get('deassign/{id}/sim', 'AssignSimController@deassign')->name('deassign.sim');
    });



    Route::get('connection-dayssearch/{iccid}/{start}/{end}', 'ConnectionController@days_search')->name('connection.daysearch');
    Route::get('connection-monthsearch/{iccid}/{start}/{end}', 'ConnectionController@month_search')->name('connection.monthsearch');


    Route::resource('quote', 'QuoteController');
    Route::prefix('quote/{quote_id}')->group(function () {
        Route::resource('deal', 'DealController');
        Route::get('/deal/individual/create', 'DealController@create')->name('deal.individual');

        Route::prefix('/deal/pooled')->group(function () {
            Route::get('/create', 'DealController@pooled')->name('deal.pooled');
            Route::post('/store', 'DealController@store_pooled')->name('store.pooled');
        });

        Route::get('send', 'QuoteController@send')->name('quote.send');
        Route::put('save', 'QuoteController@save_paymentplan')->name('save.paymentplan');
        Route::get('approve/', 'QuoteController@approve')->name('quote.approve');
    });
    Route::get('get-quote-price/{deal_type}/{product_type_id}/{data_usage}', 'QuoteController@get_quote_price');
    Route::get('get-volume/{deal_type}/{volume_id}', 'QuoteController@get_volume');
    Route::get('get-data-volume/{deal_type}/{type}', 'QuoteController@get_datavolume');

    Route::resource('wallet', 'WalletController')->except(['show', 'delete']);

    //Notifications
    Route::get('saleagent-notifications', 'NotificationController@saleagent')->name('saleagent.notifications')->middleware('ajax');
    Route::get('saleagent-notification-read/{id}', 'NotificationController@saleagent_read')->middleware('ajax');
    Route::get('saleagent-notification-readall', 'NotificationController@saleagent_readall')->name('saleagent.readall')->middleware('ajax');

    Route::resource('check', 'CheckController');
    Route::get('get-usage', 'ConnectionController@get_usage');

    Route::prefix('searchAll')->group(function () {
//        Route::get('/{start?}/{end?}', 'SearchController@index')->name('search.usage');
        Route::get('/day/{start?}/{end?}', 'SearchController@search_day')->name('searchall.day');
        Route::get('/month/{start?}/{end?}', 'SearchController@search_month')->name('searchall.month');
        Route::get('/top_metrics/{start?}/{end?}', 'SearchController@search_top_metrics')->name('searchall.topmetrics');
        Route::get('/connections', 'SearchController@search_connections')->name('searchall.connections');
        Route::get('/default_dates', 'SearchController@default_dates')->name('searchall.dates');
    });
});


