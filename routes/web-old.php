<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Auth::routes();
Auth::routes(['verify' => true]);


/*Route::get('connections', 'ConnectionsController@index')->name('connections');
Route::get('connections/{iccid}', 'ConnectionsController@view')->name('connection.details');*/

//Route::get('devices/{iccid}', 'ConnectionsController@device_view')->name('device.details');

Route::resource('devices', 'DeviceController');
Route::resource('connections', 'ConnectionController')->except(['edit', 'update']);

Route::prefix('connections')->group(function () {
    Route::get('{iccid}/update', 'ConnectionController@update')->name('connection.update');
});

Route::get('device-connections/{slug}', 'DeviceController@connections')->name('device.connections');
Route::put('add-connection/{id}', 'DeviceController@store_connection')->name('connection.store');
Route::put('remove-connection/{id}', 'DeviceController@remove_connection')->name('connection.remove');


Route::resource('packages', 'PackageController')->except(['show', 'destroy']);



Route::get('get-usage', 'ConnectionController@get_usage');

Route::resource('quote', 'QuoteController');
Route::prefix('quote/{quote_id}')->group(function () {
    Route::resource('deal', 'DealController');
});
Route::get('quote/send/{quote_id}', 'QuoteController@send');
Route::get('get-quote-price/{product_type_id}/{data_usage}', 'QuoteController@get_quote_price');

Route::resource('clients', 'UserController')->except(['edit', 'udpate'])->middleware('role:admin|superuser');

Route::get('get-data-volumn/{type}', 'QuoteController@get_datavolumn');

//Admin only routes
Route::group(['middleware' => ['role:admin', 'auth']], function () {
    Route::get('sync-devices', 'DeviceController@sync_devices')->name('sync');
    Route::get('sync-devices-ajax', 'DeviceController@sync_devices_ajax')->name('sync-ajax');
    Route::resource('settings', 'SettingController')->only(['index', 'store']);
});

//Client Only Routes
Route::group(['middleware' => ['role:client', 'auth']], function () {

});

//For All Logged in Users
Route::group(['middleware' => ['auth']], function () {

});


Route::get('get-volumn/{volumn_id}', 'QuoteController@get_volumn');



Route::resource('check', 'CheckController');









