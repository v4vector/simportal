jQuery(document).ready(function(){

       var elements = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

       elements.forEach(function(html) {
           var switchery = new Switchery(html, {color: '#D1E3F8', jackColor: '#4a90e2', size: 'small'});
       });
});