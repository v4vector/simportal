@extends('layouts.app')

@section('content')


    <section class="section print-page" id="print-page" style="background-color: white">

        <div class="container page">
            <header class="row header">
                <div class="col-sm-6">
                    <img src="{{ asset('assets/images/water-telecom-logo.png') }}" class="img-responsive logo"
                         alt="Logo">
                </div>
                <div class="col-sm-6 form-group gray text-right">
                    <div>Quote ID: #M-100{{ str_pad($quote->id, 4, '0', STR_PAD_LEFT) }}</div>
                    <div>{{ \Carbon\Carbon::now()->format('d M Y') }}</div>
                </div>
                <div class="col-sm-12">
                    <div class="gray header-heading">A proposal summary for:</div>
                    <div class="header-client">{{ $quote->client->company->name }}</div>
                    <img src="{{ asset('assets/images/icons/Proposal-Template-Line.png') }}" alt=""
                         class="img-responsive line">
                </div>
            </header>
            <div class="row">
                <div class="col-sm-12">
                    <img src="{{ asset('assets/images/Proposal-Template-Main-Image.png') }}" alt="Banner Image"
                         class="img-responsive banner">
                </div>
            </div>
        </div>

        <div class="container page">
            <div class="row header visible-print">
                <div class="col-sm-6">
                    <img src="{{ asset('assets/images/water-telecom-logo.png') }}" class="img-responsive logo"
                         alt="Logo">
                </div>
                <div class="col-sm-6 form-group gray text-right">
                    <div>Quote ID: #M-100{{ str_pad($quote->id, 4, '0', STR_PAD_LEFT) }}</div>
                    <div>{{ \Carbon\Carbon::now()->format('d M Y') }}</div>
                </div>
                <div class="col-sm-12">
                    <div class="gray header-heading">A proposal summary for:</div>
                    <div class="header-client">{{ $quote->client->company->name }}</div>
                    <img src="{{ asset('assets/images/icons/Proposal-Template-Line.png') }}" alt=""
                         class="img-responsive line">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h3>Exceptional service as standard</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h5 class="text-uppercase">THE NEW STANDARD IN MOBILE CONNNECTIVITY</h5>
                    <p>No matter how big your business, we have the technology and expertise to deliver a tailored solution
                        for your connectivity needs. Let us know your requirements and we'll take care of the rest.</p>
                    <h5 class="text-uppercase">Tailored solutions to fit your business</h5>
                    <p>Working with our global network of expert partners, we'll find the perfect connectivity and telecom
                        solutions for you. Flexibility on pricing, contract lengths and tariffs make our solutions truly
                        unique to your needs.</p>
                    <h5 class="text-uppercase">One bill, Once a month</h5>
                    <p>Know exactly what to expect from your bill, even before it has landed. With Water Telecom you have
                        instant access to your billing history, insight into data usage, current additional usage charges
                        and overviews of all the devices on your account.</p>
                    <h5 class="text-uppercase">Your account, at your fingertips</h5>
                    <p>Our online client portal gives you the ability to fully monitor your data usage and track spending, as well as activate and deactivate devices remotely. Our team of experts are on-hand working together to give you full support.</p>
                </div>
            </div>

            <div class="row form-group printable-contacts">
                <div class="col-sm-6">
                    <div class="contact">
                        <h4 class="position">CUSTOMER</h4>
                        <div class="name">{{ $quote->client->company->name }}</div>
                        <addr>{{ $quote->client->company->address }}</addr>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="contact">
                        <h4 class="position">PROVIDER</h4>
                        <div class="name">Water Telecom Ltd</div>
                        <addr>2nd Floor, Stanford Gate South Road<br> Brighton, BN316SB<br> United Kingdom</addr>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="contact">
                        <h4 class="position">KEY CONTACT</h4>
                        <div class="name">{{ $quote->client->full_name }}</div>
                        <addr>{{ $quote->client->email }}<br>{{ $quote->client->mobile }}</addr>
                    </div>
                </div>
                <div class="col-sm-6">
                    @php
                        $me = \Illuminate\Support\Facades\Auth::user();
                    @endphp
                    <div class="contact">
                        <h4 class="position">ACCOUNT MANAGER</h4>
                        <div class="name">{{ $me->full_name }}</div>

                        <addr>{{ $me->email }}<br>{{ $me->mobile }}</addr>
                    </div>
                </div>
            </div>


        </div>

        <div class="container page">
            <div class="row header visible-print">
                <div class="col-sm-6">
                    <img src="{{ asset('assets/images/water-telecom-logo.png') }}" class="img-responsive logo"
                         alt="Logo">
                </div>
                <div class="col-sm-6 form-group gray text-right">
                    <div>Quote ID: #M-100{{ str_pad($quote->id, 4, '0', STR_PAD_LEFT) }}</div>
                    <div>{{ \Carbon\Carbon::now()->format('d M Y') }}</div>
                </div>
                <div class="col-sm-12">
                    <div class="gray header-heading">A proposal summary for:</div>
                    <div class="header-client">{{ $quote->client->company->name }}</div>
                    <img src="{{ asset('assets/images/icons/Proposal-Template-Line.png') }}" alt=""
                         class="img-responsive line">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h3>Plan Details</h3>
                </div>
                @foreach($quote->deals as $dl)
                    <div class="col-sm-4">
                        <div><h4>Deal {{ $loop->iteration }}:</h4></div>
                        <div>Product Type: {{ $dl->product_type->name }}</div>
                        <div>Data Type: {{ $dl->data_type }}</div>
                        <div>Data Required: {{ $dl->volume->name }}</div>
                        <div>SIM Volume: {{ $dl->sim_volume }}</div>
                        <div>Contract: {{ $dl->contract_len }} Months</div>
                        <div>Payment Terms: {{ ucwords($dl->payment_terms) }}</div>
                        <hr>
                    </div>
                @endforeach
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <h3>SUMMARY</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Summary</th>
                                <th class="text-right">Contract Duration</th>
                                <th class="text-right">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $total = 0 @endphp
                            @foreach($quote->deals as $deal)
                                @php
                                    $Vat = $deal->vat;
                                    $with_Vat = $deal->total_deal;
                                    $ex_Vat = $with_Vat - $Vat;
                                    $total += $ex_Vat;
                                @endphp
                                <tr>
                                    <td>Deal {{ $loop->iteration }}</td>
                                    <td class="text-right">{{ $deal->contract_len }} Months</td>
                                    <td class="text-right bold">&pound;{{ number_format($ex_Vat, 2) }}</td>
                                </tr>

                            @endforeach
                            <tr>
                                <th colspan="5">Sims:</th>
                            </tr>
                            <tr>
                                <td>Sims Cost</td>
                                <td class="text-right">{{ $quote->deals->sum('sim_volume') }}x</td>
                                <td class="text-right bold">&pound;{{ number_format( $quote->deals->sum('cost_sims'), 2) }}</td>
                            </tr>
                            <tr class="bold text-right">
                                <td colspan="3">
                                    @php
                                        $vat = $quote->deals()->sum('vat') + $quote->deals->sum('vat_sims');
                                        $total += $quote->deals->sum('cost_sims');
                                    @endphp
                                    <div>Total Ex: &pound;{{ number_format($total, 2) }}</div>

                                    <div>VAT: &pound;{{ number_format($vat, 2) }}</div>
                                    <div>Total Inc: &pound;{{ number_format($total + $vat, 2) }}</div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <div class="container page">
            <div class="row header visible-print">
                <div class="col-sm-6">
                    <img src="{{ asset('assets/images/water-telecom-logo.png') }}" class="img-responsive logo"
                         alt="Logo">
                </div>
                <div class="col-sm-6 form-group gray text-right">
                    <div>Quote ID: #M-100{{ str_pad($quote->id, 4, '0', STR_PAD_LEFT) }}</div>
                    <div>{{ \Carbon\Carbon::now()->format('d M Y') }}</div>
                </div>
                <div class="col-sm-12">
                    <div class="gray header-heading">A proposal summary for:</div>
                    <div class="header-client">{{ $quote->client->company->name }}</div>
                    <img src="{{ asset('assets/images/icons/Proposal-Template-Line.png') }}" alt=""
                         class="img-responsive line">
                </div>
            </div>
            <div class="row">
                @php
                    $installs = new \Illuminate\Support\Collection();
                    if($quote->deals){
                    foreach($quote->deals as $deal){
                        if($deal->payment_terms == 'monthly'){
                            $con = $deal->contract_len - 3;

                            for($i=1; $i<=$con; $i++){
                                $arr = [
                                    'month' => 'Month '.$i,
                                    'install' => $deal->installment,
                                    'vat' => $deal->vat / $deal->contract_len,
                                ];
                                $installs->push($arr);
                            }
                        }
                    }
                    }
                    $months = $installs->groupBy(function ($r){
                        return $r['month'];
                    })->map(function ($group){
                        return ['install' => $group->sum('install'), 'vat' => $group->sum('vat')];
                    });
                    $groups = $months->groupBy(function ($mon){
                        return (string) $mon['install'];
                    });

                    $total = $total_ex = $total_vat = $initial_vat = $initial_exvat = 0;
                    foreach($quote->deals as $dl){
                        if($dl->payment_terms == 'monthly'){
                            $initial_vat += $dl->vat / ($dl->contract_len) * 3;
                            $initial_exvat += $dl->deal_amount / ($dl->contract_len) * 3;
                        } else {
                            $initial_vat += $dl->vat;
                            $initial_exvat += $dl->deal_amount;
                        }
                    }
                    //$initial_exvat = $quote->deals->sum('advance_payment') - $initial_vat;
                    $total_ex += $initial_exvat;
                    $total_vat += $initial_vat;
                    $total += $initial_total = $initial_vat + $initial_exvat;
                @endphp

                <div class="col-sm-12">
                    <h3>PAYMENT TERMS</h3>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th class="text-left">Payment Plan</th>
                                <th class="text-right">Ex VAT</th>
                                <th class="text-right">VAT</th>
                                <th class="text-right">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th colspan="5">Initial Payment:</th>
                            </tr>
                            <tr>
                                <td>Data Cost</td>
                                <td class="text-right">&pound;{{ number_format($initial_exvat, 2) }}</td>
                                <td class="text-right">&pound;{{ number_format($initial_vat, 2) }}</td>
                                <td class="text-right bold">&pound;{{ number_format($initial_total, 2) }}</td>
                            </tr>
                            <tr>
                                <td>Sims Cost</td>
                                <td class="text-right">
                                    &pound;{{ number_format($quote->deals->sum('cost_sims'), 2) }}</td>
                                <td class="text-right">
                                    &pound;{{ number_format($quote->deals->sum('vat_sims'), 2) }}</td>
                                <td class="text-right">
                                    <strong>&pound;{{ number_format($quote->deals->sum('total_sims_cost'), 2) }}</strong>
                                </td>
                            </tr>
                            @if(!$installs->isEmpty())
                                <tr>
                                    <th colspan="4">Monthly Installments:</th>
                                </tr>
                                @foreach($groups as $install => $group)
                                    @php
                                        $ex_vat = $group->first()['install'] - $group->first()['vat'];
                                        $vat = $group->first()['vat'];

                                        $total_ex += $ex_vat * $group->count();
                                        $total_vat += $vat * $group->count();
                                        $total += ($ex_vat + $vat) * $group->count();
                                    @endphp
                                    <tr>
                                        <td>{{ $group->count().' months at' }}</td>
                                        <td class="text-right">
                                            &pound;{{ number_format($ex_vat, 2) }}</td>
                                        <td class="text-right">&pound;{{ number_format($vat, 2) }}</td>
                                        <td class="text-right bold">&pound;{{ number_format(($ex_vat + $vat), 2) }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            {{--<tr class="text-right bold">
                                <td colspan="4">
                                    <div>Total Ex: &pound;{{ $total_ex }}</div>
                                    <div>Vat: &pound;{{ $total_vat }}</div>
                                    <div>Total Inc: &pound;{{ number_format($total, 2) }}</div>
                                </td>
                            </tr>--}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        {{--
        <div class="container page">
            <div class="row header visible-print">
                <div class="col-sm-6">
                    <img src="{{ asset('assets/images/water-telecom-logo.png') }}" class="img-responsive"
                         alt="Logo">
                </div>
                <div class="col-sm-6 form-group gray text-right">
                    <div>Quote ID: #M-1008867</div>
                    <div>30 Nov 2018</div>
                </div>
                <div class="col-sm-12">
                    <div class="gray header-heading">A proposal summary for:</div>
                    <div class="header-client">Aaqib Mehran</div>
                </div>
            </div>


        </div>
--}}

        <footer class="container footer">
            <div class="row">
                <div class="col-sm-4">Water Telecom Ltd</div>
                <div class="col-sm-4">support@watertelecom.com</div>
                <div class="col-sm-4">www.watertelecom.com</div>
            </div>
        </footer>

    </section>

    <div class="container hidden-print">
        <div class="row">
            <div class="col-sm-12">
                <button class="btn btn-primary btn-rounded" onclick="window.print()">Print <i class="fa fa-print"></i></button>
            </div>
        </div>
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
@endsection

@section('scripts')

@endsection