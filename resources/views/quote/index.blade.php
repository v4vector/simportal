@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection

@section('content')

    <section class="bg-section form-group">
        <div class="container">

            <div class="row overallStats">
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <a href="">
                            <div class="stat-value">{{ $quotes->count() }}</div>
                            <div class="stat-label">Total Quotes</div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <a href="">
                            <div class="stat-value">{{ $quotes->where('status', 'Draft')->count() }}</div>
                            <div class="stat-label">Draft</div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <a href="">
                            <div class="stat-value">{{ $quotes->where('status', 'Sent')->count() }}</div>
                            <div class="stat-label">Sent</div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <a href="">
                            <div class="stat-value">{{ $quotes->where('status', 'Approved')->count() }}</div>
                            <div class="stat-label">Approved</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if ($message = Session::get('success'))
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ $message }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-right">
                <a href="{{ route('quote.create') }}" class="btn btn-primary">Create New Quote</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <ul class="nav nav-pills">
                    <li class="active"><a data-toggle="pill" href="#all_quotes">Total Quotes</a></li>
                    <li><a data-toggle="pill" href="#draft_quotes">Draft</a></li>
                    <li><a data-toggle="pill" href="#sent_quotes">Sent</a></li>
                    <li><a data-toggle="pill" href="#approved_quotes">Approved</a></li>
                </ul>
            </div>
        </div>
        @php
            $draft_quotes = $quotes->filter(function ($q){
                                return $q->status == 'Draft';
                            });
            $sent_quotes = $quotes->filter(function ($q){
                                return $q->status == 'Sent';
                            });
            $approved_quotes = $quotes->filter(function ($q){
                            return $q->status == 'Approved';
                        });
        @endphp

        <div class="row">
            <div class="col-sm-12 bg-section">
                <div class="tab-content">
                    <div id="all_quotes" class="tab-pane fade in active">
                        @if(!$quotes->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-hover dataTable no-footer">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Client</th>
                                    <th>Order Number</th>
                                    <th>Status</th>
                                    @hasrole('admin')
                                    <th>Added By</th>
                                    @endhasrole
                                    <th>Added On</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($quotes as $quote)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            <a href="{{ route('client.show', $quote->client->id) }}">{{ $quote->client->company->name }}</a>
                                        </td>
                                        <td>{{ $quote->order_number }}</td>
                                        <td>{{ $quote->status }}</td>
                                        @hasrole('admin')
                                        <td>{{ $quote->user->full_name }}</td>
                                        @endhasrole
                                        <td>{{ $quote->created_at->format('d M, Y') }}</td>
                                        <td class="text-center">
                                            <ul class="list-inline">
                                                @hasanyrole('admin|saleagent')
                                                <li>
                                                    <a href="{{ route('deal.individual', $quote)  }}" data-toggle="tooltip"
                                                       title="Add Individual Deal"><i class="fa fa-plus-square"></i></a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('deal.pooled', $quote)  }}" data-toggle="tooltip"
                                                       title="Add Pooled Deal"><i class="fa fa-plus-square"></i></a>
                                                </li>
                                                @endhasanyrole
                                                <li>
                                                    <a href="{{ route('quote.show',$quote) }}" data-toggle="tooltip"
                                                       title="View Quote"><i class="fa fa-search"></i></a>
                                                </li>
                                                @hasanyrole('admin|saleagent')
                                                <li>
                                                    <a href="{{ route('quote.send', $quote->id)  }}" data-toggle="tooltip"
                                                       title="Send Quote to {{ strtolower($quote->client->email) }}"><i class="fa fa-location-arrow"></i></a>
                                                </li>
                                                @endhasanyrole
                                                @if($quote->status == 'Sent' && Auth::user()->hasRole('admin'))
                                                    <li>
                                                        <a href="{{ route('quote.approve', $quote->id)  }}"
                                                           data-toggle="tooltip"
                                                           title="Approve Quote"><i class="fa fa-check-circle"></i></a>
                                                    </li>
                                                @endif

                                                @if($quote->status == 'Approved' && Auth::user()->hasRole('admin'))
                                                    <li>
                                                        <a href="{{ route('attach.deal_conn', $quote->id) }}"
                                                           data-toggle="tooltip" title="Add Connections">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>No Quote Found!</strong>
                            </div>
                        @endif
                    </div>
                    <div id="draft_quotes" class="tab-pane fade">
                        @if(!$draft_quotes->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-hover dataTable no-footer">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Client</th>
                                    <th>Order Number</th>
                                    <th>Status</th>
                                    @hasrole('admin')
                                    <th>Added By</th>
                                    @endhasrole
                                    <th>Added On</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($draft_quotes as $quote)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            <a href="{{ route('client.show', $quote->client->id) }}">{{ $quote->client->full_name }}</a>
                                        </td>
                                        <td>{{ $quote->order_number }}</td>
                                        <td>{{ $quote->status }}</td>
                                        @hasrole('admin')
                                        <td>{{ $quote->user->full_name }}</td>
                                        @endhasrole
                                        <td>{{ $quote->created_at->format('d M, Y') }}</td>
                                        <td class="text-center">
                                            <ul class="list-inline">
                                                @hasanyrole('admin|saleagent')
                                                <li>
                                                    <a href="{{ route('deal.create', $quote)  }}" data-toggle="tooltip"
                                                       title="Add New Deal"><i class="fa fa-plus-square"></i></a>
                                                </li>
                                                @endhasanyrole
                                                <li>
                                                    <a href="{{ route('quote.show',$quote) }}" data-toggle="tooltip"
                                                       title="View Quote"><i class="fa fa-search"></i></a>
                                                </li>
                                                @hasanyrole('admin|saleagent')
                                                <li>
                                                    <a href="{{ route('quote.send', $quote->id)  }}" data-toggle="tooltip"
                                                       title="Send Quote to {{ strtolower($quote->client->email) }}"><i class="fa fa-location-arrow"></i></a>
                                                </li>
                                                @endhasanyrole
                                                @if($quote->status == 'Sent' && Auth::user()->hasRole('admin'))
                                                    <li>
                                                        <a href="{{ route('quote.approve', $quote->id)  }}"
                                                           data-toggle="tooltip"
                                                           title="Approve Quote"><i class="fa fa-check-circle"></i></a>
                                                    </li>
                                                @endif

                                                @if($quote->status == 'Approved' && Auth::user()->hasRole('admin'))
                                                    <li>
                                                        <a href="{{ route('attach.deal_conn', $quote->id) }}"
                                                           data-toggle="tooltip" title="Add Connections">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>No Draft Quote Found!</strong>
                            </div>
                        @endif
                    </div>
                    <div id="sent_quotes" class="tab-pane fade">
                        @if(!$sent_quotes->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-hover dataTable no-footer">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Client</th>
                                    <th>Order Number</th>
                                    <th>Status</th>
                                    @hasrole('admin')
                                    <th>Added By</th>
                                    @endhasrole
                                    <th>Added On</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sent_quotes as $quote)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            <a href="{{ route('client.show', $quote->client->id) }}">{{ $quote->client->full_name }}</a>
                                        </td>
                                        <td>{{ $quote->order_number }}</td>
                                        <td>{{ $quote->status }}</td>
                                        @hasrole('admin')
                                        <td>{{ $quote->user->full_name }}</td>
                                        @endhasrole
                                        <td>{{ $quote->created_at->format('d M, Y') }}</td>
                                        <td class="text-center">
                                            <ul class="list-inline">
                                                @hasanyrole('admin|saleagent')
                                                <li>
                                                    <a href="{{ route('deal.create', $quote)  }}" data-toggle="tooltip"
                                                       title="Add New Deal"><i class="fa fa-plus-square"></i></a>
                                                </li>
                                                @endhasanyrole
                                                <li>
                                                    <a href="{{ route('quote.show',$quote) }}" data-toggle="tooltip"
                                                       title="View Quote"><i class="fa fa-search"></i></a>
                                                </li>
                                                @hasanyrole('admin|saleagent')
                                                <li>
                                                    <a href="{{ route('quote.send', $quote->id)  }}" data-toggle="tooltip"
                                                       title="Send Quote to {{ strtolower($quote->client->email) }}"><i class="fa fa-location-arrow"></i></a>
                                                </li>
                                                @endhasanyrole
                                                @if($quote->status == 'Sent' && Auth::user()->hasRole('admin'))
                                                    <li>
                                                        <a href="{{ route('quote.approve', $quote->id)  }}"
                                                           data-toggle="tooltip"
                                                           title="Approve Quote"><i class="fa fa-check-circle"></i></a>
                                                    </li>
                                                @endif

                                                @if($quote->status == 'Approved' && Auth::user()->hasRole('admin'))
                                                    <li>
                                                        <a href="{{ route('attach.deal_conn', $quote->id) }}"
                                                           data-toggle="tooltip" title="Add Connections">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>No Sent Quote Found!</strong>
                            </div>
                        @endif
                    </div>
                    <div id="approved_quotes" class="tab-pane fade">
                        @if(!$approved_quotes->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-hover dataTable no-footer">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Client</th>
                                    <th>Order Number</th>
                                    <th>Status</th>
                                    @hasrole('admin')
                                    <th>Added By</th>
                                    @endhasrole
                                    <th>Added On</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($approved_quotes as $quote)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            <a href="{{ route('client.show', $quote->client->id) }}">{{ $quote->client->full_name }}</a>
                                        </td>
                                        <td>{{ $quote->order_number }}</td>
                                        <td>{{ $quote->status }}</td>
                                        @hasrole('admin')
                                        <td>{{ $quote->user->full_name }}</td>
                                        @endhasrole
                                        <td>{{ $quote->created_at->format('d M, Y') }}</td>
                                        <td class="text-center">
                                            <ul class="list-inline">
                                                @hasanyrole('admin|saleagent')
                                                <li>
                                                    <a href="{{ route('deal.create', $quote)  }}" data-toggle="tooltip"
                                                       title="Add New Deal"><i class="fa fa-plus-square"></i></a>
                                                </li>
                                                @endhasanyrole
                                                <li>
                                                    <a href="{{ route('quote.show',$quote) }}" data-toggle="tooltip"
                                                       title="View Quote"><i class="fa fa-search"></i></a>
                                                </li>
                                                @hasanyrole('admin|saleagent')
                                                <li>
                                                    <a href="{{ route('quote.send', $quote->id)  }}" data-toggle="tooltip"
                                                       title="Send Quote to {{ strtolower($quote->client->email) }}"><i class="fa fa-location-arrow"></i></a>
                                                </li>
                                                @endhasanyrole
                                                @if($quote->status == 'Sent' && Auth::user()->hasRole('admin'))
                                                    <li>
                                                        <a href="{{ route('quote.approve', $quote->id)  }}"
                                                           data-toggle="tooltip"
                                                           title="Approve Quote"><i class="fa fa-check-circle"></i></a>
                                                    </li>
                                                @endif

                                                @if($quote->status == 'Approved' && Auth::user()->hasRole('admin'))
                                                    <li>
                                                        <a href="{{ route('attach.deal_conn', $quote->id) }}"
                                                           data-toggle="tooltip" title="Add Connections">
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>No Approved Quote Found!</strong>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="container" style="margin-bottom: 40px">
        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table table-bordered dataTable no-footer">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Client</th>
                            <th>Order Number</th>
                            <th>Status</th>
                            @hasrole('admin')
                            <th>Added By</th>
                            @endhasrole
                            <th>Added On</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($quotes as $quote)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <a href="{{ route('client.show', $quote->client->id) }}">{{ $quote->client->full_name }}</a>
                                </td>
                                <td>{{ $quote->order_number }}</td>
                                <td>{{ $quote->status }}</td>
                                @hasrole('admin')
                                <td>{{ $quote->user->full_name }}</td>
                                @endhasrole
                                <td>{{ $quote->created_at->format('d M, Y') }}</td>
                                <td class="text-center">
                                    <ul class="list-inline">
                                        @hasanyrole('admin|saleagent')
                                        <li>
                                            <a href="{{ route('deal.create', $quote)  }}" data-toggle="tooltip"
                                               title="Add New Deal"><i class="fa fa-plus-square"></i></a>
                                        </li>
                                        @endhasanyrole
                                        <li>
                                            <a href="{{ route('quote.show',$quote) }}" data-toggle="tooltip"
                                               title="View Quote"><i class="fa fa-search"></i></a>
                                        </li>
                                        @hasanyrole('admin|saleagent')
                                        <li>
                                            <a href="{{ route('quote.send', $quote->id)  }}" data-toggle="tooltip"
                                               title="Send Quote in Email"><i class="fa fa-location-arrow"></i></a>
                                        </li>
                                        @endhasanyrole
                                        @if($quote->status == 'Sent' && Auth::user()->hasRole('admin'))
                                            <li>
                                                <a href="{{ route('quote.approve', $quote->id)  }}"
                                                   data-toggle="tooltip"
                                                   title="Approve Quote"><i class="fa fa-check-circle"></i></a>
                                            </li>
                                        @endif

                                        @if($quote->status == 'Approved' && Auth::user()->hasRole('admin'))
                                            <li>
                                                <a href="{{ route('attach.deal_conn', $quote->id) }}"
                                                   data-toggle="tooltip" title="Add Connections">
                                                    <i class="fa fa-plus"></i>
                                                </a>
                                            </li>
                                        @endif
                                    </ul>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>--}}
@endsection