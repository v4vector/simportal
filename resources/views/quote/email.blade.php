<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div>Dear <strong>{{ ucwords($quote->client->full_name) }}!</strong></div>
                <div>please find attached the detailed quote for your reference</div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <h4>Thanks!</h4>
                <div>Water Telecom Ltd<br>
                    2nd Floor, Stanford Gate South Road<br>
                    Brighton, BN316SB<br>
                    United Kingdom </div>
            </div>
        </div>
    </div>
</section>
