<section class="section">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <form action="{{ route('quote.store') }}" method="post" class="form-light">
                    @csrf
                    <section class="container">
                        <div class="row" style="margin-top: 30px">
                            <div class="col-sm-8">
                                <div class="bg-section">

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <h3>Create A Quote</h3>
                                        </div>
                                        <div class="col-sm-6 text-right">
                                            <a class="btn btn-primary btn-rounded pull-right" data-toggle="modal" href="#add_client_modal">Add New Client</a>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-section-heading">Client Info</div>
                                            <!-- Button trigger modal -->

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="client_id">Client</label>{{--->pluck('name', 'id')--}}
                                                @php
                                                    $logged = \Illuminate\Support\Facades\Auth::user();
                                                    $clients = \App\Models\User::role('client')
                                                    ->with('company')
                                                    ->when($logged->hasRole('saleagent'), function ($q) use ($logged) {
                                                        return $q->where('user_id', $logged->id);
                                                    })
                                                    ->get()->pluck('company.name', 'id')
                                                    ->prepend(' -- Select Client -- ', '');
                                                @endphp
                                                <label for="client_id"></label>
                                                {!! Form::select('client_id', $clients, old('client_id'), ['id' => 'client_id', 'class'=>'form-control select2', 'data-live-search'=> 'true']) !!}
                                                @if ($errors->has('client_id'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('client_id') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="order_number">Purchase Order No. (Optional)</label>
                                                <input type="text" name="order_number" id="order_number"
                                                       class="form-control" value="{{ old('order_number') }}">
                                                @if ($errors->has('order_number'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('order_number') }}</strong>
                                                        </span>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="deal_type">Deal Type</label>
                                                <select name="deal_type" id="deal_type" class="form-control">
                                                	<option value="Individual">Individual</option>
                                                	<option value="Pooled">Pooled</option>
                                                </select>
                                                @if ($errors->has('deal_type'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('deal_type') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <button class="btn btn-rounded btn-primary form-group">Save
                                                Quote
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                {{--<div class="bg-section form-group">
                                    <h4>PRICE BREAKDOWN</h4>
                                    <div class="offer-details">
                                        <div>SIMs X 0 <span class="pull-right">$0</span></div>
                                        <div>Data X 0 <span class="pull-right">$0</span></div>
                                        <div>Connection Cost <span class="pull-right">$0</span></div>
                                    </div>

                                    <div class="offer-details text-black bold">
                                        <div>Total <span class="pull-right">$0</span></div>
                                        <div>VAT <span class="pull-right">$0</span></div>
                                        <div>Total Price <span class="pull-right">$0</span></div>
                                    </div>
                                </div>--}}
                                <div class="bg-section">
                                    <h3>Offer Details</h3>

                                    <div class="offer-details gray">
                                        <h4>REQUIREMENTS</h4>
                                        <div>Product Type: <span id="product_type_text"></span>
                                        </div>
                                        <div>Data Type: <span id="data_type_text"></span></div>
                                        <div>Data Required: <span id="data_required_text"></span></div>
                                        <div>Sim Volume: <span id="sim_volume_text"></span></div>
                                        <div>Contract Length: <span id="contract_length_text"></span></div>
                                        <div>Client Application: <span id="client_application_text"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>

    </div>
</section>



<div class="modal fade" id="add_client_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><strong>Add New Client</strong></h4>
            </div>
            <div class="modal-body">
                <form action="{{ route('client.store') }}" method="post" id="store_client_form">
                    @csrf

                    @include('users._form')
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white btn-rounded" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->