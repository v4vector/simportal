@extends('layouts.app')
{{--{{ dd($errors->all()) }}--}}
@section('content')
    @include('quote._form')

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.min.js') }}"></script>

    <script>
        {{--
        //        Get volumes from database based on type
        function get_data_volume(type) {
            var select_box = $('#data_volume_id');
            var url = '{{ url('get-data-volume') }}/individual/' + type;
            var old = '{{ old('data_volume_id') }}';
            $.get(url, function (data) {
                var options = '';
                $.each(data, function (i, item) {
                    var select = '';
                    if (old == item['id']) {
                        select = 'selected';
                    }
                    options += '<option value="' + item['id'] + '"' + select + '>' + item['name'] + '</option>';
                });
                select_box.html(options)
            });
        }

        function set_terms() {
            var terms = $('.payment_terms:checked').val();
            if (terms === 'monthly') {
                $('#for-upfront').addClass('hidden');
                $('#for-monthly').removeClass('hidden');
            } else {
                $('#for-monthly').addClass('hidden');
                $('#for-upfront').removeClass('hidden');
            }
        }

        function calculate_package() {

            var product_type_id = $('#product_type_id').val();
            var data_usage = $('.data_usage:checked').val();
            var data_volume_id = Number($('#data_volume_id').val());

            if (!product_type_id || !data_usage || !data_volume_id) {
                return false;
            }

            var price_url = '{{ url('get-quote-price') }}/individual/' + product_type_id + '/' + data_usage;
            var price = 0;
            $.ajax({
                url: price_url,
                async: false,
                success: function (obt_price) {
                    price = obt_price;
                }
            });

            var volume_url = '{{ url('get-volume') }}' + '/' + data_volume_id;
            var volume = 0;
            $.ajax({
                url: volume_url,
                async: false,
                success: function (obt_vol) {
                    volume = obt_vol.volume;
                }
            });

            var base_price = volume * price;
            $('#base_price').val(base_price.toFixed(2)).change();

            $('#client_price').attr('min', base_price.toFixed(2)).val(base_price).change();
        }

        function calculate_margin() {
            var sims = Number($('#sim_volume').val());
            var contract_len = Number($('#contract_len').val());
            var base_price = Number($('#base_price').val());
            var client_price = Number($('#client_price').val());
            if (client_price > base_price) {
                var total_margin = client_price - base_price;
            } else {
                var total_margin = 0;
            }

            var unit_margin = total_margin / 2;
            var total_commission = (unit_margin * sims * contract_len);

            $('#margin_sales').val(unit_margin.toFixed(2));
            $('#margin_admin').val(unit_margin.toFixed(2));
            $('#total_commission_html').html(total_commission.toFixed(2));
            $('#total_commission').val(total_commission.toFixed(2));
        }

        function calculate_summary() {
            var sims = parseFloat($('#sim_volume').val());
            var client_price = Number($('#client_price').val());
            var contract_len = Number($('#contract_len').val());

            if (Number($('#base_price').val()) > client_price) {
                client_price = 0;
            }

            var deal_val = sims * client_price * contract_len;
            var vat = (20 / 100) * deal_val;
            var total_deal_val = deal_val + vat;

            $('#deal_amount_html').html(deal_val.toFixed(2));
            $('#vat_html').html(vat.toFixed(2));
            $('#total_deal_html').html(total_deal_val.toFixed(2));
            $('#vat').val(vat);
            $('#deal_amount').val(deal_val);
            $('#total_deal').val(total_deal_val);
            $('#total_upfront').html('£' + total_deal_val.toFixed(2));

            var monthly = total_deal_val / contract_len;
            var advance = 3 * monthly;

            $('#per_month').html('£' + monthly.toFixed(2));
            $('#installment').val(monthly.toFixed(2));

            $('#advance_payment').val(advance.toFixed(2));
            $('#advance_month').html('£' + advance.toFixed(2));

            var installment_duration = contract_len - 3;
            var total_installment_amount = installment_duration * monthly;
            $('#month_install').html(installment_duration);
            $('#total_install').html('£' + total_installment_amount.toFixed(2));
            $('#monthly').attr('checked');
        }
        --}}

        $(document).ready(function (e) {
            {{--var currency = '£';
            $('.select2').select2();

            var type = '{{ old('data') ?? 'high_data' }}';
            get_data_volume(type);

            var select_box = $("#network_preferences");
            select_box.select2();
            $('#product_type_id').on('change', function () {
                var type = $(this).val();
                if (type == 1) {
                    select_box.removeAttr('multiple');
                } else {
                    select_box.attr('multiple', 'multiple');
                }
                select_box.select2("destroy");
                select_box.select2();
            });

//            New Code Here
            $('#product_type_id, #data_volume_id').on('change', function (e) {
                e.preventDefault();
                calculate_package();
            });


            $('#contract_len').on('change', function (e) {
                if ($(this).val() <= 3) {
                    $('#for-upfront').removeClass('hidden');
                    $('#for-monthly').addClass('hidden');
                    $('#payment_terms').val('upfront');
                    $('#monthly').attr('disabled', 'disabled');
                } else {
                    $('#for-upfront').addClass('hidden');
                    $('#for-monthly').removeClass('hidden');
                    $('#payment_terms').val('monthly');
                    $('#monthly').removeAttr('disabled');
                }
                calculate_summary();
            });

//            If radio button for data usage changes, get its options from db
            $('.data_usage').on('change', function () {
                var type = $(this).val();
                get_data_volume(type);
                calculate_package();
            });
            $('#sim_volume, #data_volume_id').on('keyup change', function (e) {
                calculate_package();
            });
            $('#client_price').on('change keyup', function () {
                var base_price = Number($('#base_price').val());
                if ($(this).val() >= base_price) {
                    $('#client_price_small_error').addClass('hidden');
                    calculate_margin();
                    calculate_summary();
//                    set_terms();
                } else {
                    $('#client_price_small_error').removeClass('hidden');
                    calculate_margin();
                    calculate_summary();
//                    set_terms();
                }

            });

            $('#breakdown-trigger').on('click', function (e) {
                e.preventDefault();
                $('#breakdown').toggleClass('hidden');
            });
            $('#sim_volume, #sim_type').on('change keyup', function () {
                var vol = Number($('#sim_volume').val());
                var type = Number($('#sim_type').val());
                if (vol >= 1) {
                    $('#one_off_volume_html').html(vol);
                }
                if (vol >= 1 && type) {
                    var cost = vol * type;
                    $('#one_off_cost_html').html(cost.toFixed(2));

                    var VAT = (20 / 100) * cost;
                    var total = cost + VAT;

                    $('#one_off_vat_html').html(VAT.toFixed(2));
                    $('#one_off_total_html').html(total.toFixed(2));
                    $('#cost_sims').val(cost);
                    $('#vat_sims').val(VAT);
                    $('#total_sims_cost').val(total);
                }
            });--}}

//            Store Client Procudure
            $('#store_client_form').on('submit', function (e) {
                e.preventDefault();
                $(this).find('.valid_error').empty();
                $.ajax({
                    method: $(this).attr('method'),
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    success: function (result) {
                        var form = $('#store_client_form');
                        form[0].reset();
                        form.find('.valid_error').empty();
                        $('#add_client_modal').modal('hide');
                        window.location.href = "{{ route('quote.create') }}";
                    },
                    error: function (xhr) {
                        $.each(xhr.responseJSON.errors, function (key, msg) {
                           $('#'+key).after('<div class="text-danger small valid_error">'+ msg + '</div>');
                        });
                    }
                });
            });

//            Changes Show here
            {{--$('#product_type_id').on('change', function () {
                var option = $(this).find(':selected').text();
                $('#product_type_text').text(option);
            });
            $('#data_type').on('change', function () {
                $('#data_type_text').text($(this).val());
            });
            $('#contract_len').on('change', function () {
                $('#contract_length_text').text($(this).val() + ' Months');
            });
            $('#client_application_id').on('change', function () {
                var option = $(this).find(':selected').text();
                $('#client_application_text').text(option);
            });
            $('#data_volume_id').on('change', function () {
                var option = $(this).find(':selected').text();
                $('#data_required_text').text(option);
                $('#data_required_text').text(option);
            });
            $('#sim_volume').on('keyup', function () {
                var val = $(this).val();
               $('#sim_volume_text').html(val);
            });--}}
        });
    </script>

@endsection

{{--((number of sims * data per sim projected)+ (Number of sims*connection costs)) *Contract Duration)+(one off manufacture sim cost)--}}