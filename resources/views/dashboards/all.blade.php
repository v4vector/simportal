@extends('layouts.app')

@section('content')
    @php

        $logged = Illuminate\Support\Facades\Auth::user();

        $user = App\Models\User::has('connections')->with('usage')
                ->when($logged->hasRole('saleagent'), function ($q) use ($logged) {
                    return $q->where('user_id', $logged->id);
                })
                ->role('client')
                ->get(); 
       //dd($user);
        $connections = App\Models\Connection::
        when(Auth::user()->hasRole('client'), function ($query){
            $query->where('user_id', Auth::user()->id);
        })->
        get();
        //dd($connections); 

            $usage_report = new App\Models\UsageReport();
            //$all = $usage_report->search_all();
            $top_metrics = $usage_report->top_metric_search();
            $day_data = $usage_report->day_wise_search();
            $month_data = $usage_report->month_wise_search();

            //dd($day_data);
    @endphp

    <div class="bg-section form-group">
        <div class="container">
            <div class="row overallStats">
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">

                        <div class="stat-value">{{ $day_data->sum('daily_usage') }}</div>
                        <div class="stat-label">Total Usage (MB)</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $connections->where('status', 'ACTIVATED')->count() }}</div>
                        <div class="stat-label">Active Connections</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $connections->count() }}</div>
                        <div class="stat-label">Total Connections</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <form name="form">
                        <table>
                            <tr>
                                <td><div class="stat-value"><input type="text" name="num1" id="num1" placeholder="Enter Total Volume (MB)" /></div></td><br>
                            </tr>
                            <tr>
                                <td><div class="stat-value"><input type="text" name="subt" id="subt" readonly/></div></td>
                            </tr>
                        </table>
                        </form>
                        <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js'></script>
                        <script>
                            $(function() 
                            {
                                var usage = <?php echo json_encode($day_data->sum('daily_usage')); ?>;
                            $("#num1, #usage").on("keydown keyup", sum);
                            function sum() 
                            {
                                $("#subt").val(Number($("#num1").val()) - Number($("#usage").val()));
                            }
                            });
                        </script>
                        <div class="stat-label">Account Balance (MB)</div>
                    </div>
                </div> 
            </div>
        </div>
    </div>

    <div class="container">
        <section class="form-group">
            <div class="bg-section">
                <div class="row section-head">
                    <div class="col-sm-2"><h4 class="heading">DATA USAGE (MB)</h4></div>
                    <div class="col-sm-4">
                        <h4>
                            <span id="start_date_html"></span> - <span id="end_date_html"></span>
                        </h4>
                    </div>
                    <div class="col-sm-4 hidden-print">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="pill" href="#daily-tab">Daily</a></li>
                            <li><a data-toggle="pill" href="#monthly-tab">Monthly</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2 hidden-print">
                        <input type="hidden">
                        <a id="daterangepicker" class="btn btn-default">
                            <img src="{{ asset('assets/images/icons/calendar.png') }}"
                                 class="btn-icons" alt="Image">
                        </a>
                        <a class="btn btn-default">
                            <img src="{{ asset('assets/images/icons/file_download.png') }}"
                                 class="btn-icons" alt="Image">
                        </a>
                    </div>
                </div>

                <div class="row chart-stats">
                    <div class="col-sm-12">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="daily-tab">
                                <div class="col-sm-9">
                                    <div id="daily-data-chart"></div>
                                </div>
                                <div class="col-sm-3 hidden-print">
                                    <div class="row" id="day_statistics">
                                        <div class="col-xs-6 col-sm-12 stat form-group">
                                            <div class="stat-value total"></div>
                                            <div class="stat-label">Total Usage</div>
                                        </div>
                                        <div class="col-xs-6 col-sm-12 stat form-group">
                                            <div class="stat-value average"></div>
                                            <div class="stat-label">Average Usage</div>
                                        </div>
                                        <div class="col-xs-6 col-sm-12 stat form-group">
                                            <div class="stat-value sessions"></div>
                                            <div class="stat-label">Sessions</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="monthly-tab">
                                <div class="col-sm-9">
                                    <div id="monthly-data-chart"></div>
                                </div>
                                <div class="col-sm-3 hidden-print">
                                    <div class="row" id="month_statistics">
                                        <div class="col-xs-6 col-sm-12 stat form-group">
                                            <div class="stat-value total"></div>
                                            <div class="stat-label">Total Usage</div>
                                        </div>
                                        <div class="col-xs-6 col-sm-12 stat form-group">
                                            <div class="stat-value average"></div>
                                            <div class="stat-label">Average Usage</div>
                                        </div>
                                        <div class="col-xs-6 col-sm-12 stat form-group">
                                            <div class="stat-value sessions"></div>
                                            <div class="stat-label">Sessions</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--<div class="form-group">
                    <h4 class="heading">Data Usage</h4>
                </div>
                <div class="alert alert-info">
                    <strong><i class="fa fa-exclamation-circle"></i></strong> No Usage
                </div>--}}

            </div>
        </section>

        <section class="">
            <div class="row">
                <div class="col-sm-6 form-group">
                    <div class="bg-section">
                        <div class="form-group">
                            <h4 class="heading">CONNECTIONS</h4>
                        </div>
                        {{--@if(!$user->connections->isEmpty())--}}
                        <div id="connections-chart"></div>
                        {{--@else
                            <div class="alert alert-info">
                                <strong><i class="fa fa-exclamation-circle"></i></strong> No Connections
                            </div>
                        @endif--}}
                    </div>
                </div>
                <div class="col-sm-6 form-group">
                    <div class="bg-section">
                        <h4 class="heading">TOP METRICS (MB)</h4>
                                             @if(!$top_metrics->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-hover" id="top_metrics">
                                <tbody>
                                @foreach($top_metrics as $key => $group)
                                    @if($group->daily_usage > 0)
                                    <tr>
                                        <td>{!! number_format($group->daily_usage/1048576, 2) !!}</td>
                                        <td>{!! $group->connection->iccid !!}</td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <div class="alert alert-info">
                                <strong><i class="fa fa-exclamation-circle"></i></strong> No Usage
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/highcharts/css/highcharts.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
    <script>
        function set_day_statistics(daywise_data) {
            var month_count = 0;
            var usage_count = 0;
            var sess_count = 0;
            $.each(daywise_data, function (key, val) {
                month_count += 1;
                usage_count += val.daily_usage;
                sess_count += parseInt(val.daily_sessions);
            });
            var avg = usage_count / month_count;
            $('#day_statistics').find('.total').html(usage_count.toFixed(2));
            $('#day_statistics').find('.average').html(avg.toFixed(2));
            $('#day_statistics').find('.sessions').html(sess_count);
        }

        function set_month_statistics(monthwise_data) {
            var month_count = 0;
            var usage_count = 0;
            var sess_count = 0;
            $.each(monthwise_data, function (key, val) {
                month_count += 1;
                usage_count += val.daily_usage;
                sess_count += parseInt(val.daily_sessions);
            });
            var avg = usage_count / month_count;
            $('#month_statistics').find('.total').html(usage_count.toFixed(2));
            $('#month_statistics').find('.average').html(avg.toFixed(2));
            $('#month_statistics').find('.sessions').html(sess_count);
        }

        function set_dailychart(daywise_data) {
            var formatted = new Array();
            var day = '';
            $.each(daywise_data, function (key, val) {
                day = val.days.toString();
                formatted.push([day, val.daily_usage]);
            });

            Highcharts.chart('daily-data-chart', {
                chart: {
                    type: 'column'

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 1,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.2f} (MB)</b>'
                },
                series: [{
                    name: 'Usage',
                    data: formatted
                }]
            });
        }

        function set_monthlytab(monthwise_data) {
            var formatted = new Array();
            var day = '';
            $.each(monthwise_data, function (key, val) {
                month = val.months.toString();
                formatted.push([month, val.daily_usage]);
            });

            Highcharts.chart('monthly-data-chart', {
                chart: {
                    type: 'column'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 1,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                title: false,
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.1f} (MB)</b>'
                },
                series: [{
                    name: 'Usage',
                    data: formatted
                }]
            });
        }

        function set_topmetrics(top_metrics_data) {
            var formatted = '';
            var row = '';
            $.each(top_metrics_data, function (key, val) {
                if (val.daily_usage > 0) {
                    var usage = val.daily_usage / 1048576;
                    row = '<tr><td>' + val.connection.iccid + '</td><td>' + usage.toFixed(2) + '</td>';
                    formatted += row;
                }
            });
            $('#top_metrics').find('tbody').html(formatted);
        }

        function set_connections(connections) {
            var formatted = new Array();
            $.each(connections, function (key, val) {
                formatted.push([key, val]);
            });

            Highcharts.chart('connections-chart', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: false,
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    name: 'Connections',
                    colorByPoint: true,
                    data: formatted
                }]
            });
        }

        function set_dates(start, end) {
            $('#start_date_html').html(start);
            $('#end_date_html').html(end);
        }

        function call_ajax(start, end = false) {

            var url_day = '/day/';
            var url_month = '/month/';
            var url_metrics = '/top_metrics/';
            var url_connections = '/connections/';
            var url_dates = '/default_dates/';

            if (start && end) {
                var dates = start + '/' + end;

                var url_day = url_day + dates;
                var url_month = url_month + dates;
                var url_metrics = url_metrics + dates;
            }
            var base_url = '{{ url('searchAll') }}';

            $.ajax({
                url: base_url + url_day,
                success: function (result) {
                    set_dailychart(result);
                    set_day_statistics(result);
                }
            });
            $.ajax({
                url: base_url + url_month,
                success: function (result) {
                    set_monthlytab(result);
                    set_month_statistics(result);
                }
            });
            $.ajax({
                url: base_url + url_metrics,
                success: function (result) {
                    set_topmetrics(result);
                }
            });
            $.ajax({
                url: base_url + url_connections,
                success: function (result) {
                    set_connections(result);
                }
            });

            if (!start && !end) {
                $.ajax({
                    url: base_url + url_dates,
                    success: function (result) {
                        set_dates(result.start, result.end);
                    }
                });
            } else {
                set_dates(start, end);
            }
        }

        $(document).ready(function () {
            set_connections();

            //'2018-12-17', '2019-01-01'
            call_ajax();

            $('#daterangepicker').daterangepicker({
                opens: 'left',
                "parentEl": "daterangepicker",
                maxDate: moment(),
            }, function (start, end, label) {
                console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
            });
            $('#daterangepicker').on('apply.daterangepicker', function (ev, picker) {
                var start = picker.startDate.format('YYYY-MM-DD');
                var end = picker.endDate.format('YYYY-MM-DD');
                call_ajax(start, end);
            });
        });
    </script>
@endsection
