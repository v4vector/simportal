@extends('layouts.app')

@section('content')
    @php
    $logged = \Illuminate\Support\Facades\Auth::user();
        $devices = \App\Models\Device::
            when($logged->hasRole('client'), function ($query) {
                return $query->where('user_id', Auth::user()->id);
            })->get();

            $connections = \App\Models\Connection::with('usage')->
            when($logged->hasRole('client'), function ($query) {
                return $query->where('user_id', Auth::user()->id);
            })->get();

            $this_month_usage = \App\Models\Usage::with('connection')
                ->whereMonth('created_at', date('m'))
                ->whereYear('created_at', date('Y'))
                ->orderBy('daily_usage', 'desc')
                ->when($logged->hasRole('client'), function ($query) {
                    return $query->whereHas('connection', function ($q){
                        $q->where('user_id', Auth::user()->id);
                    });
                })
                ->get();

            $usage_days = \App\Models\Usage::with('connection')
                ->when($logged->hasRole('client'), function ($query) {
                    return $query->whereHas('connection', function ($q){
                        $q->where('user_id', Auth::user()->id);
                    });
                })
                ->where('created_at', '>=', \Carbon\Carbon::now()->subDays(7))
                ->get();

            $usage_months = \App\Models\Usage::with('connection')
                ->when($logged->hasRole('client'), function ($query) {
                    return $query->whereHas('connection', function ($q){
                        $q->where('user_id', Auth::user()->id);
                    });
                })
                ->where('created_at', '>=', \Carbon\Carbon::now()->startOfMonth()->subMonth(12))
                ->get();

            $group_days = $usage_months->groupBy(function($item) {
                return $item->created_at->format('Y-m-d');
            });
            $group_months = $usage_months->groupBy(function($item) {
                return $item->created_at->format('Y-m');
            });

            //Get avg for all the months on monthly basis
            $total_months = $group_months->count();
            $total_usage = $usage_months->sum('daily_usage');
            $average_monthly = round(($total_usage  / 1048576) / $total_months, 2);

            $total_days = $group_days->count();
            $total_usage = $usage_days->sum('daily_usage');
            $average_daily = round(($total_usage / 1048576) / $total_days, 2);
    @endphp

    <section class="bg-section form-group">
        <div class="container">

            <div class="row overallStats">
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">

                        <div class="stat-value">{{ round($this_month_usage->sum('daily_usage') / 1048576, 2) }}</div>
                        <div class="stat-label">Data Usage (MB)</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $connections->where('status', 'ACTIVATED')->count() }}</div>
                        <div class="stat-label">Active Connections</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $connections->count() }}</div>
                        <div class="stat-label">Total Connections</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">£{{ '???' }}</div>
                        <div class="stat-label">Account Balance</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container form-group">
        <div class="bg-section chart-section">
            <div class="row section-head">
                <div class="col-sm-2"><h4 class="heading">DATA USAGE (MB)</h4></div>
                <div class="col-sm-4"><h4>{{ Carbon\Carbon::now()->subDays(7)->format('d M Y') }}
                        - {{ Carbon\Carbon::now()->subDays(1)->format('d M Y') }}</h4></div>
                <div class="col-sm-4 hidden-print">
                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#daily-tab">Daily</a></li>
                        <li><a data-toggle="pill" href="#monthly-tab">Monthly</a></li>
                    </ul>
                </div>
                <div class="col-sm-2 hidden-print">
                    <a href="" class="btn btn-default"><img src="{{ asset('assets/images/icons/calendar.png') }}"
                                                            class="btn-icons" alt="Image"></a>
                    <a href="" class="btn btn-default"><img src="{{ asset('assets/images/icons/file_download.png') }}"
                                                            class="btn-icons" alt="Image"></a>
                </div>
            </div>
            <div class="row chart-stats">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="daily-tab">
                        <div class="col-sm-9">
                            <div id="daily-data-chart"></div>
                        </div>
                        <div class="col-sm-3 hidden-print">
                            <div class="row">
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ round($usage_days->sum('daily_usage') / 1048576, 2) }}</div>
                                    <div class="stat-label">Total Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $average_daily }}</div>
                                    <div class="stat-label">Average Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $usage_days->sum('daily_sessions') }}</div>
                                    <div class="stat-label">Sessions</div>
                                </div>
                                {{--<div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '35' }}</div>
                                    <div class="stat-label">Session Timeouts</div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="monthly-tab">
                        <div class="col-sm-9">
                            <div id="monthly-data-chart"></div>
                        </div>
                        <div class="col-sm-3 hidden-print">
                            <div class="row">
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ round($usage_months->sum('daily_usage') / 1048576, 2) }}</div>
                                    <div class="stat-label">Total Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $average_monthly }}</div>
                                    <div class="stat-label">Average Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $usage_months->sum('daily_sessions') }}</div>
                                    <div class="stat-label">Sessions</div>
                                </div>
                                {{--<div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '35' }}</div>
                                    <div class="stat-label">Session Timeouts</div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="row form-group">
            <div class="col-sm-6 form-group">
                <div class="bg-section">
                    <div class="form-group">
                        <h4 class="heading">CONNECTIONS</h4>
                    </div>
                    <div id="connections-chart"></div>
                </div>
            </div>
            <div class="col-sm-6 form-group">
                <div class="bg-section">
                    <div class="form-group">
                        <h4 class="heading">TOP METRICS (MB)</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table" border="0">
                            <tbody>
                            @php
                                $top_metrics = $this_month_usage->sortByDesc('ctdDataUsage')->groupBy('connection_id');
                                //dd($top_metrics);
                            @endphp
                            @if($top_metrics)
                                @foreach($top_metrics as $usage)
                                    @php
                                        $amount_used = round($usage->sum('daily_usage') / 1048576, 2)
                                    @endphp
                                    @if($amount_used > 0)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $usage->first()->connection->iccid }}</td>
                                            <td>{{ $amount_used }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/highcharts/css/highcharts.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
    <script>
        $(document).ready(function () {
            Highcharts.chart('daily-data-chart', {
                chart: {
                    type: 'column'

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 1,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.1f} (MB)</b>'
                },
                series: [{
                    name: 'Usage',
                    data: [
                            @foreach($group_days as $key => $day)
                            @php
                                $sum = $day->sum('daily_usage') / 1048576;
                                $day = \Carbon\Carbon::parse($key)->format('d M');
                            @endphp

                            @if($loop->last)
                        ['{{ $day }}', {{ $sum }}]
                                @else
                            ['{{ $day }}', {{ $sum }}],
                        @endif

                        @endforeach
                    ]
                }]
            });
            Highcharts.chart('monthly-data-chart', {
                chart: {
                    type: 'column'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 1,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                title: false,
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.1f} (MB)</b>'
                },
                series: [{
                    name: 'Population',
                    data: [
                            @foreach($group_months as $key => $month)
                            @php
                                $sum = $month->sum('daily_usage') / 1048576;
                                $label = \Carbon\Carbon::parse($key)->format('M Y');
                            @endphp

                            @if($loop->last)
                        ['{{ $label }}', {{ $sum }}]
                                @else
                            ['{{ $label }}', {{ $sum }}],
                        @endif

                        @endforeach
                    ]
                }]
            });

            Highcharts.chart('connections-chart', {
                chart: {
                    type: 'pie'
                },

                plotOptions: {
                    pie: {
                        innerSize: '70%'
                    }
                },
                title: {
                    text: '{{ $connections->count() }}',
                    verticalAlign: 'middle',
                    floating: true
                },

                series: [{
                    data: [
                        ['Online', {{ $connections->where('status', 'ACTIVATED')->count() }}],
                        ['Offline', {{ $connections->where('status', '!=', 'ACTIVATED')->count() }}]
                    ]
                }]
            });
        });
    </script>
@endsection
