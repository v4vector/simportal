@extends('layouts.app')

@section('content')
    @php
        $me = \Illuminate\Support\Facades\Auth::user();
        $my_quotes = \App\Models\Quote::has('deals')->with('deals')->where('user_id', $me->id)->get();

        $total_commission = $my_quotes->where('status', 'Approved')->sum(function ($quote){
            return $quote->deals->sum('total_commission');
        });
        $revenue_generated = $my_quotes->where('status', 'Approved')->sum(function ($quote){
            return $quote->deals->sum('total_deal') + $quote->deals->sum('total_sims_cost');
        });
        $my_clients = \App\Models\User::with('client_quotes')->where('user_id', $me->id)->get();
        $active_clients = \App\Models\User::whereHas('client_quotes', function($q){
            return $q->where('status', 'Approved');
        })->where('user_id', $me->id)->get();

        $recent_quotes = \App\Models\Quote::with('client.company')->where('user_id', $me->id)->orderBy('created_at', 'desc')->get();
        $recent_approved = \App\Models\Quote::with('client.company')->where('status', 'Approved')->orderBy('approved_date', 'desc')->get();

    @endphp

    <section class="bg-section form-group">
        <div class="container">
            <div class="row overallStats form-group">
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">

                        <div class="stat-value">&pound;{{ round($revenue_generated, 2) }}</div>
                        <div class="stat-label">Revenue Generated</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $active_clients->count() }}</div>
                        <div class="stat-label">Active Clients</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $my_clients->count() }}</div>
                        <div class="stat-label">Total Clients</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">&pound;{{ round($total_commission, 2) }}</div>
                        <div class="stat-label">Total Commission</div>
                    </div>
                </div>
            </div>

            <div class="row overallStats">
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">

                        <div class="stat-value">{{ $my_quotes->count() }}</div>
                        <div class="stat-label">Total Quotes Generated</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $my_quotes->where('status', 'Draft')->count() }}</div>
                        <div class="stat-label">Draft</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $my_quotes->where('status', 'Sent')->count() }}</div>
                        <div class="stat-label">Sent</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $my_quotes->where('status', 'Approved')->count() }}</div>
                        <div class="stat-label">Client Approved</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="form-group">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="bg-section">
                        <div class="form-group gray text-uppercase">
                            <h4>All quotes</h4>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($recent_quotes as $quote)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td class="text-capitalize"><a href="{{ route('client.show', $quote->client->id) }}">{{ $quote->client->company->name }}</a></td>
                                        <td>{{ $quote->created_at->format('d M Y') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="bg-section">
                        <div class="form-group gray text-uppercase">
                            <h4>recent client approved</h4>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($recent_approved as $quote)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td class="text-capitalize"><a href="{{ route('client.show', $quote->client->id) }}">{{ $quote->client->company->name }}</a></td>
                                        <td>{{ $quote->approved_date->format('d M Y') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('styles')

@endsection

@section('scripts')

@endsection