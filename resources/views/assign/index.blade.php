@extends('layouts/app')

@section('content')
    <div class="container">
        <div class="bg-section">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12">
                <a href="{{ route('assign.sim.create') }}" class="btn btn-primary pull-right">Assign Sims</a>

                    @if($connections->isEmpty() == false)
                        <div class="table-responsive">
                            <table class="table dataTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ICCID</th>
                                    <th>Email</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($connections as $connection)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $connection->iccid }}</td>
                                        <td>{{ isset($connection->user_id) ? $connection->user->email : 'N/A'  }}</td>
                                        <td>{{ isset($connection->user_id) ? $connection->user->fullName : 'N/A' }}</td>
                                        @if(isset($connection->user))
                                            <td><a href="{{ isset($connection->user_id) ? route('deassign.sim', ['id' => $connection->id])  : 'N/A' }}" class="btn btn-info">Dessign</a></td>
                                        @else
                                            <td>N/A</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-warning">
                            <strong>Not Found!</strong> You can add new users <a href="{{ route($url.'.create') }}">here</a>.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/switchery/dist/switchery.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.js') }}"></script>
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
@endsection
