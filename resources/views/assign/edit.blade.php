@extends('layouts/app')

@section('content')

    <div class="container">
        <div class="bg-section">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <form method="post" action="{{ route('assign.sim.update', ['id' => $client->id]) }}">
                @csrf
                <div class="form-group" id="iccid">
                    <label for="select2">Deassign Sims</label>
                    <select class="form-control select2" name="iccid[]"  multiple="multiple" style="width: 100%;">
                        @foreach($iccids as $iccid)
                            <option value="{{ $iccid->id }}"  selected >{{ $iccid->iccid }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('iccid'))
                        <span class="invalid-feedback" role="alert">
                            <strong class="text-danger">{{ $errors->first('iccid') }}</strong>
                        </span>
                    @endif
                </div>
                <div id="submitBtn" class="form-group" >
                    <input type="submit" class="btn btn-primary" value="Update Assign Sim">
                </div>
            </form>
        </div>
    </div>

@endsection
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".select2").select2();
        });

    </script>
@endsection
