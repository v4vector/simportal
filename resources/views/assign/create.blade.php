@extends('layouts/app')

@section('content')

    <div class="container">
        <div class="bg-section">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <form method="post" action="{{ route('assign.sim.store') }}">
                <h2>Assign Sims</h2>
                @csrf
                <div class="form-group" id="iccid">
                    <label for="select2">Select ICCIDs</label>
                    <select class="form-control select2" name="iccid[]"  multiple="multiple" style="width: 100%;">
                        @foreach($iccids as $iccid)
                            <option value="{{ $iccid->id }}" @if(old('iccid') != null && old('iccid') == $iccid->iccid) selected @endif>{{ $iccid->iccid }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('iccid'))
                        <span class="invalid-feedback" role="alert">
                            <strong class="text-danger">{{ $errors->first('iccid') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <input id="nextBtn" type="submit" class="btn btn-primary" value="Select Client">
                </div>
                <div class="form-group" style="display: none" id="client">
                    <select name="client" class="form-control select2" id="client" style="width: 100%;">
                        @foreach($clients as $client)
                            <option value="{{ $client->id }}">{{ $client->email }} ( {{ $client->fullName }} )</option>
                        @endforeach
                    </select>
                    @if ($errors->has('client'))
                        <span class="invalid-feedback" role="alert">
                            <strong class="text-danger">{{ $errors->first('client') }}</strong>
                        </span>
                    @endif
                </div>
                <div id="backBtn" class="form-group" style="display: none">
                    <input type="submit" class="btn btn-primary" value="Back">
                </div>
                <div id="submitBtn" class="form-group" style="display: none">
                    <input type="submit" class="btn btn-primary" value="Assign Sim">
                </div>
                <a href="{{ route('assign.sim.index') }}" class="btn btn-primary">Assign List</a>

            </form>
        </div>
    </div>

@endsection
@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".select2").select2();

            $("#nextBtn").click(function(e){
                e.preventDefault();
                $("#nextBtn").fadeOut();
                $("#iccid").fadeOut();

                $("#client").fadeIn();
                $("#backBtn").fadeIn();
                $("#submitBtn").fadeIn();

            });

            $("#backBtn").click(function(e){
                e.preventDefault();
                $("#nextBtn").fadeIn();
                $("#iccid").fadeIn();

                $("#client").fadeOut();
                $("#backBtn").fadeOut();
                $("#submitBtn").fadeOut();

            });
        });

    </script>
@endsection
