@extends('layouts.app')

@section('styles')
@endsection
@section('scripts')
@endsection

@section('content')
    <section class="section">
        <div class="container">

            <form action="{{ route('wallet.store') }}" method="POST" role="form">
                @csrf

                @include('wallet._form')

            </form>

        </div>
    </section>
@endsection
