@extends('layouts.app')

@section('styles')
@endsection
@section('scripts')
@endsection

@section('content')
    <section class="section">
        <div class="container">

            <form action="{{ route('wallet.update', $wallet) }}" method="POST" role="form">
                @method('put')
                @csrf

                @include('wallet._form')

            </form>

        </div>
    </section>
@endsection
