<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="client">Client *</label>
            {!! Form::select('client', $clients->pluck('full_name', 'id')->prepend(' -- Select Client -- ', ''), old('client', $wallet->client_id), ['id'=> 'client', 'class'=>'form-control select2']) !!}

            @if ($errors->has('client'))
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('client') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="amount">Amount *</label>
            <input type="number" name="amount" id="amount" class="form-control"
                   value="{{ old('amount', $wallet->amount) }}">

            @if ($errors->has('amount'))
                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('amount') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-sm-12">
        <input type="submit" class="btn btn-primary" value="{{ $wallet->id ? 'Update' : 'Save' }}">
    </div>
</div>