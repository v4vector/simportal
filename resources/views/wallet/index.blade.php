@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
@endsection

@section('content')
    <section class="section">
        <div class="container">
            @role('client')
            @php
                $user = \Illuminate\Support\Facades\Auth::user();
                $wallet = \App\Models\Wallet::with('client')
                ->where('client_id', $user->id)
                ->get();
            @endphp
            @if(!$wallet->isEmpty())
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <h3>Total Amount: ${{ round($wallet->sum('amount'), 2) }}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTable no-footer">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Amount</th>
                                    <th>Transaction Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($wallet as $wal)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $wal->amount }}</td>
                                        <td>{{ $wal->created_at->format('d M, Y') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-danger">
                    <strong>No Amount Found!</strong>
                </div>
            @endif
            @endrole


            @role('admin')
            @php
                $clients = \App\Models\User::role('client')->with('wallet')->get();
            @endphp
            @if(!$clients->isEmpty())
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered dataTable no-footer">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Client</th>
                                    <th>Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($clients as $client)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td><a href="">{{ $client->full_name }}</a></td>
                                        <td>{{ $client->wallet->sum('amount') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-danger">
                    <strong>No Amount Found!</strong>
                </div>
            @endif
            @endrole
        </div>
    </section>
@endsection