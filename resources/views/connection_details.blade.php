@extends('layouts/app')

@section('content')

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="sim_details">
                        <span class="switch"><input type="checkbox" class="js-switch" {{ $device->status == 'ACTIVATED' ? 'checked' : '' }}></span>
                        <span class="bold mdi mdi-fiber-manual-record {{ $device->status == 'ACTIVATED' ? 'text-success' : 'text-danger' }}"></span>
                        <span class="bold">{{ $device->status == 'ACTIVATED' ? 'Online' : 'Offline' }}</span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="sim_details">
                        <span class="icon mdi mdi-error-outline"></span>
                        <span class="bold">ICCID:</span>
                        <span class="value">{{ $device->iccid }}</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="sim_details">
                        <span class="icon mdi mdi-data-usage"></span>
                        <span class="bold">Type:</span>
                        <span class="value">{{ 'SIM Card' }}</span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="sim_details">
                        <span class="icon mdi mdi-network-cell"></span>
                        <span class="bold">Network:</span>
                        <span class="value">{{ 'EE' }}</span>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="sim_details">
                        <div class="dropdown more">
                            <a class="btn btn-link dropdown-toggle" type="button" id="nh_5_dd" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="true">
                                <i class="mdi mdi-more-horiz"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_5_dd">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container form-group">
        <div class="bg-section chart-section">
            <div class="row section-head">
                <div class="col-sm-2"><h4 class="heading">DATA USAGE (MB)</h4></div>
                <div class="col-sm-4"><h4>{{ '25 JUL 2018' }} - {{ '25 AUG 2018' }}</h4></div>
                <div class="col-sm-4">
                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#daily-tab">Daily</a></li>
                        <li><a data-toggle="pill" href="#monthly-tab">Monthly</a></li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <a href="" class="btn btn-default"><img src="{{ asset('assets/images/icons/calendar.png') }}" class="btn-icons" alt="Image"></a>
                    <a href="" class="btn btn-default"><img src="{{ asset('assets/images/icons/file_download.png') }}" class="btn-icons" alt="Image"></a>
                </div>
            </div>
            <div class="row chart-stats">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="daily-tab">
                        <div class="col-sm-9">
                            <div id="sim-daily-chart"></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '5,146.81' }}</div>
                                    <div class="stat-label">Total Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '200.53' }}</div>
                                    <div class="stat-label">Average Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '129' }}</div>
                                    <div class="stat-label">Connections</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '35' }}</div>
                                    <div class="stat-label">Session Timeouts</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="monthly-tab">
                        <div class="col-sm-9">
                            <div id="sim-monthly-chart"></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '5,146.81' }}</div>
                                    <div class="stat-label">Total Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '200.53' }}</div>
                                    <div class="stat-label">Average Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '129' }}</div>
                                    <div class="stat-label">Connections</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '35' }}</div>
                                    <div class="stat-label">Session Timeouts</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="bg-section">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <h4 class="heading">CONNECTION HISTORY</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-flex no-footer" id="connection_history_table">
                            <thead>
                            <tr>
                                <th>Session Start</th>
                                <th>Duration</th>
                                <th>Data Usage (MB)</th>
                                <th>Country</th>
                                <th>Network</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>12/09/2018 6:20pm</td>
                                <td>15:00</td>
                                <td>500</td>
                                <td>UK</td>
                                <td>Vodafone UK</td>
                            </tr>
                            <tr>
                                <td>12/09/2018 6:20pm</td>
                                <td>15:00</td>
                                <td>32.56</td>
                                <td>UK</td>
                                <td>O2 UK (Telefonica)</td>
                            </tr>
                            <tr>
                                <td>12/09/2018 6:20pm</td>
                                <td>15:00</td>
                                <td>32.56</td>
                                <td>UK</td>
                                <td>O2 UK (Telefonica)</td>
                            </tr>
                            <tr>
                                <td>12/09/2018 6:20pm</td>
                                <td>15:00</td>
                                <td>32.56</td>
                                <td>UK</td>
                                <td>O2 UK (Telefonica)</td>
                            </tr>
                            <tr>
                                <td>12/09/2018 6:20pm</td>
                                <td>15:00</td>
                                <td>32.56</td>
                                <td>UK</td>
                                <td>O2 UK (Telefonica)</td>
                            </tr>
                            <tr>
                                <td>12/09/2018 6:20pm</td>
                                <td>15:00</td>
                                <td>32.56</td>
                                <td>UK</td>
                                <td>O2 UK (Telefonica)</td>
                            </tr>
                            <tr>
                                <td>12/09/2018 6:20pm</td>
                                <td>15:00</td>
                                <td>32.56</td>
                                <td>UK</td>
                                <td>O2 UK (Telefonica)</td>
                            </tr>
                            <tr>
                                <td>12/09/2018 6:20pm</td>
                                <td>15:00</td>
                                <td>32.56</td>
                                <td>UK</td>
                                <td>O2 UK (Telefonica)</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/switchery/dist/switchery.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/highcharts/css/highcharts.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.js') }}"></script>
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
    <script>
        $(document).ready(function (e) {
            Highcharts.chart('sim-daily-chart', {
                chart: {
                    type: 'column',

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 2,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
                },
                series: [{
                    name: 'Population',
                    data: [
                        ['28 Jul', 50],
                        ['29 Jul', 55],
                        ['30 Jul', 105],
                        ['01 Aug', 85],
                        ['02 Aug', 41],
                        ['03 Aug', 125],
                        ['04 Aug', 120],
                        ['05 Aug', 100],
                        ['06 Aug', 55],
                        ['07 Aug', 98],
                        ['08 Aug', 65],
                        ['09 Aug', 102],
                        ['10 Aug', 85],
                        ['11 Aug', 90],
                        ['12 Aug', 155],
                        ['13 Aug', 116],
                        ['14 Aug', 45],
                        ['15 Aug', 107],
                        ['16 Aug', 90],
                        ['17 Aug', 96]
                    ]
                }]
            });
            Highcharts.chart('sim-monthly-chart', {
                chart: {
                    type: 'column',

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 2,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
                },
                series: [{
                    name: 'Population',
                    data: [
                        ['28 Jul', 50],
                        ['29 Jul', 55],
                        ['30 Jul', 105],
                        ['01 Aug', 85],
                        ['02 Aug', 41],
                        ['03 Aug', 125],
                        ['04 Aug', 120],
                        ['05 Aug', 100],
                        ['06 Aug', 55],
                        ['07 Aug', 98],
                        ['08 Aug', 65],
                        ['09 Aug', 102],
                        ['10 Aug', 85],
                        ['11 Aug', 90],
                        ['12 Aug', 155],
                        ['13 Aug', 116],
                        ['14 Aug', 45],
                        ['15 Aug', 107],
                        ['16 Aug', 90],
                        ['17 Aug', 96]
                    ]
                }]
            });
        });
    </script>
@endsection
