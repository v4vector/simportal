<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="icon" href="{{ asset('assets/images/water-telecom-logo.png') }}" type="image/png" sizes="16x16">
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/plugins/bootstrap-material-design-icons/css/material-icons.min.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    {{--Page Level Styles Start--}}
    @yield('styles')
    {{--Page Level Styles End--}}
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">
</head>
<body>
<div id="wrapper">
    <!-- This is the main navbar -->
    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('assets/images/water-telecom-logo.png') }}" class="img-responsive"
                         alt="Water Telecom"/>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li class="{{ Request::is('/') || Request::is('dashboard') ? 'active' : '' }}">
                        <a href="{{ url('/') }}">DASHBOARD</a>
                    </li>

                    @unlessrole('saleagent')
                    <li class="{{ Request::is('connections') ? 'active' : '' }}">
                        <a href="{{ url('connections') }}">CONNECTIVITY</a>
                    </li>
                    @endunlessrole
                     @hasanyrole('saleagent|admin|superuser')
                    <li class="{{ Request::is('quote') ? 'active' : '' }}">
                        <a href="{{ route('quote.index') }}">QUOTES</a>
                    </li>
                    @endhasanyrole
                    @hasanyrole('saleagent|admin|superuser')
                    <li class="{{ Request::is('client') ? 'active' : '' }}">
                        <a href="{{ route('client.index') }}">CLIENTS</a>
                    </li>
                    @endhasanyrole

                    @role('admin|superuser')
                    <li class="{{ Request::is('saleagent') ? 'active' : '' }}">
                        <a href="{{ route('saleagent.index') }}">AGENTS</a>
                    </li>
                    @endrole


                    @role('superuser')
                    <li class="{{ Request::is('admin') ? 'active' : '' }}">
                        <a href="{{ route('admin.index') }}">ADMINS</a>
                    </li>
                    @endrole
                </ul>
                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->full_name }} <i
                                    class="mdi mdi-expand-more"></i></a>
                        <ul class="dropdown-menu">
                            {{--                            <li><a href="{{ route('edit-account') }}">{{ Auth::user()->full_name }}</a></li>--}}

                            @role('admin|superuser')
                            <li><a href="{{ route('sync') }}">Sync Devices</a></li>
                            <li><a href="{{ route('assign.sim.create') }}">Assign Sims</a></li>
                            <li><a href="{{ route('client.index') }}">Clients</a></li>
                            @endrole

                            @role('client')
                            <li><a href="{{ url('devices/create') }}">Create Device</a></li>
                            @endrole

                            @role('saleagent')
                            <li><a href="{{ route('quote.index') }}">Quotes</a></li>
                            <li><a href="{{ route('quote.create') }}">Create Quote</a></li>
                            <li><a href="{{ route('client.index') }}">Clients</a></li>
                            @endrole

                            <li><a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                            </li>
                        </ul>
                    </li>
                    <li class="navbar-notification-area">
                        <a id="notify-button">
                            <i class="mdi mdi-notifications-none" id="notify-icon"></i>
                            <label class="notification_number"></label>
                        </a>
                        <div class="notification-box">
                            <div class="notification-header">
                                <div class="col-xs-6">Notifications</div>
                                <div class="col-xs-6 text-right"><a id="mark_all_read">Mark All as Read</a></div>
                            </div>
                            <div class="notification-pasted">
                            </div>
                        </div>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <!-- Main navbar ends here -->

    <main class="body-area">
        @yield('content')
    </main>
</div>

<script src="{{ asset('assets/plugins/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap/dist/js/bootstrap.js') }}"></script>


{{--Page Level scripts start--}}
@yield('scripts')
{{--Page Level scripts end--}}

<script src="{{ asset('assets/js/main.js') }}"></script>

@if(Auth::user()->hasRole('admin'))
    <script>
        function set_notifications() {
            $.ajax({
                url: '{{ route('admin.notifications') }}',
                async: false,
                success: function (result) {
                    var notifications = '';
                    var unread = 0;

                    $.each(result, function (iteration, note) {
                        notification = '<a class="notification ' + note.status + '" id="' + note.id + '" href="' + note.link + '">' + note.message + '</a>';
                        notifications += notification;

                        if (note.status == 'unread') {
                            unread += 1;
                        }
                    });

                    $('#notify-icon').removeClass();
                    if (unread > 0) {
                        $('#notify-icon').addClass('mdi mdi-notifications-active');
                        $('.notification_number').html(unread);
                    } else {
                        $('#notify-icon').addClass('mdi mdi-notifications-none');
                        $('.notification_number').empty();
                    }

                    $('.notification-pasted').html(notifications);
                }
            });
        }

        function real_all() {
            $.ajax({
                url: '{{ route('admin.readall') }}',
                async: false,
                success: function (result) {
                    set_notifications();
                }
            });
        }


        function read_notification(id) {
            $.ajax({
                url: '{{ url('admin-notification-read/') }}' + '/' + id,
                async: false,
                success: function (result) {
                    set_notifications();
                }
            });

            $('.unread').on('mouseover', function () {
                var url = $(this).attr('id');
                read_notification(url);
            });
        }


        $(document).ready(function () {
            set_notifications();

            $('.unread').on('mouseover', function () {
                var url = $(this).attr('id');
                read_notification(url);
            });

            $('#mark_all_read').on('click', function (e) {
                real_all();
            });
        });
    </script>
@endif

@if(Auth::user()->hasRole('saleagent'))
    <script>
        function set_notifications() {
            $.ajax({
                url: '{{ route('saleagent.notifications') }}',
                async: false,
                success: function (result) {
                    var notifications = '';
                    var unread = 0;

                    $.each(result, function (iteration, note) {
                        notification = '<a class="notification ' + note.status + '" id="' + note.id + '" href="' + note.link + '">' + note.message + '</a>';
                        notifications += notification;

                        if (note.status == 'unread') {
                            unread += 1;
                        }
                    });

                    $('#notify-icon').removeClass();
                    if (unread > 0) {
                        $('#notify-icon').addClass('mdi mdi-notifications-active');
                        $('.notification_number').html(unread);
                    } else {
                        $('#notify-icon').addClass('mdi mdi-notifications-none');
                        $('.notification_number').empty();
                    }

                    $('.notification-pasted').html(notifications);
                }
            });
        }

        function real_all() {
            $.ajax({
                url: '{{ route('saleagent.readall') }}',
                async: false,
                success: function (result) {
                    set_notifications();
                }
            });
        }


        function read_notification(id) {
            $.ajax({
                url: '{{ url('saleagent-notification-read/') }}' + '/' + id,
                async: false,
                success: function (result) {
                    set_notifications();
                }
            });

            $('.unread').on('mouseover', function () {
                var url = $(this).attr('id');
                read_notification(url);
            });
        }


        $(document).ready(function () {
            set_notifications();

            $('.unread').on('mouseover', function () {
                var url = $(this).attr('id');
                read_notification(url);
            });

            $('#mark_all_read').on('click', function (e) {
                real_all();
            });
        });
    </script>
@endif

</body>
</html>
