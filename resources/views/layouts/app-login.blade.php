<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="icon" href="{{ asset('assets/images/water-telecom-logo.png') }}" type="image/png" sizes="16x16">
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/plugins/bootstrap-material-design-icons/css/material-icons.min.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    {{--Page Level Styles Start--}}
    @yield('styles')
    {{--Page Level Styles End--}}
    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">
</head>
<body>
<div id="wrapper">

    <main class="body-area">
        @yield('content')
    </main>
</div>

<script src="{{ asset('assets/plugins/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap/dist/js/bootstrap.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>

{{--Page Level scripts start--}}
@yield('scripts')
{{--Page Level scripts end--}}

</body>
</html>
