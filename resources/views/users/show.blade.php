@extends('layouts/app')

@section('content')

    <div class="container">
        <div class="section bg-section">

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <strong>Name: </strong> {{ $user->full_name }}
                    </div>
                    <div class="form-group">
                        <strong>Contact Mobile: </strong> {{ $user->contact_mobile }}
                    </div>
                    <div class="form-group">
                        <strong>Contact E-mail: </strong> {{ $user->contact_email }}
                    </div>
                    <div class="form-group">
                        <strong>Phone Number: </strong> {{ $user->phone }}
                    </div>
                    <div class="form-group">
                        <strong>E-mail: </strong> {{ $user->email }}
                    </div>
                </div>
                @if($user->has('company'))
                <div class="col-sm-6">
                    <div class="form-group">
                        <strong>Company Name: </strong> {{ $user->company->name }}
                    </div>
                    <div class="form-group">
                        <strong>Company Address: </strong> {{ $user->company->address }}
                    </div>
                    <div class="form-group">
                        <strong>Post Code: </strong> {{ $user->company->postcode }}
                    </div>
                    <div class="form-group">
                        <strong>Company Number: </strong> {{ $user->company->company_number }}
                    </div>
                    <div class="form-group">
                        <strong>Tax ID: </strong> {{ $user->company->taxID }}
                    </div>
                </div>
                @endif
                <div class="col-sm-12">
                    <hr>
                    <strong>Notes: </strong> {{ $user->company->notes }}
                </div>
            </div>

            @if($user->hasRole('client'))

                @if(!$user->devices->isEmpty())
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Connections</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user->devices as $dev)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $dev->name }}</td>
                                        <td>{{ $dev->type }}</td>
                                        <td>{{ $dev->connections->count() }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif

                @if(!$user->connections->isEmpty())
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>ICCID</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user->connections as $con)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td><a href="{{ url('connections/'.$con->iccid) }}">{{ $con->iccid }}</a></td>
                                        <td>{{ $con->status == 'ACTIVATED' ? $con->status : 'DEACTIVATED' }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endif

            @endif

        </div>
    </div>

@endsection

@section('styles')

@endsection

@section('scripts')

@endsection