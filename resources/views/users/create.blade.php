@extends('layouts/app')

@section('content')

    <div class="container">
        <div class="bg-section">
            <form method="post" action="{{ route($url.'.store' , '.send/email')}}">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}"
                                   placeholder="Company Name">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <textarea name="address" id="address" class="form-control" rows="5"
                                      placeholder="Company Address">{{ old('address') }}</textarea>
                            @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('address') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="postcode" id="postcode" class="form-control"
                                   value="{{ old('postcode') }}" placeholder="Post Code">
                            @if ($errors->has('postcode'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('postcode') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="company_number" id="company_number" class="form-control"
                                   value="{{ old('company_number') }}" placeholder="Company Number">
                            @if ($errors->has('company_number'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('company_number') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="taxID" id="taxID" class="form-control" value="{{ old('taxID') }}"
                                   placeholder="Tax ID">
                            @if ($errors->has('taxID'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('taxID') }}</strong>
                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" name="first_name" id="first_name" class="form-control"
                                   value="{{ old('first_name') }}" placeholder="First Name">
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('first_name') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="last_name" id="last_name" class="form-control"
                                   value="{{ old('last_name') }}" placeholder="Last Name">
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('last_name') }}</strong>
                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="text" name="mobile" id="mobile" class="form-control"
                                   value="{{ old('mobile') }}" placeholder="Contact Mobile">
                            @if ($errors->has('mobile'))
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $errors->first('mobile') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" id="email" class="form-control"
                                   value="{{ old('email') }}" placeholder="Contact Email">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                </span>
                            @endif
                        </div>


                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control"
                                   value="{{ old('password') }}" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('password') }}</strong>
                </span>
                            @endif
                        </div>





                        <div class="form-group">
                            <input type="text" name="account_phone" id="account_phone" class="form-control" value="{{ old('account_phone') }}"
                                   placeholder="Accounts Phone Number">
                            @if ($errors->has('account_phone'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('account_phone') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="account_email" id="account_email" class="form-control" value="{{ old('account_email') }}"
                                   placeholder="Accounts Email">
                            @if ($errors->has('account_email'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('account_email') }}</strong>
                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <textarea name="notes" id="notes" rows="10" class="form-control"
                                      placeholder="Notes">{{ old('notes') }}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-primary" value="Save Client">
                    </div>

                </div>

            </form>
        </div>
    </div>

@endsection

@section('styles')

@endsection

@section('scripts')

@endsection
