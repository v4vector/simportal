@extends('layouts.app')
@section('content')

    <section class="bg-section form-group">
        <div class="container">

            <div class="row overallStats">
                <div class="col-sm-4 col-xs-6">
                    <div class="stat">

                        <div class="stat-value">{{ '£'.number_format($quote->deals->sum('total_deal') + $quote->deals->sum('total_sims_cost'), 2) }}</div>
                        <div class="stat-label">Total Deal Value</div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ '£'.number_format($quote->deals->sum('total_commission'), 2) }}</div>
                        <div class="stat-label">Total Agent Commission</div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <a href="{{ route('quote.send', $quote->id)  }}"
                       class="btn btn-rounded btn-white btn-block form-group">Send</a>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <section class="container">
                    <div class="row">
                        <div class="col-sm-8 bg-section">
                            <form action="{{ route('store.pooled', $quote) }}" method="post" class="form-light">
                                @csrf

                                <div class="">

                                    <h3>Create A Deal</h3>


                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-section-heading">Client Info</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="client_id">Client</label>
                                                <input value="{{ $quote->client->company->name }}" type="text"
                                                       class="form-control" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="order_number">Purchase Order No. (Optional)</label>
                                                <input value="{{ $quote->order_number }}" type="text"
                                                       class="form-control" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-section-heading">Product Requirements</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                @php
                                                    $product_types = \App\Models\ProductType::where('data_type', 'pooled')->get()->pluck('show_name', 'id')->prepend(' -- Select Product Type -- ', '');
                                                @endphp
                                                <label for="product_type_id">Product Type</label>
                                                {{ Form::select('product_type_id', $product_types, old('product_type_id'), ['id' => 'product_type_id', 'class'=>'form-control select2']) }}
                                                @if ($errors->has('product_type_id'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('product_type_id') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="data_type">Data Type</label>
                                                <input type="text" name="data_type" class="form-control" value="Pooled" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="high_data">Data Usage</label>
                                            <div class="button-rad form-group">
                                                <input type="radio" name="data" class="data_usage" id="low_data"
                                                       value="low_data" checked>
                                                <label class="btn btn-primary input-lg" for="low_data">
                                                    Low Data (GB)
                                                </label>
                                                <input type="radio" name="data" class="data_usage" id="high_data"
                                                       value="high_data" {{ old('data') == 'low_data' ? 'checked' : '' }}>
                                                <label class="btn btn-primary input-lg" for="high_data">
                                                    High Data (TB)
                                                </label>
                                            </div>
                                            @if ($errors->has('data'))
                                                <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('data') }}</strong>
                                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">

                                        @php
                                            $networks = \App\Models\Network::get();
                                        @endphp
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="network_preferences">Network Preferences</label>
                                                <select name="network_preferences[]" id="network_preferences"
                                                        class="form-control" multiple>
                                                    @foreach($networks as $net)
                                                        @php
                                                            $select = '';
                                                            if(old('network_preferences') && in_array($net->id, old('network_preferences'))){
                                                                $select = 'selected';
                                                            }
                                                        @endphp
                                                        <option value="{{ $net->id }}" {{ $select }}>{{ $net->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('network_preferences'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('network_preferences') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="data_volume_id">Data Volume</label>
                                                <select name="data_volume_id" id="data_volume_id"
                                                        class="form-control select2">
                                                    <option value=""> -- Select One --</option>
                                                </select>
                                                @if ($errors->has('data_volume_id'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('data_volume_id') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="sim_volume">Sim Volume</label>
                                                {!! Form::number('sim_volume', old('sim_volume'), ['id'=> 'sim_volume', 'class'=>'form-control']) !!}
                                                @if ($errors->has('sim_volume'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('sim_volume') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                @php
                                                    $types = \App\Models\SimType::all()->pluck('name', 'price');
                                                @endphp
                                                <label for="sim_type">Sim Type</label>
                                                {!! Form::select('sim_type', $types, old('sim_type'), ['id'=> 'sim_type', 'class'=>'form-control']) !!}
                                                @if ($errors->has('sim_type'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong class="text-danger small">{{ $errors->first('sim_type') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                @php
                                                    $cons = [1,3,6,12,24,36,48,60];
                                                @endphp
                                                <label for="contract_len">Contract Length (Months)</label>
                                                <input type="text" name="contract_len" id="contract_len" class="form-control" value="12" readonly>
                                                @if ($errors->has('contract_len'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('contract_len') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        @php
                                            $applications = \App\Models\ClientApplication::pluck('name', 'id');
                                        @endphp
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="client_application_id">Client Application</label>
                                                {{ Form::select('client_application_id', $applications, old('client_application_id'), ['id' => 'client_application_id', 'class'=>'form-control select2']) }}
                                                @if ($errors->has('client_application_id'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('client_application_id') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-section-heading">Pricing</div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-12">
                                            <h4>Data Charges</h4>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="base_price">Base Price (£)</label>
                                                <input type="text" name="base_price" id="base_price"
                                                       class="form-control"
                                                       value="{{ old('base_price', 0) }}" readonly>
                                                @if ($errors->has('base_price'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('base_price') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="client_price">Client Price (£)</label>
                                                <input type="number" name="client_price" id="client_price"
                                                       class="form-control"
                                                       value="{{ old('client_price', 0) }}" step="any">
                                                <div id="client_price_small_error" class="text-danger hidden">Can't be
                                                    smaller than Base Price
                                                </div>
                                                @if ($errors->has('client_price'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('client_price') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="margin_sales">Sale Agent Margin (£)</label>
                                                <input type="text" name="margin_sales" min="0" id="margin_sales"
                                                       class="form-control"
                                                       value="{{ old('margin_sales', 0) }}" readonly="readonly">
                                                @if ($errors->has('margin_sales'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('margin_sales') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="margin_admin">Admin Margin (£)</label>
                                                <input type="text" name="margin_admin" min="0" id="margin_admin"
                                                       class="form-control"
                                                       value="{{ old('margin_admin', 0) }}" readonly="readonly">
                                                @if ($errors->has('margin_admin'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('margin_admin') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3 gray">
                                            <div class="bold">Total Commission</div>
                                            <h2>
                                                &pound;<span id="total_commission_html">{{ old('total_commission', 0) }}</span>
                                            </h2>
                                        </div>
                                        <div class="col-sm-3 text-black">
                                            <div class="bold">Deal Value</div>
                                            <h2>&pound;<span id="deal_amount_html">{{ old('deal_amount', 0) }}</span></h2>
                                        </div>
                                        <div class="col-sm-3 text-black">
                                            <div class="bold">VAT</div>
                                            <h2>&pound;<span id="vat_html">{{ old('vat', 0) }}</span></h2>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="bold">Total Price</div>
                                            <h2>&pound;<span id="total_deal_html">{{ old('total_deal', 0) }}</span>
                                            </h2>
                                            <input type="hidden" name="total_commission" id="total_commission"
                                                   value="{{ old('total_commission', 0) }}">
                                            <input type="hidden" name="deal_amount" id="deal_amount"
                                                   value="{{ old('deal_amount', 0) }}">
                                            <input type="hidden" name="vat" id="vat" value="{{ old('vat', 0) }}">
                                            <input type="hidden" name="total_deal" id="total_deal"
                                                   value="{{ old('total_deal', 0) }}">
                                        </div>
                                    </div>
                                    <div class="row one_off_summary">
                                        <div class="col-sm-12">
                                            <h4>One-Off Charges</h4>
                                        </div>
                                        <div class="col-sm-4 gray">
                                            <div class="bold">Number of Sims</div>
                                            <h2><span id="one_off_sims_html">{{ old('sim_volume', 0) }}</span>
                                            </h2>
                                        </div>
                                        <div class="col-sm-4 gray">
                                            <div class="bold">Cost of Sims</div>
                                            <h2>&pound;<span id="one_off_per_sim_cost_html">{{ old('cost_sims', 0) }}</span>
                                            </h2>
                                        </div>
                                        <div class="col-sm-4 gray">
                                            <div class="bold">Cost</div>
                                            <h2>&pound;<span id="one_off_cost_html">{{ old('total_sims_cost', 0) }}</span>
                                            </h2>
                                        </div>
                                        <div class="col-sm-4 gray">
                                            <div class="bold">VAT</div>
                                            <h2>&pound;<span id="one_off_vat_html">{{ old('vat_sims', 0) }}</span>
                                            </h2>
                                        </div>
                                        <div class="col-sm-4 gray">
                                            <div class="bold">Total</div>
                                            <h2>&pound;<span id="one_off_total_html">{{ old('total_sims_cost', 0) }}</span>
                                            </h2>
                                        </div>
                                        <input type="hidden" name="cost_sims" id="cost_sims"
                                               value="{{ old('cost_sims', 0) }}">
                                        <input type="hidden" name="vat_sims" id="vat_sims"
                                               value="{{ old('vat_sims', 0) }}">
                                        <input type="hidden" name="total_sims_cost" id="total_sims_cost"
                                               value="{{ old('total_sims_cost', 0) }}">
                                    </div>
                                    {{--<div class="row">
                                        <div class="col-sm-12">
                                            <div id="for-upfront">
                                                <h4>Total Upfront: <b class="bold"
                                                                      id="total_upfront">${{ old('vat', '0') }}</b></h4>
                                            </div>
                                            <div id="for-monthly" class="hidden">
                                                <h4>Upfront Payment: <b id="advance_month"></b></h4>
                                                <h4><b id="month_install"></b>x Monthly Installment Amount: <b
                                                            id="per_month"></b></h4>
                                                <h4>Total Installment Amount: <b id="total_install"></b></h4>
                                                <input type="hidden" id="installment" name="installment"
                                                       value="{{ old('installment', 0) }}">
                                            </div>
                                            <input type="hidden" name="payment_terms" id="payment_terms"
                                                   value="monthly">
                                        </div>
                                    </div>--}}

                                    <div class="row">
                                        <div class="col-sm-offset-6 col-sm-6 text-right">
                                            <button class="btn btn-rounded btn-primary btn-block form-group">Save
                                                Quote
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group gray bg-section">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Summary</th>
                                            <th>Contract Duration</th>
                                            <th class="text-right">Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $total = 0 @endphp
                                        @foreach($quote->deals as $deal)
                                            @php
                                                $Vat = $deal->vat;
                                                $with_Vat = $deal->total_deal;
                                                $ex_Vat = $with_Vat - $Vat;
                                                $total += $ex_Vat;
                                            @endphp
                                            <tr>
                                                <td>Deal {{ $loop->iteration }}</td>
                                                <td class="text-right">{{ $deal->contract_len }} Months</td>
                                                <td class="text-right bold">£{{ number_format($ex_Vat, 2) }}</td>
                                            </tr>

                                        @endforeach
                                        <tr>
                                            <th colspan="5">Sims:</th>
                                        </tr>
                                        <tr>
                                            <td>Sims Cost</td>
                                            <td class="text-right">{{ $quote->deals->sum('sim_volume') }}x</td>
                                            <td class="text-right bold">&pound;{{ number_format($quote->deals->sum('cost_sims'), 2) }}</td>
                                        </tr>
                                        @php
                                        $total += $quote->deals->sum('cost_sims');
                                        $vat = $quote->deals()->sum('vat') + $quote->deals->sum('vat_sims');
                                        @endphp
                                        @php

                                        @endphp
                                        <tr class="bold text-right">
                                            <td colspan="5">
                                                <div>Total Ex: &pound;{{ number_format($total, 2) }}</div>
                                                <div>VAT: &pound;{{ number_format($vat, 2) }}</div>
                                                <div>Total Inc: &pound;{{ number_format($total + $vat, 2) }}</div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <a href="{{ route('quote.send', $quote->id)  }}"
                                       class="btn btn-rounded btn-primary btn-block form-group">Send</a>
                                    {{--<a id="breakdown-trigger" class="btn btn-primary btn-block">CREATE PAYMENT PLAN</a>--}}
                                </div>
                            </div>
                            <div class="form-group gray bg-section hidden" id="breakdown">
                                <h4>PAYMENT PLAN</h4>

                                @php
                                    $installs = new \Illuminate\Support\Collection();
                                    if($quote->deals){
                                    foreach($quote->deals as $deal){
                                        if($deal->payment_terms == 'monthly'){
                                            $con = $deal->contract_len - 3;

                                            for($i=1; $i<=$con; $i++){
                                                $arr = [
                                                    'month' => 'Month '.$i,
                                                    'install' => $deal->installment,
                                                    'vat' => $deal->vat / $deal->contract_len,
                                                ];
                                                $installs->push($arr);
                                            }
                                        }
                                    }
                                    }
                                    $months = $installs->groupBy(function ($r){
                                        return $r['month'];
                                    })->map(function ($group){
                                        return ['install' => $group->sum('install'), 'vat' => $group->sum('vat')];
                                    });
                                    $groups = $months->groupBy(function ($mon){
                                        return (string) $mon['install'];
                                    });

                                    $total = $initial_vat = $initial_exvat = 0;
                                    foreach($quote->deals as $dl){
                                        if($dl->payment_terms == 'monthly'){
                                            $initial_vat += $dl->vat / ($dl->contract_len) * 3;
                                            $initial_exvat += $dl->deal_amount / ($dl->contract_len) * 3;
                                        } else {
                                            $initial_vat += $dl->vat;
                                            $initial_exvat += $dl->deal_amount;
                                        }
                                    }
                                    //$initial_exvat = $quote->deals->sum('advance_payment') - $initial_vat;
                                    $total += $initial_total = $initial_vat + $initial_exvat;
                                @endphp
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Payment Plan</th>
                                            <th>Ex VAT</th>
                                            <th>VAT</th>
                                            <th>Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <th colspan="5">Initial Payment:</th>
                                        </tr>
                                        <tr>
                                            <td>Data Cost</td>
                                            <td>£{{ number_format($initial_exvat, 2) }}</td>
                                            <td>£{{ number_format($initial_vat, 2) }}</td>
                                            <td class="bold">£{{ number_format($initial_total, 2) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Sims Cost</td>
                                            <td>£{{ number_format($quote->deals->sum('cost_sims'), 2) }}</td>
                                            <td>£{{ number_format($quote->deals->sum('vat_sims'), 2) }}</td>
                                            <td>
                                                <strong>£{{ number_format($quote->deals->sum('total_sims_cost'), 2) }}</strong>
                                            </td>
                                        </tr>
                                        @if(!$installs->isEmpty())
                                            <tr>
                                                <th colspan="4">Monthly Installments:</th>
                                            </tr>
                                            @foreach($groups as $install => $group)
                                                @php
                                                    $ex_vat = $group->first()['install'] - $group->first()['vat'];
                                                    $vat = $group->first()['vat'];
                                                    $total += ($ex_vat + $vat);
                                                @endphp
                                                <tr>
                                                    <td>{{ $group->count().' months at' }}</td>
                                                    <td>
                                                        £{{ number_format($ex_vat, 2) }}</td>
                                                    <td>£{{ number_format($vat, 2) }}</td>
                                                    <td class="bold">£{{ number_format(($ex_vat + $vat), 2) }}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <form method="post" action="{{ route('save.paymentplan', $quote) }}">
                                    @csrf
                                    @method('put')

                                    @if(!$installs->isEmpty())
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="payment_plan" value="monthly"
                                                       id="payment_plan">
                                                Use Payment Plan
                                            </label>
                                        </div>
                                    @endif
                                    <button type="submit" class="btn btn-rounded btn-white btn-block form-group">Save
                                    </button>
                                </form>
                            </div>

                            @foreach($quote->deals as $deal)
                                <div class="deal-info bg-section form-group">
                                    <h3>{{ 'Deal '.$loop->iteration }} <a
                                                href="{{ url('quote/'.$quote->id.'/deal/'.$deal->id.'/edit') }}"><i
                                                    class="fa fa-edit pull-right"></i></a></h3>

                                    <div class="offer-details gray">
                                        <h4>REQUIREMENTS</h4>
                                        <div>Product Type: {{ $deal->product_type['name'] }}</div>
                                        <div>Data Type: {{ $deal->data_type }}</div>
                                        <div>Data Required: {{ $deal->volume->name }}</div>
                                        <div>Sim volume: {{ $deal->sim_volume }}</div>
                                        <div>Contract Length: {{ $deal->contract_len. ' Month' }}</div>
                                        <div>Client
                                            Application: {{ $deal->client_application['name'] }}</div>
                                    </div>
                                </div>
                            @endforeach

                            {{--<button class="btn btn-rounded btn-primary btn-block">Send Quote</button>--}}
                        </div>
                    </div>
                </section>

            </div>
        </div>

    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.min.js') }}"></script>
    <script>
        //        Get volumes from database based on type
        function get_data_volume(type) {
            var select_box = $('#data_volume_id');
            var url = '{{ url('get-data-volume') }}/pooled/' + type;
            var old = '{{ old('data_volume_id') }}';
            $.get(url, function (data) {
                var options = '';
                $.each(data, function (i, item) {
                    var select = '';
                    if (old == item['id']) {
                        select = 'selected';
                    }
                    options += '<option value="' + item['id'] + '"' + select + '>' + item['name'] + '</option>';
                });
                select_box.html(options)
            });
        }

        function set_terms() {
            var terms = $('.payment_terms:checked').val();
            if (terms === 'monthly') {
                $('#for-upfront').addClass('hidden');
                $('#for-monthly').removeClass('hidden');
            } else {
                $('#for-monthly').addClass('hidden');
                $('#for-upfront').removeClass('hidden');
            }
        }

        function calculate_package() {

            var product_type_id = $('#product_type_id').val();
            var data_usage = $('.data_usage:checked').val();
            var data_volume_id = Number($('#data_volume_id').val());

            if (!product_type_id || !data_usage || !data_volume_id) {
                return false;
            }

            var price_url = '{{ url('get-quote-price') }}/pooled/' + product_type_id + '/' + data_usage;
            var price = 0;
            $.ajax({
                url: price_url,
                async: false,
                success: function (obt_price) {
                    price = obt_price;
                }
            });

            var volume_url = '{{ url('get-volume') }}/pooled/' + data_volume_id;
            var volume = 0;
            $.ajax({
                url: volume_url,
                async: false,
                success: function (obt_vol) {
                    volume = obt_vol.volume;
                }
            });

            var base_price = volume * price;
            $('#base_price').val(base_price.toFixed(2)).change();

            $('#client_price').attr('min', base_price.toFixed(2)).val(base_price).change();
        }

        function calculate_margin() {
//            var sims = Number($('#sim_volume').val());
//            var contract_len = Number($('#contract_len').val());
            var base_price = Number($('#base_price').val());
            var client_price = Number($('#client_price').val());

            var total_margin = 0;
            if (client_price > base_price) {
                total_margin = client_price - base_price;
            }

            var unit_margin = total_margin / 2;

            $('#margin_sales').val(unit_margin.toFixed(2));
            $('#margin_admin').val(unit_margin.toFixed(2));
            $('#total_commission_html').html(unit_margin.toFixed(2));
            $('#total_commission').val(unit_margin.toFixed(2));
        }

        function calculate_summary() {
            var client_price = Number($('#client_price').val());

            if (Number($('#base_price').val()) > client_price) {
                client_price = 0;
            }

            var deal_val = client_price; //Removed Sims from here
            var vat = (20 / 100) * deal_val;
            var total_deal_val = deal_val + vat;

            $('#deal_amount_html').html(deal_val.toFixed(2));
            $('#vat_html').html(vat.toFixed(2));
            $('#total_deal_html').html(total_deal_val.toFixed(2));
            $('#vat').val(vat);
            $('#deal_amount').val(deal_val);
            $('#total_deal').val(total_deal_val);
            $('#total_upfront').html('£' + total_deal_val.toFixed(2));

            var monthly = total_deal_val / contract_len;
            var advance = 3 * monthly;

            $('#per_month').html('£' + monthly.toFixed(2));
            $('#installment').val(monthly.toFixed(2));

            $('#advance_payment').val(advance.toFixed(2));
            $('#advance_month').html('£' + advance.toFixed(2));

            var installment_duration = contract_len - 3;
            var total_installment_amount = installment_duration * monthly;
            $('#month_install').html(installment_duration);
            $('#total_install').html('£' + total_installment_amount.toFixed(2));
            $('#monthly').attr('checked');
        }

        $(document).ready(function (e) {
            var currency = '£';
            $('.select2').select2();

            var type = '{{ old('data') ?? 'low_data' }}';
            get_data_volume(type);

            var select_box = $("#network_preferences");
            select_box.select2();
            $('#product_type_id').on('change', function () {
                var type = $(this).val();
                if (type == 6) {
                    select_box.removeAttr('multiple');
                } else {
                    select_box.attr('multiple', 'multiple');
                }
                select_box.select2("destroy");
                select_box.select2();
            });

//            New Code Here
            $('#product_type_id, #data_volume_id').on('change', function (e) {
                e.preventDefault();
                calculate_package();
            });


            $('#contract_len').on('change', function (e) {
                if ($(this).val() <= 3) {
                    $('#for-upfront').removeClass('hidden');
                    $('#for-monthly').addClass('hidden');
                    $('#payment_terms').val('upfront');
                    $('#monthly').attr('disabled', 'disabled');
                } else {
                    $('#for-upfront').addClass('hidden');
                    $('#for-monthly').removeClass('hidden');
                    $('#payment_terms').val('monthly');
                    $('#monthly').removeAttr('disabled');
                }
                calculate_summary();
            });

//            If radio button for data usage changes, get its options from db
            $('.data_usage').on('change', function () {
                var type = $(this).val();
                get_data_volume(type);
                calculate_package();
            });
            $('#sim_volume, #data_volume_id').on('keyup change', function (e) {
                calculate_package();
            });
            $('#client_price').on('change keyup', function () {
                var base_price = Number($('#base_price').val());
                if ($(this).val() >= base_price) {
                    $('#client_price_small_error').addClass('hidden');
                    calculate_margin();
                    calculate_summary();
//                    set_terms();
                } else {
                    $('#client_price_small_error').removeClass('hidden');
                    calculate_margin();
                    calculate_summary();
//                    set_terms();
                }

            });

            $('#breakdown-trigger').on('click', function (e) {
                e.preventDefault();
                $('#breakdown').toggleClass('hidden');
            });
            $('#sim_volume, #sim_type').on('change keyup', function () {
                var sims = Number($('#sim_volume').val());
                var type = Number($('#sim_type').val());
                if (sims >= 1) {
                    $('#one_off_sims_html').html(sims);
                    $('#one_off_per_sim_cost_html').html(type);
                }
                if (sims >= 1 && type) {
                    var cost = (sims * 0.50) + ((sims * 2.50) * 12);
                    var VAT = cost * 0.20;
                    var total = cost + VAT;

                    $('#one_off_cost_html').html(cost.toFixed(2));
                    $('#one_off_vat_html').html(VAT.toFixed(2));
                    $('#one_off_total_html').html(total.toFixed(2));
                }
            });
        });
    </script>
@endsection

{{--((number of sims * data per sim projected)+ (Number of sims*connection costs)) *Contract Duration)+(one off manufacture sim cost)--}}