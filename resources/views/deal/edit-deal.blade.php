@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">
                    <form action="{{ url('quote/'.$quote->id.'/deal/'.$deal->id) }}" method="post" class="form-light">
                        @method('put')
                        @csrf
                        <section class="container">
                            <div class="row" style="margin-top: 30px">
                                <div class="col-sm-8">
                                    <div class="bg-section">

                                        <h3>Edit Deal</h3>


                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-section-heading">Client Info</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="client_id">Client</label>
                                                    <input value="{{ $quote->client->full_name }}" type="text"
                                                           class="form-control" disabled="disabled">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="order_number">Purchase Order No. (Optional)</label>
                                                    <input value="{{ $quote->order_number }}" type="text"
                                                           class="form-control" disabled="disabled">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-section-heading">Product Requirements</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    @php
                                                        $product_types = \App\Models\ProductType::get()->pluck('show_name', 'id')->prepend(' -- Select Product Type -- ', '');
                                                    @endphp
                                                    <label for="product_type_id">Product Type</label>
                                                    {{ Form::select('product_type_id', $product_types, old('product_type_id', $deal->product_type_id), ['id' => 'product_type_id', 'class'=>'form-control select2']) }}
                                                    @if ($errors->has('product_type_id'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('product_type_id') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="data_type">Data Type</label>
                                                    {{ Form::select('data_type', ['' => ' -- Select One --', 'Pooled' => 'Pooled', 'Individual' => 'Individual'], old('data_type', $deal->data_type), ['id' => 'data_type', 'class'=>'form-control select2']) }}
                                                    @if ($errors->has('data_type'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('data_type') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label for="high_data">Data Usage</label>
                                                <div class="button-rad form-group">
                                                    <input type="radio" name="data" class="data_usage" id="high_data"
                                                           value="high_data" checked>
                                                    <label class="btn btn-primary input-lg" for="high_data">
                                                        High Data
                                                    </label>
                                                    <input type="radio" name="data" class="data_usage" id="low_data"
                                                           value="low_data" {{ old('data', $deal->data) == 'low_data' ? 'checked' : '' }}>
                                                    <label class="btn btn-primary input-lg" for="low_data">
                                                        Low Data
                                                    </label>
                                                </div>
                                                @if ($errors->has('data'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('data') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="row">

                                            @php
                                                $networks = \App\Models\Network::get();
                                            @endphp
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="network_preferences">Network Preferences</label>
                                                    <select name="network_preferences[]" id="network_preferences"
                                                            class="form-control" multiple>
                                                        @foreach($networks as $net)
                                                            @php
                                                                $select = '';
                                                                if(old('network_preferences', $deal->network_preferences) && in_array($net->id, old('network_preferences', explode(',',$deal->network_preferences)))){
                                                                    $select = 'selected';
                                                                }
                                                            @endphp
                                                            <option value="{{ $net->id }}" {{ $select }}>{{ $net->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('network_preferences'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('network_preferences') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="data_volume_id">Data Volume</label>
                                                    <select name="data_volume_id" id="data_volume_id"
                                                            class="form-control select2">
                                                        <option value=""> -- Select One --</option>
                                                    </select>
                                                    @if ($errors->has('data_volume_id'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('data_volume_id') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="sim_volume">Sim Volume</label>
                                                    {!! Form::number('sim_volume', old('sim_volume', $deal->sim_volume), ['id'=> 'sim_volume', 'class'=>'form-control']) !!}
                                                    @if ($errors->has('sim_volume'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('sim_volume') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    @php
                                                        $types = \App\Models\SimType::all()->pluck('name', 'price');
                                                    @endphp
                                                    <label for="sim_type">Sim Type</label>
                                                    {!! Form::select('sim_type', $types, old('sim_type'), ['id'=> 'sim_type', 'class'=>'form-control']) !!}
                                                    @if ($errors->has('sim_type'))
                                                        <span class="invalid-feedback" role="alert">
                                                        <strong class="text-danger small">{{ $errors->first('sim_type') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    @php
                                                        $cons = [1,3,6,12,24,36,48,60];
                                                    @endphp
                                                    <label for="contract_len">Contract Length</label>
                                                    <select name="contract_len" id="contract_len"
                                                            class="form-control select2">
                                                        @foreach($cons as $con)
                                                            @php
                                                                $select = old('contract_len', $deal->contract_len) == $con ? 'selected' : '';
                                                            @endphp
                                                            <option value="{{ $con }}" {{ $select }}>{{ $con.' Months' }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('contract_len'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('contract_len') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            @php
                                                $applications = \App\Models\ClientApplication::pluck('name', 'id');
                                            @endphp
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="client_application_id">Client Application</label>
                                                    {{ Form::select('client_application_id', $applications, old('client_application_id' ,$deal->client_application_id), ['id' => 'client_application_id', 'class'=>'form-control select2']) }}
                                                    @if ($errors->has('client_application_id'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('client_application_id') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-section-heading">Pricing</div>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="base_price">Base Price (£)</label>
                                                    <input type="text" name="base_price" id="base_price"
                                                           class="form-control"
                                                           value="{{ old('base_price', $deal->base_price) }}" readonly>
                                                    @if ($errors->has('base_price'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('base_price') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="client_price">Client Price (£)</label>
                                                    <input type="text" name="client_price" id="client_price"
                                                           class="form-control"
                                                           value="{{ old('client_price', $deal->client_price) }}">
                                                    @if ($errors->has('client_price'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('client_price') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="margin_sales">Sale Agent Margin (£)</label>
                                                    <input type="text" name="margin_sales" min="0" id="margin_sales"
                                                           class="form-control"
                                                           value="{{ old('margin_sales', $deal->margin_sales) }}" readonly="readonly">
                                                    @if ($errors->has('margin_sales'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('margin_sales') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="margin_admin">Admin Margin (£)</label>
                                                    <input type="text" name="margin_admin" min="0" id="margin_admin"
                                                           class="form-control"
                                                           value="{{ old('margin_admin', $deal->margin_admin) }}" readonly="readonly">
                                                    @if ($errors->has('margin_admin'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('margin_admin') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="commission">Commission</label>
                                                </div>
                                            </div>
                                        </div>--}}
                                        {{--<div class="row">
                                            <div class="col-sm-12">
                                                <label for="upfront">Payment Terms</label>
                                                <div class="button-rad form-group">
                                                    <input type="radio" name="payment_terms" class="payment_terms"
                                                           id="upfront"
                                                           value="upfront" checked>
                                                    <label class="btn btn-primary input-lg" for="upfront">
                                                        Upfront Payment
                                                    </label>
                                                    <input type="radio" name="payment_terms" class="payment_terms"
                                                           id="monthly"
                                                           value="monthly">
                                                    <label class="btn btn-primary input-lg" for="monthly">
                                                        Monthly Installments
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="monthly_installments">Monthly Installments</label>
                                                    {!! Form::select('monthly_installments', ['a'=> 'a'], old('monthly_installments', $quote->monthly_installments), ['id'=> 'monthly_installments', 'class'=>'form-control hidden']) !!}
                                                </div>
                                            </div>
                                        </div>--}}
                                        <div class="row form-group">
                                            <div class="col-sm-3 gray">
                                                <div class="bold">Total Commission</div>
                                                <h2>
                                                    £<span id="total_commission_html">{{ old('total_commission', $deal->total_commission) }}</span>
                                                </h2>
                                            </div>
                                            <div class="col-sm-3 text-black">
                                                <div class="bold">Deal Value</div>
                                                <h2>£<span id="deal_amount_html">{{ old('deal_amount', $deal->deal_amount) }}</span></h2>
                                            </div>
                                            <div class="col-sm-3 text-black">
                                                <div class="bold">VAT</div>
                                                <h2>£<span id="vat_html">{{ old('vat', $deal->vat) }}</span></h2>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="bold">Total Price</div>
                                                <h2>£<span
                                                            id="total_deal_html">{{ old('total_deal', $deal->total_deal) }}</span>
                                                </h2>
                                                <input type="hidden" name="total_commission" id="total_commission"
                                                       value="{{ old('total_commission', $deal->total_commission) }}">
                                                <input type="hidden" name="deal_amount" id="deal_amount"
                                                       value="{{ old('deal_amount', $deal->deal_amount) }}">
                                                <input type="hidden" name="vat" id="vat" value="{{ old('vat', $deal->vat) }}">
                                                <input type="hidden" name="total_deal" id="total_deal"
                                                       value="{{ old('total_deal', $deal->total_deal) }}">
                                            </div>
                                        </div>
                                        <div class="row one_off_summary">
                                            <div class="col-sm-12">
                                                <h4>One Off Charges</h4>
                                            </div>
                                            <div class="col-sm-3 gray">
                                                <div class="bold">Number of Sims</div>
                                                <h2>£<span id="one_off_volume_html">{{ old('sim_volume', $deal->sim_volume) }}</span>
                                                </h2>
                                            </div>
                                            <div class="col-sm-3 gray">
                                                <div class="bold">Cost of Sims</div>
                                                <h2>£<span id="one_off_cost_html">{{ old('cost_sims', $deal->cost_sims) }}</span>
                                                </h2>
                                            </div>
                                            <div class="col-sm-3 gray">
                                                <div class="bold">VAT</div>
                                                <h2>£<span id="one_off_vat_html">{{ old('vat_sims', $deal->vat_sims) }}</span>
                                                </h2>
                                            </div>
                                            <div class="col-sm-3 gray">
                                                <div class="bold">Total</div>
                                                <h2>£<span id="one_off_total_html">{{ old('total_sims_cost', $deal->total_sims_cost) }}</span>
                                                </h2>
                                            </div>
                                            <input type="hidden" name="cost_sims" id="cost_sims" value="{{ old('cost_sims', $deal->cost_sims) }}">
                                            <input type="hidden" name="vat_sims" id="vat_sims" value="{{ old('vat_sims', $deal->vat_sims) }}">
                                            <input type="hidden" name="total_sims_cost" id="total_sims_cost" value="{{ old('total_sims_cost', $deal->total_sims_cost) }}">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="for-upfront" class="{{ $deal->payment_terms == 'monthly' ? 'hidden' : '' }}">
                                                    <h4>Total Upfront: <b class="bold" id="total_upfront">£{{ old('vat', '0') }}</b></h4>
                                                </div>
                                                <div id="for-monthly" class="{{ $deal->payment_terms == 'monthly' ? '' : 'hidden' }}">
                                                    <h4>Upfront Payment: <b id="advance">£{{ old('advance_payment', $deal->advance_payment) }}</b></h4>
                                                    <h4><b id="month_install">{{ $deal->contract_len - 3 }}</b>x Monthly  Installment Amount: <b id="per_month">£{{ old('installment', $deal->installment) }}</b></h4>
                                                    <h4>Total Installment Amount: <b id="total_install">£{{ ($deal->contract_len - 3) * $deal->installment }}</b></h4>

                                                    <input type="hidden" id="advance_payment" name="advance_payment" value="{{ old('advance_payment', $deal->advance_payment) }}">
                                                    <input type="hidden" id="installment" name="installment" value="{{ old('installment', $deal->installment) }}">
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="row">
                                            <div class="col-sm-12">
                                                <label for="upfront">Payment Terms</label>
                                                <div class="button-rad form-group">
                                                    <input type="radio" name="payment_terms" class="payment_terms"
                                                           id="upfront"
                                                           value="upfront" checked>
                                                    <label class="btn btn-primary input-lg" for="upfront">
                                                        Upfront Payment
                                                    </label>
                                                    <input type="radio" name="payment_terms" class="payment_terms"
                                                           id="monthly"
                                                           value="monthly" {{ (old('payment_terms')=='monthly' || $deal->payment_terms == 'monthly') ? 'checked' : '' }}>
                                                    <label class="btn btn-primary input-lg" for="monthly">
                                                        Monthly Installments
                                                    </label>
                                                </div>
                                            </div>
                                        </div>--}}

                                        <div class="row">
                                            <div class="col-sm-offset-6 col-sm-6 text-right">
                                                <button class="btn btn-rounded btn-primary btn-block form-group">Update Quote
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="">
                                        @foreach($quote->deals as $deel)
                                            <div class="deal-info bg-section form-group">
                                                <h3>{{ 'Deal '.$loop->iteration }}</h3>

                                                <div class="offer-details gray">
                                                    <h4>REQUIREMENTS</h4>
                                                    <div>Product Type: {{ $deel->product_type['name'] }}</div>
                                                    <div>Data Type: {{ $deel->data_type }}</div>
                                                    <div>Data Required: {{ $deel->volume->name }}</div>
                                                    <div>Sim Volume: {{ $deel->sim_volume }}</div>
                                                    <div>Contract Length: {{ $deel->contract_len. ' Month' }}</div>
                                                    <div>Client
                                                        Application: {{ $deel->client_application['name'] }}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}">
    <style>
        .select2-container--default .select2-selection {
            min-height: 46px;
        }

        .select2-container--default .select2-selection .select2-selection__rendered {
            line-height: 46px;
        }

        .select2-container--default .select2-selection .select2-selection__arrow {
            min-height: 46px;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.min.js') }}"></script>
    <script>
        //        Get volumes from database based on type
        function get_data_volume(type) {
            var select_box = $('#data_volume_id');
            var url = '{{ url('get-data-volume') }}' + '/' + type;
            var old = '{{ old('data_volume_id', $deal->data_volume_id) }}';
            $.get(url, function (data) {
                var options = '';
                $.each(data, function (i, item) {
                    var select = '';
                    if (old == item['id']) {
                        select = ' selected';
                    }
                    options += '<option value="' + item['id'] + '"' + select + '>' + item['name'] + '</option>';
                });
                console.log(options);
                select_box.html(options)
            });
        }

        function set_terms() {
            var terms = $('.payment_terms:checked').val();
            if (terms === 'monthly') {
                $('#for-upfront').addClass('hidden');
                $('#for-monthly').removeClass('hidden');
            } else {
                $('#for-monthly').addClass('hidden');
                $('#for-upfront').removeClass('hidden');
            }
        }

        function calculate_package() {

            var product_type_id = $('#product_type_id').val();
            var data_usage = $('.data_usage:checked').val();
            var data_volume_id = Number($('#data_volume_id').val());

            if (!product_type_id || !data_usage || !data_volume_id) {
                return false;
            }

            var price_url = '{{ url('get-quote-price') }}' + '/' + product_type_id + '/' + data_usage;
            var price = 0;
            $.ajax({
                url: price_url,
                async: false,
                success: function (obt_price) {
                    price = obt_price;
                }
            });

            var volume_url = '{{ url('get-volume') }}' + '/' + data_volume_id;
            var volume = 0;
            $.ajax({
                url: volume_url,
                async: false,
                success: function (obt_vol) {
                    volume = obt_vol.volume;
                }
            });

            var base_price = volume * price;
            $('#base_price').val(base_price.toFixed(2)).change();

            $('#client_price').attr('min', base_price.toFixed(2)).val(base_price).change();
        }

        function calculate_margin() {
            var sims = Number($('#sim_volume').val());
            var contract_len = Number($('#contract_len').val());
            var base_price = Number($('#base_price').val());
            var client_price = Number($('#client_price').val());
            if (client_price > base_price) {
                var total_margin = client_price - base_price;
            } else {
                var total_margin = 0;
            }

            var unit_margin = total_margin / 2;
            var total_commission = (unit_margin * sims * contract_len);

            $('#margin_sales').val(unit_margin.toFixed(2));
            $('#margin_admin').val(unit_margin.toFixed(2));
            $('#total_commission_html').html(total_commission.toFixed(2));
            $('#total_commission').val(total_commission.toFixed(2));
        }

        function calculate_summary() {
            var sims = parseFloat($('#sim_volume').val());
            var client_price = Number($('#client_price').val());
            var contract_len = Number($('#contract_len').val());

            if (Number($('#base_price').val()) > client_price) {
                client_price = 0;
            }

            var deal_val = sims * client_price * contract_len;
            var vat = (20 / 100) * deal_val;
            var total_deal_val = deal_val + vat;

            $('#deal_amount_html').html(deal_val.toFixed(2));
            $('#vat_html').html(vat.toFixed(2));
            $('#total_deal_html').html(total_deal_val);
            $('#vat').val(vat);
            $('#deal_amount').val(deal_val);
            $('#total_deal').val(total_deal_val);
            $('#total_upfront').html('£' + total_deal_val.toFixed(2));

            var monthly = total_deal_val / contract_len;
            var advance = 3 * monthly;

            $('#per_month').html('£' + monthly.toFixed(2));
            $('#installment').val(monthly.toFixed(2));

            $('#advance_payment').val(advance.toFixed(2));
            $('#advance_month').html('£' + advance.toFixed(2));

            var installment_duration = contract_len - 3;
            var total_installment_amount = installment_duration * monthly;
            $('#month_install').html(installment_duration);
            $('#total_install').html('£' + total_installment_amount.toFixed(2));
            $('#monthly').attr('checked');
        }

        $(document).ready(function (e) {
            var currency = '£';
            $('.select2').select2();

            var type = '{{ old('data', $deal->data) }}';
            get_data_volume(type);

            var select_box = $("#network_preferences");
            select_box.select2();
            $('#product_type_id').on('change', function () {
                var type = $(this).val();
                if (type == 1) {
                    select_box.removeAttr('multiple');
                } else {
                    select_box.attr('multiple', 'multiple');
                }
                select_box.select2("destroy");
                select_box.select2();
            });

//            New Code Here
            $('#product_type_id, #data_volume_id').on('change', function (e) {
                e.preventDefault();
                calculate_package();
            });


            $('#contract_len').on('change', function (e) {
                if ($(this).val() <= 3) {
                    $('#for-upfront').removeClass('hidden');
                    $('#for-monthly').addClass('hidden');
                    $('#payment_terms').val('upfront');
                    $('#monthly').attr('disabled', 'disabled');
                } else {
                    $('#for-upfront').addClass('hidden');
                    $('#for-monthly').removeClass('hidden');
                    $('#payment_terms').val('monthly');
                    $('#monthly').removeAttr('disabled');
                }
                calculate_summary();
            });

//            If radio button for data usage changes, get its options from db
            $('.data_usage').on('change', function () {
                var type = $(this).val();
                get_data_volume(type);
                calculate_package();
            });
            $('#sim_volume, #data_volume_id').on('keyup change', function (e) {
                calculate_package();
            });
            $('#client_price').on('change keyup', function () {
                var base_price = Number($('#base_price').val());
                if ($(this).val() >= base_price) {
                    $('#client_price_small_error').addClass('hidden');
                    calculate_margin();
                    calculate_summary();
//                    set_terms();
                } else {
                    $('#client_price_small_error').removeClass('hidden');
                    calculate_margin();
                    calculate_summary();
//                    set_terms();
                }

            });

            $('#breakdown-trigger').on('click', function (e) {
                e.preventDefault();
                $('#breakdown').toggleClass('hidden');
            });
            $('#sim_volume, #sim_type').on('change keyup', function () {
                var vol = Number($('#sim_volume').val());
                var type = Number($('#sim_type').val());
                if (vol >= 1) {
                    $('#one_off_volume_html').html(vol);
                }
                if (vol >= 1 && type) {
                    var cost = vol * type;
                    $('#one_off_cost_html').html(cost.toFixed(2));

                    var VAT = (20 / 100) * cost;
                    var total = cost + VAT;

                    $('#one_off_vat_html').html(VAT.toFixed(2));
                    $('#one_off_total_html').html(total.toFixed(2));
                    $('#cost_sims').val(cost);
                    $('#vat_sims').val(VAT);
                    $('#total_sims_cost').val(total);
                }
            });


//            Changes Show here
            /*$('#product_type_id').on('change', function () {
                var option = $(this).find(':selected').text();
                $('#product_type_text').text(option);
            });
            $('#data_type').on('change', function () {
                $('#data_type_text').text($(this).val());
            });
            $('#contract_len').on('change', function () {
                $('#contract_length_text').text($(this).val() + ' Months');
            });
            $('#client_application_id').on('change', function () {
                var option = $(this).find(':selected').text();
                $('#client_application_text').text(option);
            });
            $('#data_volume_id').on('change', function () {
                var option = $(this).find(':selected').text();
                $('#data_required_text').text(option);
                $('#data_required_text').text(option);
            });*/
        });
    </script>
@endsection

{{--((number of sims * data per sim projected)+ (Number of sims*connection costs)) *Contract Duration)+(one off manufacture sim cost)--}}