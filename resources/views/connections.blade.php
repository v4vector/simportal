@extends('layouts/app')

@section('content')

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#connections-tab">Connections</a></li>
                        <li><a data-toggle="pill" href="#devices-tab">Devices</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="container">
        <div class="bg-section">
            <div class="row">
                <div class="col-sm-12">
                    <div class="tab-content">
                        <div id="connections-tab" class="tab-pane fade in active">
                            <div class="table-responsive">
                                <table class="table table-flex dataTable" id="connections_table">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div class="icheck-primary">
                                                <input type="checkbox" id="connections_head"/>
                                                <label for="connections_head"></label>
                                            </div>
                                        </th>
                                        <th></th>
                                        <th>Status</th>
                                        <th>ICCID</th>
                                        <th>Usage (MB)</th>
                                        <th>Network</th>
                                        <th>Date Added</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($details as $device)
                                        @php
                                        $status = $device->status == 'ACTIVATED' ? 'text-success' : 'text-danger';
                                        $checkbox = $device->status == 'ACTIVATED' ? 'checked' : '';
                                        $usage = $usages->where('iccid', $device->iccid)->first();
                                        @endphp
                                        <tr>
                                            <td>
                                                <div class="icheck-primary">
                                                    <input type="checkbox" id="{{ $loop->iteration }}"/>
                                                    <label for="{{ $loop->iteration }}"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="checkbox" class="js-switch" {{ $checkbox }}>
                                            </td>
                                            <td><i class="mdi mdi-fiber-manual-record {{ $status }}"></i></td>
                                            <td><a href="{{ url('connections/'.$device->iccid) }}">{{ $device->iccid }}</a></td>
                                            <td>{{ $usage->ctdDataUsage }}</td>
                                            <td>EE</td>
                                            <td>{{ date('d/m/Y H:i', strtotime($device->dateAdded)) }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <a class="btn btn-link dropdown-toggle" type="button" id="nh_5_dd"
                                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                        <i class="mdi mdi-more-horiz"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_5_dd">
                                                        <li><a href="#">Action</a></li>
                                                        <li><a href="#">Another action</a></li>
                                                        <li><a href="#">Something else here</a></li>
                                                        <li role="separator" class="divider"></li>
                                                        <li><a href="#">Separated link</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="devices-tab" class="tab-pane fade">
                            <div class="table-responsive">
                                <table class="table table-flex dataTable" id="devices_table">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div class="icheck-primary">
                                                <input type="checkbox" id="devices_head"/>
                                                <label for="devices_head"></label>
                                            </div>
                                        </th>
                                        <th>Name</th>
                                        <th>Usage (MB)</th>
                                        <th>Type</th>
                                        <th>Date Added</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="icheck-primary">
                                                <input class="select_device" type="checkbox" id="nh_1"/>
                                                <label for="nh_1"></label>
                                            </div>
                                        </td>
                                        <td><a href="{{ url('devices/details') }}">NG Nighthawk 1</a></td>
                                        <td>800.23</td>
                                        <td>Router</td>
                                        <td>01/08/2018 12:23</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="btn btn-link dropdown-toggle" type="button" id="nh_1_dd"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <i class="mdi mdi-more-horiz"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_1_dd">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="icheck-primary">
                                                <input class="select_device" type="checkbox" id="nh_2"/>
                                                <label for="nh_2"></label>
                                            </div>
                                        </td>
                                        <td><a href="{{ url('devices/details') }}">NG Nighthawk 2</a></td>
                                        <td>10,362.81</td>
                                        <td>Router</td>
                                        <td>01/08/2018 12:23</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="btn btn-link dropdown-toggle" type="button" id="nh_2_dd"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <i class="mdi mdi-more-horiz"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_2_dd">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="icheck-primary">
                                                <input class="select_device" type="checkbox" id="nh_3"/>
                                                <label for="nh_3"></label>
                                            </div>
                                        </td>
                                        <td><a href="{{ url('devices/details') }}">NG Nighthawk 3</a></td>
                                        <td>1,464.28</td>
                                        <td>Router</td>
                                        <td>01/08/2018 12:23</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="btn btn-link dropdown-toggle" type="button" id="nh_3_dd"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <i class="mdi mdi-more-horiz"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_3_dd">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="icheck-primary">
                                                <input class="select_device" type="checkbox" id="nh_4"/>
                                                <label for="nh_4"></label>
                                            </div>
                                        </td>
                                        <td><a href="{{ url('devices/details') }}">NG Nighthawk 4</a></td>
                                        <td>12,476.37</td>
                                        <td>Router</td>
                                        <td>01/08/2018 12:23</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="btn btn-link dropdown-toggle" type="button" id="nh_4_dd"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <i class="mdi mdi-more-horiz"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_4_dd">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="icheck-primary">
                                                <input class="select_device" type="checkbox" id="nh_5"/>
                                                <label for="nh_5"></label>
                                            </div>
                                        </td>
                                        <td><a href="{{ url('devices/details') }}">NG Nighthawk 5</a></td>
                                        <td>3,834.32</td>
                                        <td>Router</td>
                                        <td>01/08/2018 12:23</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="btn btn-link dropdown-toggle" type="button" id="nh_5_dd"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <i class="mdi mdi-more-horiz"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_5_dd">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/switchery/dist/switchery.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.js') }}"></script>
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
@endsection
