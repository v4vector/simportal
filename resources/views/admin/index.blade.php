@extends('layouts/app')

@section('content')
    <div class="container">
        <div class="bg-section">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="row">
                @role('superuser')
                <div class="col-sm-12 form-group text-right">
                    <a href="{{ route('admin.create') }}" class="btn btn-primary">Create Admin</a>
                </div>
                @endrole
                <div class="col-sm-12">
                    @if($users->isEmpty() == false)
                        <div class="table-responsive">
                            <table class="table dataTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $user->full_name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->mobile }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="btn btn-link dropdown-toggle" type="button" id="nh_5_dd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <i class="mdi mdi-more-horiz"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_5_dd">
                                                    <li><a href="{{ route('admin.edit', ['id' => $user->id]) }}">Edit</a></li>
                                                    <li><a href="#" onclick="event.preventDefault();
                                                        document.getElementById('deleteForm{{ $user->id }}').submit();">Delete</a></li>
                                                    <form style="display: none" id="deleteForm{{ $user->id }}" action="{{ route('admin.destroy', ['id' => $user->id]) }}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                    </form>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-warning">
                            <strong>Not Found!</strong> You can add new users <a href="{{ route($url.'.create') }}">here</a>.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/switchery/dist/switchery.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.js') }}"></script>
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
@endsection
