@extends('layouts/app')

@section('content')

    <div class="container" style="margin-top: 10px;">
        <div class="bg-section">
            <h2>Create Admin</h2>
            <form method="post" action="{{ route('admin.store') }}">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" name="first_name" id="first_name" class="form-control"
                                   value="{{ old('first_name') }}"
                                   placeholder="First Name">
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('first_name') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="last_name" id="last_name" class="form-control"
                                   placeholder="Last Name" value="{{ old('last_name') }}">
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('last_name') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" id="email" class="form-control"
                                   value="{{ old('email') }}" placeholder="E-Mail">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="mobile" id="mobile" class="form-control"
                                   value="{{ old('mobile') }}" placeholder="Mobile Number">
                            @if ($errors->has('mobile'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('mobile') }}</strong>
                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-primary" value="Create Admin">
                    </div>

                </div>

            </form>
        </div>
    </div>

@endsection
