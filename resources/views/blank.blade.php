@extends('layouts.app')

@section('content')
    {{--Page Content Goes here--}}
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/highcharts/css/highcharts.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
    <script>
        $(document).ready(function (e) {
            Highcharts.chart('daily-data-chart', {
                chart: {
                    type: 'column'

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 2,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
                },
                series: [{
                    name: 'Population',
                    data: [
                        ['28 Jul', 50],
                        ['29 Jul', 55],
                        ['30 Jul', 105],
                        ['01 Aug', 85],
                        ['02 Aug', 41],
                        ['03 Aug', 125],
                        ['04 Aug', 120],
                        ['05 Aug', 100],
                        ['06 Aug', 55],
                        ['07 Aug', 98],
                        ['08 Aug', 65],
                        ['09 Aug', 102],
                        ['10 Aug', 85],
                        ['11 Aug', 90],
                        ['12 Aug', 155],
                        ['13 Aug', 116],
                        ['14 Aug', 45],
                        ['15 Aug', 107],
                        ['16 Aug', 90],
                        ['17 Aug', 96]
                    ]
                }]
            });
            Highcharts.chart('monthly-data-chart', {
                chart: {
                    type: 'column',

                },
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 2,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                title: false,
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>'
                },
                series: [{
                    name: 'Population',
                    data: [
                        ['28 Jul', 50],
                        ['29 Jul', 55],
                        ['30 Jul', 105],
                        ['01 Aug', 85],
                        ['02 Aug', 41],
                        ['03 Aug', 125],
                        ['04 Aug', 120],
                        ['05 Aug', 100],
                        ['06 Aug', 55],
                        ['07 Aug', 98],
                        ['08 Aug', 65],
                        ['09 Aug', 102],
                        ['10 Aug', 85],
                        ['11 Aug', 90],
                        ['12 Aug', 155],
                        ['13 Aug', 116],
                        ['14 Aug', 45],
                        ['15 Aug', 107],
                        ['16 Aug', 90],
                        ['17 Aug', 96]
                    ]
                }]
            });

            Highcharts.chart('connections-chart', {
                chart: {
                    type: 'pie'
                },

                plotOptions: {
                    pie: {
                        innerSize: '70%'
                    }
                },
                title: {
                    text: '24',
                    verticalAlign: 'middle',
                    floating: true
                },

                series: [{
                    data: [
                        ['Online', 85],
                        ['StandBy', 5],
                        ['Offline', 10]
                    ]
                }]
            });
        });
    </script>
@endsection
