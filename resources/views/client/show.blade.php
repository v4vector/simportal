 @extends('layouts/app')

@section('content')
//
    @php
        $top_metrics = $all['top_metrics'];
        $day_data = $all['day_data'];
        $month_data = $all['month_data'];
    @endphp

    <div class="container">
        <div class="section bg-section form-group">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <strong>Name: </strong> {{ $user->full_name }}
                    </div>
                    <div class="form-group">
                        <strong>Contact Mobile: </strong> {{ $user->contact_mobile }}
                    </div>
                    <div class="form-group">
                        <strong>Contact E-mail: </strong> {{ $user->contact_email }}
                    </div>
                    <div class="form-group">
                        <strong>Phone Number: </strong> {{ $user->phone }}
                    </div>
                    <div class="form-group">
                        <strong>E-mail: </strong> {{ $user->email }}
                    </div>
                </div>
                @if($user->has('company'))
                    <div class="col-sm-6">
                        <div class="form-group">
                            <strong>Company Name: </strong> {{ $user->company->name }}
                        </div>
                        <div class="form-group">
                            <strong>Company Address: </strong> {{ $user->company->address }}
                        </div>
                        <div class="form-group">
                            <strong>Post Code: </strong> {{ $user->company->postcode }}
                        </div>
                        <div class="form-group">
                            <strong>Company Number: </strong> {{ $user->company->company_number }}
                        </div>
                        <div class="form-group">
                            <strong>Tax ID: </strong> {{ $user->company->taxID }}
                        </div>
                    </div>
                @endif
                <div class="col-sm-12">
                    <hr>
                    <strong>Notes: </strong> {{ $user->company->notes }}
                </div>
            </div>
        </div>


        <section class=" bg-section form-group">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <h4 class="heading">QUOTES</h4>
                    </div>
                    @if(!$user->client_quotes->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Order Number</th>
                                    <th>Status</th>
                                    <th>Sale Agent</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user->client_quotes as $quo)
                                    <tr>
                                        <td>{{ $quo->order_number ?? 'NaN' }}</td>
                                        <td>{{ $quo->status }}</td>
                                        <td>{{ $quo->user->full_name }}</td>
                                        <td class="text-center">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="{{ route('quote.show',$quo) }}" data-toggle="tooltip"
                                                       title="View Quote"><i class="fa fa-search"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-info">
                            <strong><i class="fa fa-exclamation-circle"></i></strong> No Quote
                        </div>
                    @endif
                </div>
            </div>
        </section>

<!--Start-->
        @if($day_data && $month_data)
        <section class="form-group">
            <div class="bg-section">
                @if(!$month_data->isEmpty() && !$day_data->isEmpty())
                    <div class="row section-head">
                        <div class="col-sm-2"><h4 class="heading">DATA USAGE (MB)</h4></div>
                        <div class="col-sm-4"><h4>{{ Carbon\Carbon::now()->subDays(7)->format('d M Y') }}
                                - {{ Carbon\Carbon::now()->subDays(1)->format('d M Y') }}</h4></div>
                        <div class="col-sm-4 hidden-print">
                            <ul class="nav nav-pills">
                                <li class="active"><a data-toggle="pill" href="#daily-tab">Daily</a></li>
                                <li><a data-toggle="pill" href="#monthly-tab">Monthly</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-2 hidden-print">
                            <a href="" class="btn btn-default"><img
                                        src="{{ asset('assets/images/icons/calendar.png') }}"
                                        class="btn-icons" alt="Image"></a>
                            <a href="" class="btn btn-default"><img
                                        src="{{ asset('assets/images/icons/file_download.png') }}"
                                        class="btn-icons" alt="Image"></a>
                        </div>
                    </div>

                    <div class="row chart-stats">
                        <div class="col-sm-12">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="daily-tab">
                                    <div class="col-sm-9">
                                        <div id="daily-data-chart"></div>
                                    </div>
                                    <div class="col-sm-3 hidden-print">
                                        @php
                                            $total_days = $day_data->count();
                                            $total_usage = $day_data->sum('daily_usage');
                                            $average_daily = round(($total_usage) / $total_days, 2);
                                        @endphp 
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-12 stat form-group">
                                                <div class="stat-value">{{ round($total_usage, 2) }}</div>
                                                <div class="stat-label">Total Usage</div>
                                            </div>
                                            <div class="col-xs-6 col-sm-12 stat form-group">
                                                <div class="stat-value">{{ $average_daily }}</div>
                                                <div class="stat-label">Average Usage</div>
                                            </div>
                                            <div class="col-xs-6 col-sm-12 stat form-group">
                                                <div class="stat-value">{{ $day_data->sum('daily_sessions') }}</div>
                                                <div class="stat-label">Sessions</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="monthly-tab">
                                    <div class="col-sm-9">
                                        <div id="monthly-data-chart"></div>
                                    </div>
                                    <div class="col-sm-3 hidden-print">
                                       @php
                                            $total_months = $month_data->count();
                                            $total_usage = $month_data->sum('daily_usage');
                                            $average_monthly = round(($total_usage) / $total_months, 2);
                                        @endphp
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-12 stat form-group">
                                                <div class="stat-value">{{ round($total_usage, 2) }}</div>
                                                <div class="stat-label">Total Usage</div>
                                            </div>
                                            <div class="col-xs-6 col-sm-12 stat form-group">
                                                <div class="stat-value">{{ $average_monthly }}</div>
                                                <div class="stat-label">Average Usage</div>
                                            </div>
                                            <div class="col-xs-6 col-sm-12 stat form-group">
                                                <div class="stat-value">{{ $month_data->sum('daily_sessions') }}</div>
                                                <div class="stat-label">Sessions</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="form-group">
                        <h4 class="heading">Data Usage</h4>
                    </div>
                    <div class="alert alert-info">
                        <strong><i class="fa fa-exclamation-circle"></i></strong> No Usage
                    </div>
                @endif
            </div>
        </section>
        @endif

        <section class="">
            <div class="row">
                <div class="col-sm-6 form-group">
                    <div class="bg-section">
                        <div class="form-group">
                            <h4 class="heading">CONNECTIONS</h4>
                        </div>
                        @if(!$user->connections->isEmpty())
                            <div id="connections-chart"></div>
                        @else
                            <div class="alert alert-info">
                                <strong><i class="fa fa-exclamation-circle"></i></strong> No Connections
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6 form-group">
                    <div class="bg-section">
                        <h4 class="heading">TOP METRICS (MB)</h4>
                        @if(!$top_metrics->isEmpty())
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tbody>
                                    @foreach($top_metrics->slice(0,11) as $top)
                                        <tr>
                                            <td>{{ $top->first()->connection->iccid }}</td>
                                            <td>{{ number_format($top->sum('daily_usage') / 1048576, 2) }}</td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                                </table>
                            </div>
                        @else
                            <div class="alert alert-info">
                                <strong><i class="fa fa-exclamation-circle"></i></strong> No Usage
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/highcharts/css/highcharts.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
    <script>
        function set_dailychart(daywise_data) {
            var formatted = new Array();
            var day = '';
            $.each(daywise_data, function (key, val) {
                day = val.days.toString();
                formatted.push([day, val.daily_usage]);
            });

            Highcharts.chart('daily-data-chart', {
                chart: {
                    type: 'column'

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 1,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.2f} (MB)</b>'
                },
                series: [{
                    name: 'Usage',
                    data: formatted
                }]
            });
        }
        function set_monthlychart(monthwise_data) {
            var formatted = new Array();
            var day = '';
            $.each(monthwise_data, function (key, val) {
                day = val.months.toString();
//                        daywise[day] = val.daily_usage
                formatted.push([day, val.daily_usage]);
            });
            Highcharts.chart('monthly-data-chart', {
                chart: {
                    type: 'column'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 1,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                title: false,
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.1f} (MB)</b>'
                },
                series: [{
                    name: 'Usage',
                    data: formatted
                }]
            });
        }
        function set_topmetrics(top_metrics_data) {
            var formatted = '';
            var row = '';
            $.each(top_metrics_data, function (key, val) {
               console.log(val);
               row = '<tr><td>'+val.iccid+'</td><td>'+val.usage+'</td>';
               formatted += row;
            });
            console.log(formatted);
        }


        $(document).ready(function () {


            Highcharts.chart('daily-data-chart', {
                chart: {
                    type: 'column'

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 1,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.2f} (MB)</b>'
                },
                series: [{
                    name: 'Usage',
                    data: [
                            @foreach($day_data as $daily)
                            @if($loop->last)
                        ['{{ $daily->days }}', {{ $daily->daily_usage }}]
                                @else
                            ['{{ $daily->days }}', {{ $daily->daily_usage }}],
                        @endif
                        @endforeach
                    ]
                }]
            });
            Highcharts.chart('monthly-data-chart', {
                chart: {
                    type: 'column'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 1,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                title: false,
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.2f} (MB)</b>'
                },
                series: [{
                    name: 'Population',
                    data: [
                            @foreach($month_data as $monthly)
                            @if($loop->last)
                                ['{{ $monthly->months }}', {{ $monthly->daily_usage }}]
                            @else
                                ['{{ $monthly->months }}', {{ $monthly->daily_usage }}],
                            @endif
                        @endforeach
                    ]
                }]
            });

            Highcharts.chart('connections-chart', {
                chart: {
                    type: 'pie'
                },

                plotOptions: {
                    pie: {
                        innerSize: '70%'
                    }
                },
                title: {
                    text: '{{ $user->connections->count() }}',
                    verticalAlign: 'middle',
                    floating: true
                },

                series: [{
                    data: [
                        ['Online', {{ $user->connections->where('status', 'ACTIVATED')->count() }}],
                        ['Offline', {{ $user->connections->where('status', '!=', 'ACTIVATED')->count() }}]
                    ]
                }]
            });


            var default_start = '{{ Carbon\Carbon::now()->subMonth()->startOfMonth()->format('d-m-Y') }}';
            var default_end = '{{ Carbon\Carbon::now()->endOfMonth()->format('d-m-Y') }}';
            /*$.ajax({
                url: '{{ url('search-usage') }}' +'/'+ default_start +'/'+ default_end,
                success: function (result) {
                    set_dailychart(result.day_data);
                    set_monthlychart(result.month_data);
                    set_topmetrics(result.top_metrics);
                }
            });*/
        });
    </script>
@endsection 