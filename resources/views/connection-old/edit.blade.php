@extends('layouts.app')

@section('styles')
@endsection
@section('scripts')
@endsection

@section('content')
    <section class="section">
        <div class="container">

            @include('device._form')

        </div>
    </section>
@endsection