<!-- @extends('layouts/app')

@section('content')

    @php
        //Get avg for all the months on monthly basis
        $total_months = $group_months->count();
        $total_usage = $con_months->usage->sum('daily_usage');
        $average_monthly = round(($total_usage  / 1048576) / $total_months, 2);

        $total_days = $group_days->count();
        $total_usage = $con_days->usage->sum('daily_usage');
        $average_daily = round(($total_usage / 1048576) / $total_days, 2);
    @endphp

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="sim_details">
                        <span class="switch"><input type="checkbox"
                                                    class="js-switch" {{ $con_days->status == 'ACTIVATED' ? 'checked' : '' }}></span>
                        <span class="bold mdi mdi-fiber-manual-record {{ $con_days->status == 'ACTIVATED' ? 'text-success' : 'text-danger' }}"></span>
                        <span class="bold">{{ $con_days->status == 'ACTIVATED' ? 'Online' : 'Offline' }}</span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="sim_details">
                        <span class="icon mdi mdi-error-outline"></span>
                        <span class="bold">ICCID:</span>
                        <span class="value">{{ $con_days->iccid }}</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="sim_details">
                        <span class="icon mdi mdi-data-usage"></span>
                        <span class="bold">Type:</span>
                        <span class="value">{{ '??' }}</span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="sim_details">
                        <span class="icon mdi mdi-network-cell"></span>
                        <span class="bold">Network:</span>
                        <span class="value">{{ '??' }}</span>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="sim_details">
                        <div class="dropdown more">
                            <a class="btn btn-link dropdown-toggle" type="button" id="nh_5_dd" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="true">
                                <i class="mdi mdi-more-horiz"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_5_dd">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container form-group">
        <div class="bg-section chart-section">
            <div class="row section-head">
                <div class="col-sm-2"><h4 class="heading">DATA USAGE (MB)</h4></div>
                <div class="col-sm-4"><h4>{{ Carbon\Carbon::now()->subDays(7)->format('d M Y') }} - {{ Carbon\Carbon::now()->subDays(1)->format('d M Y') }}</h4></div>
                <div class="col-sm-4">
                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#daily-tab">Daily</a></li>
                        <li><a data-toggle="pill" href="#monthly-tab">Monthly</a></li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <a href="" class="btn btn-default"><img src="{{ asset('assets/images/icons/calendar.png') }}"
                                                            class="btn-icons" alt="Image"></a>
                    <a href="" class="btn btn-default"><img src="{{ asset('assets/images/icons/file_download.png') }}"
                                                            class="btn-icons" alt="Image"></a>
                </div>
            </div>
            <div class="row chart-stats">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="daily-tab">
                        <div class="col-sm-9">
                            <div id="sim-daily-chart"></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ round($con_days->usage->sum('daily_usage') / 1048576, 2) }}</div>
                                    <div class="stat-label">Total Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $average_daily }}</div>
                                    <div class="stat-label">Average Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $con_days->usage->sum('daily_sessions') }}</div>
                                    <div class="stat-label">Sessions</div>
                                </div>
                                {{--<div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '35' }}</div>
                                    <div class="stat-label">Session Timeouts</div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="monthly-tab">
                        <div class="col-sm-9">
                            <div id="sim-monthly-chart"></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ round($con_months->usage->sum('daily_usage') / 1048576, 2) }}</div>
                                    <div class="stat-label">Total Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $average_monthly  }}</div>
                                    <div class="stat-label">Average Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $con_months->usage->sum('daily_sessions') }}</div>
                                    <div class="stat-label">Sessions</div>
                                </div>
                                {{--<div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '35' }}</div>
                                    <div class="stat-label">Session Timeouts</div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="bg-section">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <h4 class="heading">CONNECTION HISTORY</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-flex no-footer" id="connection_history_table">
                            <thead>
                            <tr>
                                <th>ICCID</th>
                                <th>IP Address</th>
                                <th>Session Start</th>
                                <th>Session End</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $session['iccid'] }}</td>
                                <td>{{ $session['ipAddress'] }}</td>
                                <td>{{ $session['dateSessionStarted'] }}</td>
                                <td>{{ $session['dateSessionEnded'] }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/switchery/dist/switchery.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/highcharts/css/highcharts.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.js') }}"></script>
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
    <script>
        $(document).ready(function (e) {
            Highcharts.chart('sim-daily-chart', {
                chart: {
                    type: 'column'

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 2,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.1f} (MB)</b>'
                },
                series: [{
                    name: 'Usage',
                    data: [
                            @foreach($group_days as $key => $day)
                            @php
                                $sum = $day->sum('daily_usage') / 1048576;
                                $day = \Carbon\Carbon::parse($key)->format('d M');
                            @endphp

                            @if($loop->last)
                        ['{{ $day }}', {{ $sum }}]
                                @else
                            ['{{ $day }}', {{ $sum }}],
                        @endif

                        @endforeach
                    ]
                }]
            });
            Highcharts.chart('sim-monthly-chart', {
                chart: {
                    type: 'column',

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 2,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.1f} (MB)</b>'
                },
                series: [{
                    name: 'Usage',
                    data: [
                            @foreach($group_months as $key => $month)
                            @php
                                $sum = $month->sum('daily_usage') / 1048576;
                                $mon = \Carbon\Carbon::parse($key)->format('M, Y');
                            @endphp

                            @if($loop->last)
                        ['{{ $mon }}', {{ $sum }}]
                                @else
                            ['{{ $mon }}', {{ $sum }}],
                        @endif

                        @endforeach
                    ]
                }]
            });
        });
    </script>
@endsection
 -->