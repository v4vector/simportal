@extends('layouts/app')

@section('content')

<div class="container">
    <div class="bg-section">
        <form method="post" action="{{ route($url.'.update', $package) }}">
            @method('put')
            @csrf

            @include($view_dir.'/_form')

        </form>
    </div>
</div>

@endsection

@section('styles')

@endsection

@section('scripts')

@endsection