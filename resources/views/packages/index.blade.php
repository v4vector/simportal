@extends('layouts/app')

@section('content')
    <div class="container">
        <div class="bg-section">
            <div class="row">
                <div class="col-sm-12 form-group text-right">
                    <a href="{{ route($url.'.create') }}" class="btn btn-primary">Packages</a>
                </div>
                <div class="col-sm-12">
                    @if($packages->isEmpty() == false)
                        <div class="table-responsive">
                            <table class="table dataTable">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Amount of Data</th>
                                    <th>Price</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($packages as $pckg)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $pckg->name }}</td>
                                        <td>{{ $pckg->data }}</td>
                                        <td>{{ $pckg->price }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="btn btn-link dropdown-toggle" type="button" id="nh_5_dd" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <i class="mdi mdi-more-horiz"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_5_dd">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="alert alert-warning">
                            <strong>Not Found!</strong> You can add new Packages <a href="{{ route($url.'.create') }}">here</a>.
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/switchery/dist/switchery.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.js') }}"></script>
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
@endsection