<div class="row">
    <div class="col-sm-4">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <input type="text" name="name" id="name" class="form-control"
                   value="{{ $package->name ?? old('name') }}" placeholder="Package Name">
            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </span>
            @endif
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group{{ $errors->has('data') ? ' has-error' : '' }}">
            <input type="text" name="data" id="data" class="form-control"
                   value="{{ $package->data ?? old('data') }}" placeholder="Amount of Data">
            @if ($errors->has('data'))
                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $errors->first('data') }}</strong>
                                </span>
            @endif
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
            <input type="text" name="price" id="price" class="form-control"
                   value="{{ $package->price ?? old('price') }}" placeholder="Price">
            @if ($errors->has('price'))
                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $errors->first('price') }}</strong>
                                </span>
            @endif
        </div>
    </div>
    <div class="col-sm-12">
        <input type="submit" class="btn btn-primary" value="Save">
    </div>
</div>