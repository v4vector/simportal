@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
@endsection

@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h3>Client: {{ ucwords($quote->client->full_name) }}</h3>
                </div>
                <div class="col-sm-6">
                    <h3>Order Number: {{ ucwords($quote->order_number) }}</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 form-group">Total Upfront: <strong>${{ $quote->deals->sum('advance_payment') }}</strong>
                </div>
                @if(!$quote->installments->isEmpty())
                    @php
                        $monthly = $quote->installments->groupBy(function ($q){
                            return $q->date->format('m-Y');
                        })->map(function ($row) {
                            return (string) $row->sum('amount');
                        });
                        $array = $monthly->toArray();
                        $unique = array_count_values($array);
                        $total = $monthly->sum();
                    @endphp
                    @foreach($unique as $amount => $months)
                        <div class="col-sm-12">{{ $months }}x Monthly Installments: <strong>${{ $amount }}</strong></div>
                    @endforeach
                    @foreach($monthly as $month => $sum)
                        <div class="col-sm-1 col-xs-6 form-group"><strong>{{ 'Month '.$loop->iteration }}</strong> ${{ $sum }}</div>
                    @endforeach
                @else
                    @php
                        $installs = new \Illuminate\Support\Collection();
                        if($quote->deals){
                        foreach($quote->deals as $deal){
                            if($deal->payment_terms == 'monthly'){
                                $con = $deal->contract_len - 3;
                                $ins = $deal->installment;

                                for($i=1; $i<=$con; $i++){
                                    $arr = [
                                        'month' => 'Month '.$i,
                                        'install' => $ins,
                                    ];
                                    $installs->push($arr);
                                }
                            }
                        }
                        }
                        $months = $installs->groupBy(function ($r){
                            return $r['month'];
                        })->map(function ($group){
                            return $group->sum('install');
                        });
                    @endphp
                    @foreach($months as $mon => $ins)
                        <div class="col-sm-1 col-xs-6 form-group"><strong>{{ $mon }}:</strong> ${{ $ins }}</div>
                    @endforeach
                @endif
            </div>

            <div class="row">
                @foreach($quote->deals as $deal)
                    <div class="col-sm-4 form-group">
                        <div class="table-responsive bg-section" style="padding: 0">
                            <table class="table table-bordered no-footer">
                                <thead>
                                <tr>
                                    <th colspan="2"><h4>Deal {{ $loop->iteration }}</h4></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="col-xs-6">Product Type</td>
                                    <td class="col-xs-6">{{ $deal->product_type->name }}</td>
                                </tr>
                                <tr>
                                    <td>Data Type</td>
                                    <td>{{ $deal->data_type }}</td>
                                </tr>
                                <tr>
                                    <td>Data Usage</td>
                                    <td>{{ $deal->data }}</td>
                                </tr>
                                <tr>
                                    @php
                                        $networks = \App\Models\Network::get();
                                        $exploded = explode(',', $deal->network_preferences);
                                        $nets = [];
                                        foreach($exploded as $id){
                                            $nets[] = $networks->where('id', $id)->first()->name;
                                        }
                                        $nets = implode(',', $nets);
                                    @endphp
                                    <td>Network Preferences</td>
                                    <td>{{ $nets }}</td>
                                </tr>
                                <tr>
                                    <td>Sim Volumn</td>
                                    <td>{{ $deal->sim_volumn }}</td>
                                </tr>
                                <tr>
                                    <td>Data Volumn</td>
                                    <td>{{ $deal->volumn->name }}</td>
                                </tr>
                                <tr>
                                    <td>Contract Length</td>
                                    <td>{{ $deal->contract_len.' Month' }}</td>
                                </tr>
                                <tr>
                                    <td>Client Application</td>
                                    <td>{{ $deal->client_application->name }}</td>
                                </tr>
                                <tr>
                                    <td>Basic Price</td>
                                    <td>${{ $deal->base_price }}</td>
                                </tr>
                                <tr>
                                    <td>Margin Price</td>
                                    <td>${{ $deal->margin_sales }}</td>
                                </tr>
                                <tr>
                                    <td>Client Rate</td>
                                    <td>${{ $deal->client_price }}</td>
                                </tr>
                                <tr>
                                    <td>Total Deal Value</td>
                                    <td>${{ $deal->total_deal }}</td>
                                </tr>
                                <tr>
                                    <td>Total Comission</td>
                                    <td>${{ $deal->total_commission }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
            <a href="{{ url('quote/send', $quote->id)  }}" class="btn btn-primary">Send</a>
        </div>
    </section>
@endsection