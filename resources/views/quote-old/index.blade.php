@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection

@section('content')

    <section class="bg-section form-group">
        <div class="container">

            <div class="row overallStats">
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">

                        <div class="stat-value">{{ $quotes->count() }}</div>
                        <div class="stat-label">Total Quotes</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $quotes->where('status', 'Draft')->count() }}</div>
                        <div class="stat-label">Draft</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $quotes->where('status', 'Sent')->count() }}</div>
                        <div class="stat-label">Sent</div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ $quotes->where('status', 'Approved')->count() }}</div>
                        <div class="stat-label">Approved</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-bordered dataTable no-footer">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Client</th>
                                <th>Order Number</th>
                                <th>Status</th>
                                @hasrole('admin')
                                <th>Added By</th>
                                @endhasrole
                                <th>Added On</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($quotes as $quote)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><a href="">{{ $quote->client->full_name }}</a></td>
                                    <td>{{ $quote->order_number }}</td>
                                    <td>{{ $quote->status }}</td>
                                    @hasrole('admin')
                                    <td>{{ $quote->user->full_name }}</td>
                                    @endhasrole
                                    <td>{{ $quote->created_at->format('d M, Y') }}</td>
                                    <td class="text-center">
                                        <ul class="list-inline">
                                            @hasanyrole('admin|saleagent')
                                            <li>
                                                <a href="{{ route('deal.create', $quote)  }}" data-toggle="tooltip"
                                                   title="Add New Deal"><i class="fa fa-plus-square"></i></a>
                                            </li>
                                            @endhasanyrole
                                            <li>
                                                <a href="{{ route('quote.show',$quote) }}" data-toggle="tooltip"
                                                   title="View Quote"><i class="fa fa-search"></i></a>
                                            </li>
                                            @hasanyrole('admin|saleagent')
                                            <li>
                                                <a href="{{ route('quote.send', $quote->id)  }}" data-toggle="tooltip"
                                                   title="Send Quote in Email"><i class="fa fa-location-arrow"></i></a>
                                            </li>
                                            @endhasanyrole
                                            @if($quote->status == 'Sent' && Auth::user()->hasRole('admin'))
                                                <li>
                                                    <a href="{{ route('quote.approve', $quote->id)  }}"
                                                       data-toggle="tooltip"
                                                       title="Approve Quote"><i class="fa fa-check-circle"></i></a>
                                                </li>
                                            @endif

                                            @if($quote->status == 'Approved')
                                                <li>
                                                    <a href="{{ route('attach.deal_conn', $quote->id) }}" data-toggle="tooltip" title="Add Connections">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </li>
                                            @endif
                                        </ul>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection