<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h3>Client: {{ ucwords($quote->client->full_name) }}</h3>
            </div>
            <div class="col-sm-6">
                <h3>Order Number: {{ ucwords($quote->order_number) }}</h3>
            </div>
        </div>

        <div class="row">
            @php
                $installs = new \Illuminate\Support\Collection();
                if($quote->deals){
                foreach($quote->deals as $deal){
                    if($deal->payment_terms == 'monthly'){
                        $con = $deal->contract_len - 3;
                        $ins = $deal->installment;

                        for($i=1; $i<=$con; $i++){
                            $arr = [
                                'month' => 'Month '.$i,
                                'install' => $ins,
                            ];
                            $installs->push($arr);
                        }
                    }
                }
                }
                $months = $installs->groupBy(function ($r){
                    return $r['month'];
                })->map(function ($group){
                    return $group->sum('install');
                });
            @endphp
            @foreach($months as $mon => $ins)
                <div class="col-sm-1 col-xs-6 form-group"><strong>{{ $mon }}:</strong> ${{ $ins }}</div>
            @endforeach
        </div>

        @foreach($quote->deals as $deal)
            <div class="row form-group">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-bordered no-footer">
                            <thead>
                            <tr>
                                <th colspan="2" class="col-xs-6"><h4>Deal {{ $loop->iteration }}</h4></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Product Type</td>
                                <td>{{ $deal->product_type->name }}</td>
                            </tr>
                            <tr>
                                <td>Data Type</td>
                                <td>{{ $deal->data_type }}</td>
                            </tr>
                            <tr>
                                <td>Data Usage</td>
                                <td>{{ $deal->data }}</td>
                            </tr>
                            <tr>
                                @php
                                    $networks = \App\Models\Network::get();
                                    $exploded = explode(',', $deal->network_preferences);
                                    $nets = [];
                                    foreach($exploded as $id){
                                        $nets[] = $networks->where('id', $id)->first()->name;
                                    }
                                    $nets = implode(',', $nets);
                                @endphp
                                <td>Network Preferences</td>
                                <td>{{ $nets }}</td>
                            </tr>
                            <tr>
                                <td>Sim Volumn</td>
                                <td>{{ $deal->sim_volumn }}</td>
                            </tr>
                            <tr>
                                <td>Data Volumn</td>
                                <td>{{ $deal->volumn->name }}</td>
                            </tr>
                            <tr>
                                <td>Contract Length</td>
                                <td>{{ $deal->contract_len.' Month' }}</td>
                            </tr>
                            <tr>
                                <td>Client Application</td>
                                <td>{{ $deal->client_application->name }}</td>
                            </tr>
                            <tr>
                                <td>Basic Price</td>
                                <td>${{ $deal->base_price }}</td>
                            </tr>
                            <tr>
                                <td>Margin Price</td>
                                <td>${{ $deal->margin_sales }}</td>
                            </tr>
                            <tr>
                                <td>Client Rate</td>
                                <td>${{ $deal->client_price }}</td>
                            </tr>
                            <tr>
                                <td>Total Deal Value</td>
                                <td>${{ $deal->total_deal }}</td>
                            </tr>
                            <tr>
                                <td>Total Comission</td>
                                <td>${{ $deal->total_commission }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>
