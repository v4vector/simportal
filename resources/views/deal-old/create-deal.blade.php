@extends('layouts.app')

@section('content')

    <section class="bg-section form-group">
        <div class="container">

            <div class="row overallStats">
                <div class="col-sm-4 col-xs-6">
                    <div class="stat">

                        <div class="stat-value">{{ '$'.round($quote->deals->sum('total_deal'), 2) }}</div>
                        <div class="stat-label">Total Deal Value</div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <div class="stat">
                        <div class="stat-value">{{ '$'.round($quote->deals->sum('total_commission'), 2) }}</div>
                        <div class="stat-label">Total Agent Commission</div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-6">
                    <a href="{{ url('quote/send', $quote->id)  }}"
                       class="btn btn-rounded btn-white btn-block form-group">Send</a>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <form action="{{ route('deal.store', $quote) }}" method="post" class="form-light">
                    @csrf
                    <section class="container">
                        <div class="row">
                            <div class="col-sm-8 bg-section">
                                <div class="">

                                    <h3>Create A Deal</h3>


                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-section-heading">Client Info</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="client_id">Client</label>
                                                <input value="{{ $quote->client->full_name }}" type="text"
                                                       class="form-control" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="order_number">Purchase Order No. (Optional)</label>
                                                <input value="{{ $quote->order_number }}" type="text"
                                                       class="form-control" disabled="disabled">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-section-heading">Product Requirements</div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                @php
                                                    $product_types = \App\Models\ProductType::get()->pluck('show_name', 'id')->prepend(' -- Select Product Type -- ', '');
                                                @endphp
                                                <label for="product_type_id">Product Type</label>
                                                {{ Form::select('product_type_id', $product_types, old('product_type_id'), ['id' => 'product_type_id', 'class'=>'form-control select2']) }}
                                                @if ($errors->has('product_type_id'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('product_type_id') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="data_type">Data Type</label>
                                                {{ Form::select('data_type', ['' => ' -- Select One --', 'Pooled' => 'Pooled', 'Individual' => 'Individual'], old('data_type'), ['id' => 'data_type', 'class'=>'form-control select2']) }}
                                                @if ($errors->has('data_type'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('data_type') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="high_data">Data Usage</label>
                                            <div class="button-rad form-group">
                                                <input type="radio" name="data" class="data_usage" id="high_data"
                                                       value="high_data" checked>
                                                <label class="btn btn-primary input-lg" for="high_data">
                                                    High Data
                                                </label>
                                                <input type="radio" name="data" class="data_usage" id="low_data"
                                                       value="low_data" {{ old('data') == 'low_data' ? 'checked' : '' }}>
                                                <label class="btn btn-primary input-lg" for="low_data">
                                                    Low Data
                                                </label>
                                            </div>
                                            @if ($errors->has('data'))
                                                <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('data') }}</strong>
                                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">

                                        @php
                                            $networks = \App\Models\Network::get();
                                        @endphp
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="network_preferences">Network Preferences</label>
                                                <select name="network_preferences[]" id="network_preferences"
                                                        class="form-control" multiple>
                                                    @foreach($networks as $net)
                                                        @php
                                                            $select = '';
                                                            if(old('network_preferences') && in_array($net->id, old('network_preferences'))){
                                                                $select = 'selected';
                                                            }
                                                        @endphp
                                                        <option value="{{ $net->id }}" {{ $select }}>{{ $net->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('network_preferences'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('network_preferences') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="sim_volumn">Sim Volumn</label>
                                                {!! Form::number('sim_volumn', old('sim_volumn'), ['id'=> 'sim_volumn', 'class'=>'form-control']) !!}
                                                @if ($errors->has('sim_volumn'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('sim_volumn') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="data_volumn_id">Data Volumn</label>
                                                <select name="data_volumn_id" id="data_volumn_id"
                                                        class="form-control select2">
                                                    <option value=""> -- Select One --</option>
                                                </select>
                                                @if ($errors->has('data_volumn_id'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('data_volumn_id') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                @php
                                                    $cons = [1,3,6,12,24,36,48,60];
                                                @endphp
                                                <label for="contract_len">Contract Length</label>
                                                <select name="contract_len" id="contract_len"
                                                        class="form-control select2">
                                                    @foreach($cons as $con)
                                                        @php
                                                            $select = old('contract_len') == $con ? 'selected' : '';
                                                        @endphp
                                                        <option value="{{ $con }}" {{ $select }}>{{ $con.' Months' }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('contract_len'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('contract_len') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        @php
                                            $applications = \App\Models\ClientApplication::pluck('name', 'id');
                                        @endphp
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="client_application_id">Client Application</label>
                                                {{ Form::select('client_application_id', $applications, old('client_application_id'), ['id' => 'client_application_id', 'class'=>'form-control select2']) }}
                                                @if ($errors->has('client_application_id'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('client_application_id') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-section-heading">Pricing</div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="base_price">Base Price ($)</label>
                                                <input type="text" name="base_price" id="base_price"
                                                       class="form-control"
                                                       value="{{ old('base_price', 0) }}" readonly>
                                                @if ($errors->has('base_price'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('base_price') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="client_price">Client Price ($)</label>
                                                <input type="text" name="client_price" id="client_price"
                                                       class="form-control"
                                                       value="{{ old('client_price', 0) }}">
                                                <div id="client_price_small_error" class="text-danger hidden">Can't be smaller than Base Price</div>
                                                @if ($errors->has('client_price'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('client_price') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="margin_sales">Sale Agent Margin ($)</label>
                                                <input type="text" name="margin_sales" min="0" id="margin_sales"
                                                       class="form-control"
                                                       value="{{ old('margin_sales', 0) }}" readonly="readonly">
                                                @if ($errors->has('margin_sales'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('margin_sales') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="margin_admin">Admin Margin ($)</label>
                                                <input type="text" name="margin_admin" min="0" id="margin_admin"
                                                       class="form-control"
                                                       value="{{ old('margin_admin', 0) }}" readonly="readonly">
                                                @if ($errors->has('margin_admin'))
                                                    <span class="invalid-feedback" role="alert">
                                                            <strong class="text-danger small">{{ $errors->first('margin_admin') }}</strong>
                                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="row hidden" id="installment_row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="monthly_installments">Monthly Installments</label>
                                                {!! Form::select('monthly_installments', ['a'=> 'a'], old('monthly_installments', $quote->monthly_installments), ['id'=> 'monthly_installments', 'class'=>'form-control']) !!}
                                            </div>
                                        </div>
                                    </div>--}}
                                    <div class="row form-group">
                                        <div class="col-sm-4 text-black text-center">
                                            <div class="bold">Total Deal Value</div>
                                            <h2>$<span id="total_deal_val">{{ old('total_deal', 0) }}</span></h2>
                                        </div>
                                        <div class="col-sm-4 text-center">
                                            <div class="bold">Total Commission</div>
                                            <h2>$<span id="total_commission">{{ old('total_commission', 0) }}</span>
                                            </h2>
                                            <input type="hidden" name="total_commission" id="input_total_commission"
                                                   value="{{ old('total_commission', 0) }}">
                                        </div>
                                        <div class="col-sm-4 text-center">
                                            <div class="bold">Total Price + VAT</div>
                                            <h2>$<span
                                                        id="total_vat">{{ old('vat', 0) }}</span>
                                            </h2>
                                            <input type="hidden" name="vat" id="input_vat" value="{{ old('vat', 0) }}">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label for="upfront">Payment Terms</label>
                                            <div class="button-rad form-group">
                                                <input type="radio" name="payment_terms" class="payment_terms"
                                                       id="upfront"
                                                       value="upfront" checked>
                                                <label class="btn btn-primary input-lg" for="upfront">
                                                    Upfront Payment
                                                </label>
                                                <input type="radio" name="payment_terms" class="payment_terms"
                                                       id="monthly"
                                                       value="monthly" {{ old('payment_terms')=='monthly' ? 'checked' : '' }}>
                                                <label class="btn btn-primary input-lg" for="monthly">
                                                    Monthly Installments
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div id="for-upfront">
                                                <h4>Total Upfront: <b class="bold"
                                                                      id="total_upfront">${{ old('vat', '0') }}</b></h4>
                                            </div>
                                            <div id="for-monthly" class="hidden">
                                                <h4>Upfront Payment: <b id="advance_month"></b></h4>
                                                <h4><b id="month_install"></b>x Monthly Installment Amount: <b
                                                            id="per_month"></b></h4>
                                                <h4>Total Installment Amount: <b id="total_install"></b></h4>
                                                <input type="hidden" id="installment" name="installment"
                                                       value="{{ old('installment', 0) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-offset-6 col-sm-6 text-right">
                                            <button class="btn btn-rounded btn-primary btn-block form-group">Save
                                                Quote
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="">
                                    <div class="form-group gray bg-section">
                                        <h4>PRICE BREAKDOWN</h4>
                                        <h5>Prices including VAT</h5>

                                        @foreach($quote->deals as $deal)
                                            <div class="form-group">
                                                @if($deal->payment_terms == 'upfront')
                                                    <div>Deal {{ $loop->iteration }} <span
                                                                class="pull-right">Upfront ${{ round($deal->advance_payment, 2) }}</span>
                                                    </div>

                                                @else
                                                    <div>Deal {{ $loop->iteration }} <span
                                                                class="pull-right">Upfront ${{ round($deal->advance_payment, 2) }}</span>
                                                    </div>
                                                    <div class="text-right">{{ $deal->contract_len - 3 }}x monthly
                                                        installments of ${{ round($deal->installment, 2) }}</div>
                                                @endif
                                            </div>
                                        @endforeach

                                        @php
                                            $installs = new \Illuminate\Support\Collection();
                                            if($quote->deals){
                                            foreach($quote->deals as $deal){
                                                if($deal->payment_terms == 'monthly'){
                                                    $con = $deal->contract_len - 3;
                                                    $ins = $deal->installment;

                                                    for($i=1; $i<=$con; $i++){
                                                        $arr = [
                                                            'month' => 'Month '.$i,
                                                            'install' => $ins,
                                                        ];
                                                        $installs->push($arr);
                                                    }
                                                }
                                            }
                                            }
                                            $months = $installs->groupBy(function ($r){
                                                return $r['month'];
                                            })->map(function ($group){
                                                return $group->sum('install');
                                            });
                                            $string_values = $months->map(function($r, $key){
                                                return (string) $r;
                                            });
                                            $unique = array_count_values($string_values->toArray());
                                        @endphp
                                        <div><h5>Installment Details</h5></div>
                                        @foreach($unique as $ins => $count)
                                            <div>{{ $count }}x monthly installments <span class="pull-right">${{ $ins }}</span></div>
                                            @endforeach

                                        <hr>
                                        <div class="offer-details text-black bold">
                                            @php
                                                $deal_amount = $quote->deals->sum('deal_amount');
                                                $vat = $quote->deals->sum('vat');
                                                $upfront = $quote->deals->sum('advance_payment');
                                            @endphp
                                            {{--<div>Total <span class="pull-right">${{ round($deal_amount, 2) }}</span>
                                            </div>
                                            <div>VAT <span class="pull-right">${{ round($vat, 2) }}</span></div>--}}
                                            <div>Total Offer <span
                                                        class="pull-right">${{ round($deal_amount + $vat, 2) }}</span>
                                            </div>
                                            <hr>
                                            <div>Total Upfront: <span
                                                        class="pull-right">${{ round($upfront, 2) }}</span></div>
                                        </div>
                                    </div>

                                    {{--<div class="form-group gray bg-section">
                                        <h4>Payment Terms</h4>
                                        <div>
                                            <ul class="nav nav-pills nav-justified">
                                                <li class="active"><a data-toggle="pill" href="#infull-tab">In Full</a>
                                                </li>
                                                <li><a data-toggle="pill" href="#monthly-tab">Monthly</a></li>
                                            </ul>
                                        </div>
                                        <div class="tab-content" style="padding: 0">
                                            <div class="tab-pane fade in active" id="infull-tab">
                                                <div class="offer-details text-black bold">
                                                    <div>Total <span class="pull-right">{{ '$'.$total }}</span></div>
                                                    <div>VAT <span class="pull-right">{{ '$'.$VAT }}</span></div>
                                                    <div>Total Price <span
                                                                class="pull-right">{{ '$'.$grand_total }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="monthly-tab">
                                                <div class="offer-details text-black bold">
                                                    <div>Upfront Payment <span
                                                                class="pull-right">{{ '$'.$total }}</span></div>
                                                    <div>21 months installments <span
                                                                class="pull-right">{{ '$'.$VAT }}</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--}}

                                    @foreach($quote->deals as $deal)
                                        <div class="deal-info bg-section form-group">
                                            <h3>{{ 'Deal '.$loop->iteration }} <a
                                                        href="{{ url('quote/'.$quote->id.'/deal/'.$deal->id.'/edit') }}"><i
                                                            class="fa fa-edit pull-right"></i></a></h3>

                                            <div class="offer-details gray">
                                                <h4>REQUIREMENTS</h4>
                                                <div>Product Type: {{ $deal->product_type['name'] }}</div>
                                                <div>Data Type: {{ $deal->data_type }}</div>
                                                <div>Data Required: {{ $deal->volumn->name }}</div>
                                                <div>Sim Volumn: {{ $deal->sim_volumn }}</div>
                                                <div>Contract Length: {{ $deal->contract_len. ' Month' }}</div>
                                                <div>Client
                                                    Application: {{ $deal->client_application['name'] }}</div>
                                            </div>
                                        </div>
                                    @endforeach

                                    {{--<button class="btn btn-rounded btn-primary btn-block">Send Quote</button>--}}
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>

    </div>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}">
    <style>
        .select2-container--default .select2-selection {
            min-height: 46px;
        }

        .select2-container--default .select2-selection .select2-selection__rendered {
            line-height: 46px;
        }

        .select2-container--default .select2-selection .select2-selection__arrow {
            min-height: 46px;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.min.js') }}"></script>
    <script>
        //        Get Volumns from database based on type
        function get_data_volumn(type) {
            var select_box = $('#data_volumn_id');
            var url = '{{ url('get-data-volumn') }}' + '/' + type;
            var old = '{{ old('data_volumn_id') }}';
            $.get(url, function (data) {
                var options = '';
                $.each(data, function (i, item) {
                    var select = '';
                    if (old == item['id']) {
                        select = 'selected';
                    }
                    options += '<option value="' + item['id'] + '"' + select + '>' + item['name'] + '</option>';
                });
                select_box.html(options)
            });
        }

        function set_terms() {
            var terms = $('.payment_terms:checked').val();
            var vat = $('#vat').val();
            if (terms === 'monthly') {
                $('#for-upfront').addClass('hidden');
                $('#for-monthly').removeClass('hidden');

                /*var contract_len = Number($('#contract_len').val());
                var monthly = vat / contract_len;
                $('#per_month').html('$' + monthly.toFixed(2));
                $('#installment').val(monthly.toFixed(2));

                var advance = 3 * monthly;

                $('#advance_payment').val(advance.toFixed(2));
                $('#advance').html('$' + advance.toFixed(2));

                var installment_duration = contract_len - 3;
                var total_installment_amount = installment_duration * monthly;
                $('#month_install').html(installment_duration.toFixed(2));
                $('#total_install').html('$' + total_installment_amount.toFixed(2));*/

            } else {
                $('#for-monthly').addClass('hidden');
                $('#for-upfront').removeClass('hidden');

                /*$('#total_upfront').html('$' + vat.toFixed(2));
                $('#advance_payment').val(vat.toFixed(2));
                $('#installment').val('');*/
            }
        }

        function calculate_package() {

            var product_type_id = $('#product_type_id').val();
            var data_usage = $('.data_usage:checked').val();
            var data_volumn_id = Number($('#data_volumn_id').val());

            if (!product_type_id || !data_usage || !data_volumn_id) {
                return false;
            }

            var price_url = '{{ url('get-quote-price') }}' + '/' + product_type_id + '/' + data_usage;
            var price = 0;
            $.ajax({
                url: price_url,
                async: false,
                success: function (obt_price) {
                    price = obt_price;
                }
            });

            var volumn_url = '{{ url('get-volumn') }}' + '/' + data_volumn_id;
            var volumn = 0;
            $.ajax({
                url: volumn_url,
                async: false,
                success: function (obt_vol) {
                    volumn = obt_vol.volumn;
                }
            });

            var base_price = volumn * price;
            $('#base_price').val(base_price.toFixed(2)).change();

            $('#client_price').attr('min', base_price.toFixed(2)).val(base_price).change();
        }

        function calculate_margin() {
            var sims = Number($('#sim_volumn').val());
            var contract_len = Number($('#contract_len').val());
            var base_price = Number($('#base_price').val());
            var client_price = Number($('#client_price').val());
            if(client_price > base_price){
                var total_margin = client_price - base_price;
            } else {
                var total_margin = 0;
            }

            var unit_margin = total_margin / 2;
            var total_commission = (unit_margin * sims * contract_len);

            $('#margin_sales').val(unit_margin.toFixed(2));
            $('#margin_admin').val(unit_margin.toFixed(2));
            $('#total_commission').html(total_commission.toFixed(2));
            $('#input_total_commission').val(total_commission.toFixed(2));
        }

        function calculate_summary() {
            var sims = parseFloat($('#sim_volumn').val());
            var client_price = Number($('#client_price').val());
            var contract_len = Number($('#contract_len').val());

            if(Number($('#base_price').val()) > client_price){
                client_price = 0;
            }

            var total_deal_val = sims * client_price * contract_len;
            var vat = total_deal_val + ((20 / 100) * total_deal_val);

            $('#total_deal_val').html(total_deal_val.toFixed(2));
            $('#total_vat').html(vat.toFixed(2));
            $('#input_vat').val(vat);
            $('#total_upfront').html('$' + vat.toFixed(2));

            var monthly = vat / contract_len;
            var advance = 3 * monthly;

            $('#per_month').html('$' + monthly.toFixed(2));
            $('#installment').val(monthly.toFixed(2));


            $('#advance_payment').val(advance.toFixed(2));
            $('#advance_month').html('$' + advance.toFixed(2));

            var installment_duration = contract_len - 3;
            var total_installment_amount = installment_duration * monthly;
            $('#month_install').html(installment_duration.toFixed(2));
            $('#total_install').html('$' + total_installment_amount.toFixed(2));
            $('#monthly').attr('checked');
        }

        $(document).ready(function (e) {
            var currency = '£';
            $('.select2').select2();

            var type = '{{ old('data') ?? 'high_data' }}';
            get_data_volumn(type);

            var select_box = $("#network_preferences");
            select_box.select2();
            $('#product_type_id').on('change', function () {
                var type = $(this).val();
                if (type == 1) {
                    select_box.removeAttr('multiple');
                } else {
                    select_box.attr('multiple', 'multiple');
                }
                select_box.select2("destroy");
                select_box.select2();
            });

//            New Code Here
            $('#product_type_id, #data_volumn_id').on('change', function (e) {
                e.preventDefault();
                calculate_package();
            });


            $('#contract_len').on('change', function (e) {
                if ($(this).val() <= 3) {
                    $('#monthly').attr('disabled', 'disabled');
                } else {
                    $('#monthly').removeAttr('disabled');
                }
                calculate_summary();
            });

//            If radio button for data usage changes, get its options from db
            $('.data_usage').on('change', function () {
                var type = $(this).val();
                get_data_volumn(type);
                calculate_package();
            });
            $('#sim_volumn, #data_volumn_id').on('keyup change', function (e) {
                calculate_package();
            });
            $('#client_price').on('change keyup', function () {
                var base_price = Number($('#base_price').val());
                if($(this).val() >= base_price) {
                    $('#client_price_small_error').addClass('hidden');
                    calculate_margin();
                    calculate_summary();
                    set_terms();
                } else {
                    $('#client_price_small_error').removeClass('hidden');
                    calculate_margin();
                    calculate_summary();
                    set_terms();
                }

            });
            $('.payment_terms').on('change', function () {
                var terms = $('.payment_terms:checked').val();
                var contract = Number($('#contract_len').val());
                if (terms === 'monthly' && contract>3) {
                    $('#for-upfront').addClass('hidden');
                    $('#for-monthly').removeClass('hidden');
                } else {
                    $('#for-monthly').addClass('hidden');
                    $('#for-upfront').removeClass('hidden');
                }
            });



//            Changes Show here
            /*$('#product_type_id').on('change', function () {
                var option = $(this).find(':selected').text();
                $('#product_type_text').text(option);
            });
            $('#data_type').on('change', function () {
                $('#data_type_text').text($(this).val());
            });
            $('#contract_len').on('change', function () {
                $('#contract_length_text').text($(this).val() + ' Months');
            });
            $('#client_application_id').on('change', function () {
                var option = $(this).find(':selected').text();
                $('#client_application_text').text(option);
            });
            $('#data_volumn_id').on('change', function () {
                var option = $(this).find(':selected').text();
                $('#data_required_text').text(option);
            });*/
        });
    </script>
@endsection

{{--((number of sims * data per sim projected)+ (Number of sims*connection costs)) *Contract Duration)+(one off manufacture sim cost)--}}