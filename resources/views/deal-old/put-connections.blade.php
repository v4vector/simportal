@extends('layouts.app')
@section('content')
    <section class="section">
        <div class="container">

            @if (\Session::has('danger'))
                <div class="alert alert-danger">
                    {!! \Session::get('danger') !!}
                </div>
            @endif
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    {!! \Session::get('success') !!}
                </div>
            @endif

            <form action="{{ route('store.deal_conn', $quote) }}" method="post">
                @csrf

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="client_id">Client</label>
                            <input type="text" class="form-control" value="{{ $quote->client->full_name }}"
                                   readonly="readonly">
                        </div>
                        <div class="form-group">
                            <label for="deal">Select Deal <sup>*</sup></label>
                            {{--{!! Form::select('deal', $quote->deals->pluck(''), old('deal'), ['id' => 'deal', 'class'=>'form-control selectpicker', 'multiple'=> 'multiple', 'data-live-search'=> 'true']) !!}--}}
                            <div id="deals-area">
                                @foreach($quote->deals as $deal)
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="deal" id="deal" value="{{ $deal->id }}"
                                                    {{ (old('deal')==$deal->id) ? 'checked' : '' }}>
                                            {{ $quote->order_number .' (Sims: '. $deal->sim_volumn.', Data: '.$deal->volumn->name.', Type: '.$deal->data_type.')' }}
                                        </label>
                                    </div>
                                @endforeach
                            </div>
                            @if ($errors->has('deal'))
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $errors->first('deal') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="connections">Select Devices <sup>*</sup></label>
                            {!! Form::select('connections[]', $connections->pluck('iccid', 'id'), old('connections'), ['id' => 'connections', 'class'=>'form-control selectpicker', 'multiple'=> 'multiple', 'data-live-search'=> 'true']) !!}
                            @if ($errors->has('connections'))
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $errors->first('connections') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-primary" value="Save">
                    </div>
                </div>
            </form>

        </div>
    </section>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-select/dist/css/bootstrap-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}">
    <style>
        .select2-container--default .select2-selection {
            min-height: 46px;
        }

        .select2-container--default .select2-selection .select2-selection__rendered {
            line-height: 46px;
        }

        .select2-container--default .select2-selection .select2-selection__arrow {
            min-height: 46px;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/bootstrap-select/dist/js/bootstrap-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();
            $('.selectpicker').selectpicker();

            $('#client_id').on('change', function (e) {
                var client_id = $(this).val();
                var url = '{{ url('ajax/client_deals') }}' + '/' + client_id;

                $.get(url, function (result) {
                    var deals = '';
                    $.each(result, function (iteration, deal) {
                        console.log(deal.advance_payment);
                        deals += '<div class="radio"><label>';
                        deals += '<input type="radio" name="deal" value="' + deal.id + '">';
                        deals += deal.advance_payment;
                        deals += '</label></div>';
//                        deals += '<option value="' + deal.id + '">' + deal.advance_payment + '</option>';
                    });
                    $('#deals-area').html(deals);
                });
            });
        });
    </script>
@endsection