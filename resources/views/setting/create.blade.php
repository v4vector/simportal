@extends('layouts.app')

@section('styles')
@endsection
@section('scripts')
@endsection

@section('content')
    <section class="section">
        <div class="container">

            <form action="{{ route('settings.store') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="app_name">App Name</label>
                            <input type="text" min="0" name="app_name" id="app_name" class="form-control"
                                   value="{{ old('per_mb_rate', $settings->where('setting_key', 'app_name')->first()->setting_val) }}"
                                   title="" required="required">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="per_mb_rate">Rate Per MB</label>
                            <input type="number" step="any" min="0" name="per_mb_rate" id="per_mb_rate" class="form-control"
                                   value="{{ old('per_mb_rate', $settings->where('setting_key', 'per_mb_rate')->first()->setting_val) }}"
                                   title="" required="required">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-primary" value="Save">
                    </div>
                </div>
            </form>

        </div>
    </section>
@endsection
