@extends('layouts/app')

@section('content')

    <div class="container">
        <div class="bg-section">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <h2>Edit Sales Agent</h2>
            <form method="post" action="{{ route($url.'.update', $model) }}">
                @csrf
                @method('put')


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" name="first_name" id="first_name" class="form-control"
                                   value="{{ old('first_name', $model->first_name) }}"
                                   placeholder="First Name">
                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('first_name') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="last_name" id="last_name" class="form-control"
                                   placeholder="Last Name" value="{{ old('last_name', $model->last_name) }}">
                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('last_name') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" id="email" class="form-control"
                                   value="{{ old('email', $model->email) }}" placeholder="E-Mail">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="text" name="mobile" id="mobile" class="form-control"
                                   value="{{ old('mobile', $model->mobile) }}" placeholder="Mobile Number">
                            @if ($errors->has('mobile'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('mobile') }}</strong>
                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input type="text" name="password" id="password" class="form-control"
                                   value="{{ old('password') }}" placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('password') }}</strong>
                </span>
                            @endif
                            <span class="invalid-feedback" role="alert">
                                <small class="text-primary">Leave Password Blank if you don't want to update</small>
                            </span>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-primary" value="Update Sale Agent">
                    </div>

                </div>

            </form>
        </div>
    </div>

@endsection

@section('styles')

@endsection

@section('scripts')

@endsection
