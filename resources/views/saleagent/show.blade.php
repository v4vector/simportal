@extends('layouts/app')

@section('content')

    <div class="container">
        <section class=" bg-section form-group">
            <div class="row">
                <div class="col-sm-6">
                    <h3>Name: {{ $model->fullname }}</h3>
                </div>
                <div class="col-sm-6">
                    <h3>E-Mail: {{ $model->email }}</h3>
                </div>
                <div class="col-sm-6">
                    <h3>Mobile: {{ $model->mobile }}</h3>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </section>
    </div>

    @if($clients->count())
    <div class="container">
        <section class=" bg-section form-group">
            <h4 class="heading">Clients of {{ $model->fullname }}</h4>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($clients as $client)
                        <tr>
                            <td>
                                <a href="{{ route('client.show', $client) }}">
                                    {{ $client->fullname }}
                                </a>
                            </td>
                            <td>{{ $client->email }}</td>
                            <td>{{ $client->mobile }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    </div>
    @endif

@endsection

@section('styles')
@endsection

@section('scripts')
@endsection