@extends('layouts/app')

@section('content')

    <div class="container">
        <div class="row" style="margin-top: 40px">
            <div class="col-sm-12 text-right">
                <div class="form-group">
                    <a href="{{ route('saleagent.create') }}" class="btn btn-primary">Create New Agent</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <section class="bg-section">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    @if($models->count())
                        <div class="table-responsive">
                            <table class="table table-hover dataTable">
                                <thead>
                                <th>Name</th>
                                <th>E-Mail</th>
                                <th>Mobile</th>
                                <th>Actions</th>
                                </thead>
                                <tbody>
                                @foreach($models as $model)
                                    <tr>
                                        <td>
                                            <a href="{{ route('saleagent.show', $model) }}">
                                                {{ $model->fullname }}
                                            </a>
                                        </td>
                                        <td>{{ $model->email }}</td>
                                        <td>{{ $model->mobile }}</td>
                                        <td>
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="{{ route('saleagent.show', $model) }}" title="Show {{ $model->fullname }} Account" data-toggle="tooltip">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('saleagent.edit', $model) }}" title="Edit {{ $model->fullname }} Account" data-toggle="tooltip">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a onclick="event.preventDefault();
                                                                document.getElementById('deleteForm{{ $model->id }}').submit()" title="Delete {{ $model->fullname }} Account" data-toggle="tooltip">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                </li>
                                                <form id="deleteForm{{ $model->id }}" action="{{ route('saleagent.destroy', $model) }}" method="POST" style="display: none;">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
                                @endforeach
                            </table>
                        </div>
                    @else
                        <div class="alert alert-info">
                            <strong><i class="fa fa-exclamation-circle"></i></strong> No Sale Agent
                        </div>
                    @endif
                </section>
            </div>
        </div>
    </div>

@endsection

@section('styles')
@endsection

@section('scripts')
@endsection
