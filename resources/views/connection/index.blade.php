@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/switchery/dist/switchery.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.js') }}"></script>
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
@endsection

@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-bordered dataTable no-footer">
                            <thead>
                            <tr>
                                <th>
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="connections_head"/>
                                        <label for="connections_head"></label>
                                    </div>
                                </th>
                                <th></th>
                                <th>Status</th>
                                <th>ICCID</th>
                                <th>Usage (MB)</th>
                                <th>Network</th>
                                <th>Date Added</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($connections))
                                @foreach($connections as $con)
                                    @php
                                        $status = $con->status == 'ACTIVATED' ? 'text-success' : 'text-danger';
                                        $checkbox = $con->status == 'ACTIVATED' ? 'checked' : '';
                                    @endphp
                                    <tr>
                                        <td>
                                            <div class="icheck-primary">
                                                <input type="checkbox" id="{{ $loop->iteration }}"/>
                                                <label for="{{ $loop->iteration }}"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="checkbox" class="js-switch" {{ $checkbox }}>
                                        </td>
                                        <td><i class="mdi mdi-fiber-manual-record {{ $status }}"></i></td>
                                        <td><a href="{{ route('connections.show', $con->iccid) }}">{{ $con->iccid }}</a>
                                        </td>
                                        <td>Usage</td>
                                        <td>Network</td>
                                        <td>{{ $con->created_at->format('d M, Y') }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="btn btn-link dropdown-toggle" type="button" id="nh_5_dd"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <i class="mdi mdi-more-horiz"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_5_dd">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection