@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="container">

            <form action="{{ route('connection.deal_info', $con) }}">
                @csrf
                @method('put')

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="deal_base_price">Base Price (MB) <sup>*</sup></label>
                            <input type="number" step="any" name="deal_base_price" id="deal_base_price"
                                   class="form-control"
                                   value="{{ old('deal_base_price', $con->deal_base_price) }}">
                            @if ($errors->has('deal_base_price'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('deal_base_price') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="deal_admin_margin">Admin Margin (MB) <sup>*</sup></label>
                            <input type="number" step="any" name="deal_admin_margin" id="deal_admin_margin"
                                   class="form-control"
                                   value="{{ old('deal_admin_margin', $con->deal_admin_margin) }}">
                            @if ($errors->has('deal_admin_margin'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('deal_admin_margin') }}</strong>
                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="deal_sales_margin">Sale Agent Margin (MB) <sup>*</sup></label>
                            <input type="number" step="any" name="deal_sales_margin" id="deal_sales_margin"
                                   class="form-control"
                                   value="{{ old('deal_sales_margin', $con->deal_sales_margin) }}">
                            @if ($errors->has('deal_sales_margin'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('deal_sales_margin') }}</strong>
                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="deal_client_price">Client Price (MB) <sup>*</sup></label>
                            <input type="number" step="any" name="deal_client_price" id="deal_client_price"
                                   class="form-control"
                                   value="{{ old('deal_client_price', $con->deal_client_price) }}">
                            @if ($errors->has('deal_client_price'))
                                <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('deal_client_price') }}</strong>
                </span>
                            @endif
                        </div>
                    </div>
                </div>

                {{--<div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="monthly_data_limit">Monthly Data Limit (GB) <sup>*</sup></label>
                            <input type="number" step="any" name="monthly_data_limit" id="monthly_data_limit" class="form-control"
                                   value="{{ old('monthly_data_limit', $con->monthly_data_limit) }}">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="total_data_limit">Total Data Limit (GB) <sup>*</sup></label>
                            <input type="number" step="any" name="total_data_limit" id="total_data_limit" class="form-control"
                                   value="{{ old('total_data_limit', $con->total_data_limit) }}">
                        </div>
                    </div>
                </div>--}}

                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-primary" value="Save">
                    </div>
                </div>
            </form>

        </div>
    </section>
@endsection

@section('styles')

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#deal_sales_margin').on('keyup', function (e) {
                var base_price   = Number($('#deal_base_price').val());
                var admin_margin = Number($('#deal_admin_margin').val());
                var sales_margin = Number($(this).val());

                var client_price = base_price + admin_margin + sales_margin;
                console.log(client_price);
                $('#deal_client_price').val(client_price);
            });
        });
    </script>
@endsection