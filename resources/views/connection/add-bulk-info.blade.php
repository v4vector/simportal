@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="container">

            <form action="{{ route('store_deal_info') }}" method="post">
                @csrf
                @method('put')

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="client_id">Client</label>
                            @php
                                $clients = \App\Models\User::role('client')->get()->pluck('full_name', 'id')->prepend(' -- Select Client -- ', '');
                            @endphp
                            <label for="client_id"></label>
                            {!! Form::select('client_id', $clients, old('client_id'), ['id' => 'client_id', 'class'=>'form-control select2', 'data-live-search'=> 'true']) !!}
                            @if ($errors->has('client_id'))
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger small">{{ $errors->first('client_id') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="deal">Select Deal <sup>*</sup></label>
                            {{--{!! Form::select('deal', [], old('deal'), ['id' => 'deal', 'class'=>'form-control selectpicker', 'multiple'=> 'multiple', 'data-live-search'=> 'true']) !!}--}}
                            <div id="deals-area"></div>
                            @if ($errors->has('deal'))
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $errors->first('deal') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="connections">Select Devices <sup>*</sup></label>
                            {!! Form::select('connections[]', $connections, old('connections'), ['id' => 'connections', 'class'=>'form-control selectpicker', 'multiple'=> 'multiple', 'data-live-search'=> 'true']) !!}
                            @if ($errors->has('connections'))
                                <span class="invalid-feedback" role="alert">
                                    <strong class="text-danger">{{ $errors->first('connections') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-primary" value="Save">
                    </div>
                </div>
            </form>

        </div>
    </section>
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-select/dist/css/bootstrap-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}">
    <style>
        .select2-container--default .select2-selection {
            min-height: 46px;
        }

        .select2-container--default .select2-selection .select2-selection__rendered {
            line-height: 46px;
        }

        .select2-container--default .select2-selection .select2-selection__arrow {
            min-height: 46px;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/bootstrap-select/dist/js/bootstrap-select.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();
            $('.selectpicker').selectpicker();

            $('#client_id').on('change', function (e) {
                var client_id = $(this).val();
                var url = '{{ url('ajax/client_deals') }}' + '/' + client_id;

                $.get(url, function (result) {
                    var deals = '';
                    $.each(result, function (iteration, deal) {
                        console.log(deal.advance_payment);
                        deals += '<div class="radio"><label>';
                        deals += '<input type="radio" name="deal" value="'+ deal.id +'">';
                        deals += deal.advance_payment;
                        deals += '</label></div>';
//                        deals += '<option value="' + deal.id + '">' + deal.advance_payment + '</option>';
                    });
                    $('#deals-area').html(deals);
                });
            });
        });
    </script>
@endsection