<div class="row">
    <form action="{{ $device->id ? route($url.'.update', $device->id) : route($url.'.store') }}" method="POST" role="form">
        @csrf
        @if($device->id)
            @method('put')
        @endif
        <div class="col-sm-6">
            <div class="form-group">
                <label for="name">Name *</label>
                <input type="text" name="name" id="name" class="form-control"
                       value="{{ old('name', $device->name) }}">

                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label for="type">Type *</label>
                <input type="text" name="type" id="type" class="form-control"
                       value="{{ old('type', $device->type) }}">

                @if ($errors->has('type'))
                    <span class="invalid-feedback" role="alert">
                    <strong class="text-danger">{{ $errors->first('type') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="col-sm-12">
            <input type="submit" class="btn btn-primary" value="{{ $device->id ? 'Update' : 'Save' }}">
        </div>
    </form>
</div>