@extends('layouts/app')

@section('content')

    @php
        //Get avg for all the months on monthly basis
        /*$total_months = $group_months->count();
        $total_usage = $con_months->usage->sum('daily_usage');
        $average_monthly = round(($total_usage  / 1048576) / $total_months, 2);

        $total_days = $group_days->count();
        $total_usage = $con_days->usage->sum('daily_usage');
        $average_daily = round(($total_usage / 1048576) / $total_days, 2);*/
    @endphp

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="sim_details">
                        <span class="switch"><input type="checkbox"
                                                    class="js-switch" {{ $con->status == 'ACTIVATED' ? 'checked' : '' }}></span>
                        <span class="bold mdi mdi-fiber-manual-record {{ $con->status == 'ACTIVATED' ? 'text-success' : 'text-danger' }}"></span>
                        <span class="bold">{{ $con->status == 'ACTIVATED' ? 'Online' : 'Offline' }}</span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="sim_details">
                        <span class="icon mdi mdi-error-outline"></span>
                        <span class="bold">ICCID:</span>
                        <span class="value">{{ $con->iccid }}</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="sim_details">
                        <span class="icon mdi mdi-data-usage"></span>
                        <span class="bold">Type:</span>
                        <span class="value">{{ '??' }}</span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="sim_details">
                        <span class="icon mdi mdi-network-cell"></span>
                        <span class="bold">Network:</span>
                        <span class="value">{{ '??' }}</span>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="sim_details">
                        <div class="dropdown more">
                            <a class="btn btn-link dropdown-toggle" type="button" id="nh_5_dd" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="true">
                                <i class="mdi mdi-more-horiz"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_5_dd">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(!$day_wise->isEmpty() && !$month_wise->isEmpty())
    <div class="container form-group">
        <div class="bg-section chart-section">
            <div class="row section-head">
                <div class="col-sm-2"><h4 class="heading">DATA USAGE (MB)</h4></div>
                <div class="col-sm-4">
                    <h4>{{ Carbon\Carbon::now()->subDays(15)->format('d M Y') }} - {{ Carbon\Carbon::now()->subDays(1)->format('d M Y') }}</h4>
                </div>
                <div class="col-sm-4">
                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#daily-tab">Daily</a></li>
                        <li><a data-toggle="pill" href="#monthly-tab">Monthly</a></li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <a href="" id="range_picker" class="btn btn-default"><img src="{{ asset('assets/images/icons/calendar.png') }}"
                                                            class="btn-icons" alt="Image"></a>
                    <a href="" class="btn btn-default"><img src="{{ asset('assets/images/icons/file_download.png') }}"
                                                            class="btn-icons" alt="Image"></a>
                </div>
            </div>
            <div class="row chart-stats">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="daily-tab">
                        <div class="col-sm-9">
                            <div id="sim-daily-chart"></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                @php
                                $daywise_total = $day_wise->sum('daily_usage');
                                $daywise_avg = round($daywise_total / $day_wise->count(), 2);
                                $daywise_sessions = $day_wise->sum('daily_sessions');
                                @endphp
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $daywise_total }}</div>
                                    <div class="stat-label">Total Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $daywise_avg }}</div>
                                    <div class="stat-label">Average Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $daywise_sessions }}</div>
                                    <div class="stat-label">Sessions</div>
                                </div>
                                {{--<div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '35' }}</div>
                                    <div class="stat-label">Session Timeouts</div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="monthly-tab">
                        <div class="col-sm-9">
                            <div id="sim-monthly-chart"></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                @php
                                    $monthwise_total = $month_wise->sum('daily_usage');
                                    $monthwise_avg = round($monthwise_total / $month_wise->count(), 2);
                                    $monthwise_sessions = $month_wise->sum('daily_sessions');
                                @endphp
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $monthwise_total }}</div>
                                    <div class="stat-label">Total Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $monthwise_avg  }}</div>
                                    <div class="stat-label">Average Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $monthwise_sessions }}</div>
                                    <div class="stat-label">Sessions</div>
                                </div>
                                {{--<div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ '35' }}</div>
                                    <div class="stat-label">Session Timeouts</div>
                                </div>--}}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @endif

    <div class="container">
        <div class="bg-section">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <h4 class="heading">CONNECTION HISTORY</h4>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-flex no-footer" id="connection_history_table">
                            <thead>
                            <tr>
                                <th>ICCID</th>
                                <th>IP Address</th>
                                <th>Session Start</th>
                                <th>Session End</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{ $session['iccid'] }}</td>
                                <td>{{ $session['ipAddress'] }}</td>
                                <td>{{ $session['dateSessionStarted'] }}</td>
                                <td>{{ $session['dateSessionEnded'] }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/switchery/dist/switchery.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/highcharts/css/highcharts.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.js') }}"></script>
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
    <script>
        $(document).ready(function (e) {
            var today = '{{ \Carbon\Carbon::now()->format('Y-m-d') }}';
            var url = '{{ url('connection-dayssearch') }}'+ '/{{ $con->iccid }}/{{ \Carbon\Carbon::now()->subDays(7)->format('Y-m-d') }}/' + today;
            var url2 = '{{ url('connection-monthsearch') }}'+ '/{{ $con->iccid }}/{{ \Carbon\Carbon::now()->subMonth(12)->format('Y-m-d') }}/' + today;

            function set_dayschart(day_data) {
                var days_chart = Highcharts.chart('sim-daily-chart', {
                    chart: {
                        type: 'column'
                    },
                    title: false,
                    xAxis: {
                        type: 'category',
                        labels: {
                            step: 2,
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: false
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'DATA USAGE: <b>{point.y:.1f} (MB)</b>'
                    },
                    series: [{
                        name: 'Usage',
                        data: day_data
                    }]
                });
            }
            function set_monthchart(month_data) {
                var month_chart = Highcharts.chart('sim-monthly-chart', {
                    chart: {
                        type: 'column'

                    },
                    title: false,
                    xAxis: {
                        type: 'category',
                        labels: {
                            step: 2,
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: false
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'DATA USAGE: <b>{point.y:.1f} (MB)</b>'
                    },
                    series: [{
                        name: 'Usage',
                        data: month_data
                    }]
                });
            }

            $.get(url, function (daywise_usage) {
                console.log(daywise_usage);
                //set_dayschart(daywise_usage);
            });
            $.get(url2, function (monthwise_usage) {
                console.log(monthwise_usage);
                //set_monthchart(monthwise_usage, true);
            });

            var range_picker = $('#range_picker');
            range_picker.on('click', function (e) {
                e.preventDefault();
            });
            range_picker.daterangepicker({
                showOn: 'button',
                autoclose: true
            });
            range_picker.daterangepicker().on('apply.daterangepicker', function (e, picker) {
                var startDate = picker.startDate.format('YYYY-MM-DD');
                var endDate = picker.endDate.format('YYYY-MM-DD');

                var url = '{{ url('connection-dayssearch') }}'+ '/{{ $con->iccid }}' + '/' + startDate+'/'+endDate;
                var url2 = '{{ url('connection-monthsearch') }}'+ '/{{ $con->iccid }}' + '/' + startDate+'/'+endDate;

                $.get(url, function (daywise_usage) {
                    console.log(daywise_usage);
                    //set_dayschart(daywise_usage, true);
                });
                $.get(url2, function (monthwise_usage) {
                    console.log(monthwise_usage);
                    //set_monthchart(monthwise_usage, true);
                });
            });

            Highcharts.chart('sim-daily-chart', {
                chart: {
                    type: 'column'

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 2,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.2f} (MB)</b>'
                },
                series: [{
                    name: 'Usage',
                    data: [
                            @foreach($day_wise as $day)

                            @if($loop->last)
                            ['{{ $day->days }}', {{ $day->daily_usage }}]
                                @else
                            ['{{ $day->days }}', {{ $day->daily_usage }}],
                        @endif

                        @endforeach
                    ]
                }]
            });
            Highcharts.chart('sim-monthly-chart', {
                chart: {
                    type: 'column',

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 2,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.2f} (MB)</b>'
                },
                series: [{
                    name: 'Usage',
                    data: [
                            @foreach($month_wise as $month)

                            @if($loop->last)
                            ['{{ $month->months }}', {{ $month->daily_usage }}]
                                @else
                            ['{{ $month->months }}', {{ $month->daily_usage }}],
                        @endif

                        @endforeach
                    ]
                }]
            });
        });
    </script>
@endsection
