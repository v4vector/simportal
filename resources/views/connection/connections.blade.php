@extends('layouts/app')

@section('content')

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#connections-tab">Connections</a></li>
                        <li><a data-toggle="pill" href="#devices-tab">Devices</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="container">
        <div class="bg-section">
            <div class="row">
                <div class="col-sm-12">
                    <div class="tab-content">
                        <div id="connections-tab" class="tab-pane fade in active">
                            <div class="table-responsive">
                                <table class="table table-flex dataTable" id="connections_table">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div class="icheck-primary">
                                                <input type="checkbox" id="connections_head"/>
                                                <label for="connections_head"></label>
                                            </div>
                                        </th>
                                        <th></th>
                                        <th>Status</th>
                                        <th>ICCID</th>
                                        <th>Usage (MB)</th>
                                        <th>Added On</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($connections))
                                        @foreach($connections as $con)
                                        @php
                                        $status = $con->status == 'ACTIVATED' ? 'text-success' : 'text-danger';
                                        $checkbox = $con->status == 'ACTIVATED' ? 'checked' : '';
                                        $usage = $con->usage->filter(function($q){
                                                return $q->created_at->format('Y-m') == date('Y-m');
                                            })->sum('daily_usage');
                                        @endphp
                                        <tr>
                                            <td>
                                                <div class="icheck-primary">
                                                    <input type="checkbox" id="{{ 'connection_'.$loop->iteration }}"/>
                                                    <label for="{{ 'connection_'.$loop->iteration }}"></label>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="checkbox" class="js-switch" data-content="{{ $con->status }}" data-id="{{ $con->iccid }}" {{ $checkbox }}>
                                            </td>
                                            <td><i class="mdi mdi-fiber-manual-record {{ $status }}"></i></td>

                                            @if($usage > 0)
                                            <td><a href="{{ url('connections/'.$con->iccid) }}">{{ $con->iccid }}</a></td>
                                            @else
                                                <td>{{ $con->iccid }}</td>
                                            @endif
                                            <td>{{ round($usage / 1048576, 2) }}</td>

                                            <td>{{ date('d/m/Y H:i', strtotime($con->dateAdded)) }}</td>
                                            <td>
                                                <div class="dropdown">
                                                    <a class="btn btn-link dropdown-toggle" type="button" id="nh_5_dd"
                                                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                        <i class="mdi mdi-more-horiz"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_5_dd">
                                                        <li><a href="{{ route('connections.edit', $con) }}">Add Deal Info</a></li>
                                                        <li><a href="#">Another action</a></li>
                                                        <li><a href="#">Something else here</a></li>
                                                        <li role="separator" class="divider"></li>
                                                        <li><a href="#">Separated link</a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="devices-tab" class="tab-pane fade">
                            <div class="table-responsive">
                                <table class="table table-flex dataTable" id="devices_table">
                                    <thead>
                                    <tr>
                                        <th>
                                            <div class="icheck-primary">
                                                <input type="checkbox" id="devices_head"/>
                                                <label for="devices_head"></label>
                                            </div>
                                        </th>
                                        <th>Name</th>
                                        <th>Usage (MB)</th>
                                        <th>Type</th>
                                        <th>Date Added</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($devices as $device)
                                    <tr>
                                        <td>
                                            <div class="icheck-primary">
                                                <input class="select_device" type="checkbox" id="nh_1"/>
                                                <label for="nh_1"></label>
                                            </div>
                                        </td>
                                        <td><a href="{{ url('devices/'.$device->slug) }}">{{ ucwords($device->name) }}</a></td>
                                        <td>{{ round($device->usage->sum('daily_usage') / 1048576) }}</td>
                                        <td>{{ $device->type }}</td>
                                        <td>{{ $device->created_at->format('d/m/Y H:i') }}</td>
                                        <td>
                                            <div class="dropdown">
                                                <a class="btn btn-link dropdown-toggle" type="button" id="nh_1_dd"
                                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    <i class="mdi mdi-more-horiz"></i>
                                                </a>
                                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_1_dd">
                                                    <li><a href="{{ route('device.connections', $device->slug) }}">Device Connections</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/switchery/dist/switchery.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.js') }}"></script>
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.js-switch').on('change', function (e) {
                var iccid = $(this).data('id');
                var status = $(this).data('content');
                console.log(status);
                $.ajax({
                    method: "GET",
                    url: "{{ url('connections') }}" +'/'+iccid+'/update',
                    data: {status: status},
                    success: function (result) {
                        console.log(result);
                    }
                });
            });
        });
    </script>
@endsection
