@extends('layouts.app-login')

@section('content')
    <div class="container">
        <div class="popupBG">
            <div class="popup-logo">
                <img src="{{ asset('assets/images/water-telecom-logo.png') }}" alt="Water Telecom">
            </div>
            <div class="form-group">
                @if (session('status'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
            <form action="{{ route('login') }}" method="POST" role="form">
                @csrf
                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="mdi mdi-person"></i></span>
                    <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" id="email" placeholder="Username" autofocus>
                </div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif
                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="mdi mdi-vpn-key"></i></span>
                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" id="password" placeholder="Password">
                </div>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif
                <div class="row">
                    <div class="col-xs-6">
                        <a href="{{ route('password.request') }}" class="gray">Forget Password?</a>
                    </div>
                    <div class="col-xs-6 text-right">
                        <button type="submit" class="btn btn-primary btn-standard btn-rounded">LOGIN</button>
                    </div>
                </div>

            </form>
            <hr>
            <div class="text-center">
                <a href="{{ url('register_view') }}"><strong>Register for a Water Telecom account</strong></a>
            </div>
        </div>
    </div>
@endsection