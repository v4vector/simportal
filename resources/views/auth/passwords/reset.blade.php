@extends('layouts.app-login')

@section('content')
    <div class="container">
        <div class="popupBG">
            <div class="popup-logo">
                <img src="{{ asset('assets/images/water-telecom-logo.png') }}" alt="Water Telecom">
            </div>
            <form action="{{ route('login') }}" method="POST" role="form">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="mdi mdi-person"></i></span>
                    <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" id="email" value="{{ $email ?? old('email') }}" placeholder="Username" autofocus>
                </div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif


                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="mdi mdi-vpn-key"></i></span>
                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" id="password" placeholder="Password">
                </div>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif

                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="mdi mdi-vpn-key"></i></span>
                    <input type="password" class="form-control"
                           name="password_confirmation" id="password-confirm" placeholder="Password">
                </div>
                @if ($errors->has('password_confirmation'))
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                @endif


                <div class="row">
                    <div class="col-xs-6">
                        <a href="{{ route('password.request') }}" class="gray">Forget Password?</a>
                    </div>
                    <div class="col-xs-6 text-right">
                        <button type="submit" class="btn btn-primary btn-rounded">LOGIN</button>
                    </div>
                </div>

            </form>
            <hr>
            <div class="text-center">
                <a href="{{ route('register') }}"><strong>Register for a Water Telecom account</strong></a>
            </div>
        </div>
    </div>
@endsection