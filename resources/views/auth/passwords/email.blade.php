@extends('layouts.app-login')

@section('content')
    <div class="container">
        <div class="popupBG">
            <div class="row form-group">
                <div class="col-sm-12">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="popup-logo">
                <img src="{{ asset('assets/images/water-telecom-logo.png') }}" alt="Water Telecom">
            </div>
            <form action="{{ route('password.email') }}" method="POST" role="form">
                @csrf
                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="mdi mdi-person"></i></span>
                    <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" id="email" placeholder="Email" autofocus>
                </div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif


                <div class="row">
                    <div class="col-xs-12 text-right">
                        <button type="submit" class="btn btn-primary btn-rounded btn-block">Send Password Reset Link</button>
                    </div>
                </div>

            </form>
            <hr>
            <div class="text-center">
                <a href="{{ route('login') }}"><strong>Login to Water Telecom account</strong></a>
            </div>
        </div>
    </div>
@endsection