@extends('layouts.app-login')

@section('content')
    <div class="container">
        <div class="popupBG">
            <div class="popup-logo">
                <img src="{{ asset('assets/images/water-telecom-logo.png') }}" alt="Water Telecom">
            </div>
            <form action="{{ route('register') }}" method="POST" role="form">
                @csrf

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                   name="first_name" id="first_name" placeholder="First Name" autofocus>
                        </div>
                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                   name="last_name" id="last_name" placeholder="Last Name" autofocus>
                        </div>
                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                  <label for="sel1">Select Role</label>
                  <select class="form-control" name="role">
                    
                    <option value="{{$roles[2]->id}}">{{$roles[2]->name}}</option>
                    <option value="{{$roles[3]->id}}">{{$roles[3]->name}}</option>
                    
                  </select>
                </div>

                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="mdi mdi-person"></i></span>
                    <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" id="email" placeholder="Username" autofocus>
                </div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                @endif


                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="mdi mdi-vpn-key"></i></span>
                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" id="password" placeholder="Password">
                </div>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                @endif

                <div class="form-group input-group">
                    <span class="input-group-addon"><i class="mdi mdi-vpn-key"></i></span>
                    <input type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                           name="password_confirmation" id="password-confirmation" placeholder="Password Confirmation">
                </div>
                @if ($errors->has('password_confirmation'))
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                @endif


                <div class="row">
                    <div class="col-xs-offset-6 col-xs-6 text-right">
                        <button type="submit" class="btn btn-primary btn-rounded">Register</button>
                    </div>
                </div>

            </form>
            <hr>
            <div class="text-center">
                <a href="{{ route('login') }}"><strong>Login to Water Telecom account</strong></a>
            </div>
        </div>
    </div>
@endsection 