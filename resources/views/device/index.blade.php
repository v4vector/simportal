@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
@endsection
@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
@endsection

@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-bordered dataTable no-footer">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Type</th>
                                <th>Added On</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($devices as $device)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><a href="">{{ $device->name }}</a></td>
                                    <td>{{ $device->type }}</td>
                                    <td>{{ $device->created_at->format('d M, Y') }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection