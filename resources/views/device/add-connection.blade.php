@extends('layouts.app')

@section('styles')
@endsection
@section('scripts')
@endsection

@section('content')
    <section class="section">
        <div class="container">


            <div class="row">
                <div class="col-sm-6">
                    <form method="post" action="{{ route('connection.store', $device) }}">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="unassigned">Select Connections *</label>
                            <select name="unassigned[]" id="unassigned" class="form-control" multiple required>
                                @foreach($unassined as $con)
                                    <option value="{{ $con->id }}">{{ $con->iccid }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Add">
                        </div>
                    </form>
                </div>


                <div class="col-sm-6">
                    <form method="post" action="{{ route('connection.remove', $device) }}">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="assigned">Select Connections *</label>
                            <select name="assigned[]" id="assigned" class="form-control" multiple required>
                                @foreach($assined as $con)
                                    <option value="{{ $con->id }}">{{ $con->iccid }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="Remove">
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </section>
@endsection