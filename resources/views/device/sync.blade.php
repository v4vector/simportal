@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="container">
            <div id="msg"></div>

            <button class="btn btn-primary btn-block" id="sync-btn">Sync Devices Now!</button>

        </div>
    </section>
@endsection

@section('styles')
@endsection
@section('scripts')
    <script>
        $(document).ready(function (e) {
            $('#sync-btn').on('click', function (e) {
                e.preventDefault();
                $(this).addClass('disabled');
                url = '{{ route('sync-ajax') }}';
                $('#msg').text('Please be patience! It will take a while');

                var request = $.ajax({
                    url: url,
                    success: function (data, textStatus, xhr) {
                        $('#msg').text('Devices Synchronized Successfully!');
                    },
                    error: function (data, textStatus, xhr) {
                        $('#msg').text('Something Went Wrong. Try again!');
                    },
                    complete: function (jqXHR, status) {
                        $('#sync-btn').removeClass('disabled');
                    }
                });
                /*request.done(function(){
                    $(this).removeClass('disabled');
                });*/

            });
        })
    </script>
@endsection
