@extends('layouts/app')

@section('content')

    @php
        //Get avg for all the months on monthly basis
    @endphp

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="sim_details">
                        <span class="icon mdi mdi-error-outline"></span>
                        <span class="bold">Device</span>
                        <span class="value">{{ $device->name }}</span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="sim_details">
                        <span class="icon mdi mdi-network-cell"></span>
                        <span class="bold">Connections</span>
                        <span class="value">{{ $device->connections->count() }}</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="sim_details">
                        <span class="icon mdi mdi-data-usage"></span>
                        <span class="bold">Data Usage</span>
                        <span class="value">{{ round($month_wise->sum('daily_usage') / 1048576 , 2) }}</span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="sim_details">
                        <span class="icon mdi mdi-error-outline"></span>
                        <span class="bold">Type</span>
                        <span class="value">{{ $device->type }}</span>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="sim_details">
                        <div class="dropdown more">
                            <a class="btn btn-link dropdown-toggle" type="button" id="nh_5_dd" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="true">
                                <i class="mdi mdi-more-horiz"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="nh_5_dd">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(!$day_wise->isEmpty() && !$month_wise->isEmpty())
    <div class="container form-group">
        <div class="bg-section chart-section">
            <div class="row section-head">
                <div class="col-sm-2"><h4 class="heading">DATA USAGE (MB)</h4></div>
                <div class="col-sm-4"><h4>{{ Carbon\Carbon::now()->subDays(15)->format('d M Y') }}
                        - {{ Carbon\Carbon::now()->subDays(1)->format('d M Y') }}</h4></div>
                <div class="col-sm-4">
                    <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#daily-tab">Daily</a></li>
                        <li><a data-toggle="pill" href="#monthly-tab">Monthly</a></li>
                    </ul>
                </div>
                <div class="col-sm-2">
                    <a href="" class="btn btn-default"><img src="{{ asset('assets/images/icons/calendar.png') }}"
                                                            class="btn-icons" alt="Image"></a>
                    <a href="" class="btn btn-default"><img src="{{ asset('assets/images/icons/file_download.png') }}"
                                                            class="btn-icons" alt="Image"></a>
                </div>
            </div>

            <div class="row chart-stats">
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="daily-tab">
                        <div class="col-sm-9">
                            <div id="daily-device-chart"></div>
                        </div>
                        <div class="col-sm-3">
                            @php
                            $total_daywise = $day_wise->sum('daily_usage');
                            $avg_daywise = round($total_daywise / $day_wise->count(), 2);
                            $sessions_daywise = $day_wise->sum('daily_sessions');
                            @endphp
                            <div class="row">
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $total_daywise }}</div>
                                    <div class="stat-label">Total Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $avg_daywise }}</div>
                                    <div class="stat-label">Average Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $sessions_daywise }}</div>
                                    <div class="stat-label">Sessions</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="monthly-tab">
                        <div class="col-sm-9">
                            <div id="monthly-device-chart"></div>
                        </div>
                        <div class="col-sm-3">
                            @php
                                $total_monthwise = $month_wise->sum('daily_usage');
                                $avg_monthwise = round($total_monthwise / $month_wise->count(), 2);
                                $sessions_monthwise = $month_wise->sum('daily_sessions');
                            @endphp
                            <div class="row">
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $total_monthwise }}</div>
                                    <div class="stat-label">Total Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $avg_monthwise }}</div>
                                    <div class="stat-label">Average Usage</div>
                                </div>
                                <div class="col-xs-6 col-sm-12 stat form-group">
                                    <div class="stat-value">{{ $sessions_monthwise }}</div>
                                    <div class="stat-label">Sessions</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    @endif
    <div class="container">
        <div class="bg-section">
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-flex dataTable" id="connections_table">
                            <thead>
                            <tr>
                                <th>
                                    <div class="icheck-primary">
                                        <input type="checkbox" id="connections_head"/>
                                        <label for="connections_head"></label>
                                    </div>
                                </th>
                                <th></th>
                                <th>Status</th>
                                <th>ICCID</th>
                                <th>Usage (MB)</th>
                                <th>Network</th>
                                <th>Date Added</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($device->connections))
                                @foreach($device->connections as $con)
                                    @php
                                        $status = $con->status == 'ACTIVATED' ? 'text-success' : 'text-danger';
                                        $checkbox = $con->status == 'ACTIVATED' ? 'checked' : '';
                                    @endphp
                                    <tr>
                                        <td>
                                            <div class="icheck-primary">
                                                <input type="checkbox" id="{{ $loop->iteration }}"/>
                                                <label for="{{ $loop->iteration }}"></label>
                                            </div>
                                        </td>
                                        <td>
                                            <input type="checkbox" class="js-switch" {{ $checkbox }}>
                                        </td>
                                        <td><i class="mdi mdi-fiber-manual-record {{ $status }}"></i></td>
                                        <td>{{ $con->iccid }}</td>
                                        <td>{{ round($con->usage->sum('daily_usage') / 1048576, 2) }}</td>
                                        <td>{{ '??' }}</td>
                                        <td>{{ $con->created_at->format('d M Y') }}</td>
                                        <td><i class="mdi mdi-more-horiz"></i></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/media/css/jquery.dataTables.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/switchery/dist/switchery.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/highcharts/css/highcharts.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('assets/plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/switchery/dist/switchery.js') }}"></script>
    <script src="{{ asset('assets/plugins/highcharts/js/highcharts.js') }}"></script>
    <script>
        $(document).ready(function (e) {
            Highcharts.chart('daily-device-chart', {
                chart: {
                    type: 'column',

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 2,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.1f} (MB)</b>'
                },
                series: [{
                    name: 'Usage',
                    data: [
                        @foreach($day_wise as $day)

                            @if($loop->last)
                                ['{{ $day->days }}', {{ $day->daily_usage }}]
                            @else
                                ['{{ $day->days }}', {{ $day->daily_usage }}],
                            @endif

                        @endforeach
                    ]
                }]
            });
            Highcharts.chart('monthly-device-chart', {
                chart: {
                    type: 'column',

                },
                title: false,
                xAxis: {
                    type: 'category',
                    labels: {
                        step: 2,
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: false
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'DATA USAGE: <b>{point.y:.1f} (MB)</b>'
                },
                series: [{
                    name: 'Population',
                    data: [
                        @foreach($month_wise as $month)

                            @if($loop->last)
                                ['{{ $month->months }}', {{ $month->daily_usage }}]
                            @else
                                ['{{ $month->months }}', {{ $month->daily_usage }}],
                            @endif

                        @endforeach
                    ]
                }]
            });
        });
    </script>
@endsection