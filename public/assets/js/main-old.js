jQuery(document).ready(function(){

       var elements = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

       elements.forEach(function(html) {
           var switchery = new Switchery(html, {color: '#D1E3F8', jackColor: '#4a90e2', size: 'small'});
       });
});
jQuery(document).ready(function ($) {
    $('#connections_head, #devices_head').on('change', function (el) {
        if ($(this).is(':checked')) {
            $(this).closest('.table').find('tbody').find('.icheck-primary').find('input').prop('checked', true);
        } else {
            $(this).closest('.table').find('tbody').find('.icheck-primary').find('input').prop('checked', false);
        }
    });
    if(jQuery.fn.dataTable){
        jQuery('.dataTable').dataTable({
            "dom": '<"pull-left"f>t',
            "bPaginate": false,
            "bLengthChange": false,
            "bInfo": false,
            "bAutoWidth": false,
            /*"oLanguage": {
                "sSearch": ""
            },*/
            "language": {
                "search": '',
                "searchPlaceholder": "Search",
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
    }
});