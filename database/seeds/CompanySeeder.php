<?php

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = [
            ['name'=>'First Client Comp', 'address'=>'abcdef', 'postcode'=>'43350','company_number'=>'12345', 'taxID'=>'12345', 'notes'=>'This is notes', 'user_id'=> '3'],
            ['name'=>'Adam Holden Comp', 'address'=>'abcdef', 'postcode'=>'43350','company_number'=>'12345', 'taxID'=>'12345', 'notes'=>'This is notes', 'user_id'=> '4'],
            ['name'=>'Dave Taylor Comp', 'address'=>'abcdef', 'postcode'=>'43350','company_number'=>'12345', 'taxID'=>'12345', 'notes'=>'This is notes', 'user_id'=> '5'],
            ['name'=>'Dan Josiah Comp', 'address'=>'abcdef', 'postcode'=>'43350','company_number'=>'12345', 'taxID'=>'12345', 'notes'=>'This is notes', 'user_id'=> '6'],
        ];

        foreach ($companies as $com){
            $co = new Company($com);
            $co->save();
        }
    }
}
