<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'super.admin@gmail.com',
            'account_phone' => '+000000000',
            'account_phone' => '+000000000',
            'password' => Hash::make('superadmin'),
            'email_verified_at' => Carbon::now()
        ];

        $user = new User($data);
        $user->save();
        $user->assignRole('superuser');

        $credentials = [
            'first_name' => 'Aaqib',
            'last_name' => 'Mehran',
            'mobile' => '03238502424',
            'email' => 'mehran.aaqib@gmail.com',
            'account_phone' => '03238502424',
            'account_email' => 'mehran.aaqib@gmail.com',
            'password' => Hash::make('aaqibmehran'),
            'email_verified_at' => Carbon::now()
        ];
        $user = new User($credentials);
        $user->save();
        $user->assignRole('admin');

        $credentials = [
            'first_name' => 'Admin',
            'last_name' => '',
            'mobile' => '03238502424',
            'email' => 'admin@gmail.com',
            'account_phone' => '03238502424',
            'account_email' => 'admin@gmail.com',
            'password' => Hash::make('admin'),
            'email_verified_at' => Carbon::now()
        ];
        $user = new User($credentials);
        $user->save();
        $user->assignRole('admin');

        $credentials = [
            'first_name' => 'First',
            'last_name' => 'Client',
            'mobile' => '03238502424',
            'email' => 'firstclient@gmail.com',
            'account_phone' => '03238502424',
            'account_email' => 'firstclient@gmail.com',
            'user_id' => 1,
            'password' => Hash::make('firstclient'),
            'email_verified_at' => Carbon::now()
        ];
        $user = new User($credentials);
        $user->save();
        $user->assignRole('client');



        $credentials = [
            'first_name' => 'Adam',
            'last_name' => 'Holdan',
            'mobile' => '03238502424',
            'email' => 'AdamL@holdan.co.uk',
            'account_phone' => '03238502424',
            'account_email' => 'AdamL@holdan.co.uk',
            'user_id' => 1,
            'password' => Hash::make('adaml'),
            'email_verified_at' => Carbon::now()
        ];
        $user = new User($credentials);
        $user->save();
        $user->assignRole('client');

        $credentials = [
            'first_name' => 'Dave',
            'last_name' => 'Taylor',
            'mobile' => '03238502424',
            'email' => 'dave.taylor@streamingtank.com',
            'account_phone' => '03238502424',
            'account_email' => 'dave.taylor@streamingtank.com',
            'user_id' => 1,
            'password' => Hash::make('dave.taylor'),
            'email_verified_at' => Carbon::now()
        ];
        $user = new User($credentials);
        $user->save();
        $user->assignRole('client');

        $credentials = [
            'first_name' => 'Dan',
            'last_name' => 'Josiah',
            'mobile' => '03238502424',
            'email' => 'dan.josiah@outlook.com',
            'account_phone' => '03238502424',
            'account_email' => 'dan.josiah@outlook.com',
            'user_id' => 1,
            'password' => Hash::make('dan.josiah'),
            'email_verified_at' => Carbon::now()
        ];
        $user = new User($credentials);
        $user->save();
        $user->assignRole('client');

        $credentials = [
            'first_name' => 'Sale',
            'last_name' => 'Agent',
            'mobile' => '03238502424',
            'email' => 'saleagent@gmail.com',
            'account_phone' => '03238502424',
            'account_email' => 'saleagent@gmail.com',
            'password' => Hash::make('saleagent'),
            'email_verified_at' => Carbon::now()
        ];
        $user = new User($credentials);
        $user->save();
        $user->assignRole('saleagent');
    }
}
