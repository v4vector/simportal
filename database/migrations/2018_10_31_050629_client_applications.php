<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ClientApplication;

class ClientApplications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        $applications = [
            ['name'=> 'Machine 2 Machine'],
            ['name'=> 'IoT'],
            ['name'=> 'BroadCast'],
            ['name'=> 'Retail Fail Over'],
            ['name'=> 'Business Back up'],
            ['name'=> 'Construction'],
            ['name'=> 'Energy'],
            ['name'=> 'Maritime'],
            ['name'=> 'Sensor'],
            ['name'=> 'Metering'],
            ['name'=> 'CCTV'],
            ['name'=> 'Smart Device'],
            ['name'=> 'Corporate Personal'],
            ['name'=> 'Corporate Device']
        ];

        foreach($applications as $app){
            $q = new ClientApplication;
            $q->name = $app['name'];
            $q->created_at = \Carbon\Carbon::now();
            $q->updated_at = \Carbon\Carbon::now();
            $q->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_applications');
    }
}
