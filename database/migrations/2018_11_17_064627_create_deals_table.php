<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quote_id')->unsigned();
            $table->integer('product_type_id')->unsigned();
            $table->string('data_type');
            $table->string('data');
            $table->string('network_preferences');
            $table->integer('sim_type')->unsigned();
            $table->integer('sim_volume')->unsigned();
            $table->integer('data_volume_id')->unsigned();
            $table->string('contract_len');
            $table->integer('client_application_id')->unsigned();

            $table->double('cost_sims')->unsigned();
            $table->double('vat_sims')->unsigned();
            $table->double('total_sims_cost')->unsigned();

            $table->double('base_price')->unsigned();
            $table->double('margin_sales')->unsigned();
            $table->double('margin_admin')->unsigned();
            $table->double('client_price')->unsigned();
            $table->double('deal_amount')->unsigned();
            $table->double('vat')->unsigned();
            $table->double('total_deal')->unsigned();
            $table->double('total_commission')->unsigned();

            $table->string('payment_terms');
            $table->double('advance_payment')->unsigned();
            $table->double('installment')->nullable()->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
