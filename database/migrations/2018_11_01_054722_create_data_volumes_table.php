<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\DataVolume;
use Carbon\Carbon;

class CreateDataVolumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_volumes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('volume');
            $table->string('type');
            $table->timestamps();
        });

        $low = [
            ['type'=>'low_data', 'name'=> '1 MB', 'volume'=> '1'],
            ['type'=>'low_data', 'name'=> '3 MB', 'volume'=> '3'],
            ['type'=>'low_data', 'name'=> '5 MB', 'volume'=> '5'],
            ['type'=>'low_data', 'name'=> '10 MB', 'volume'=> '10'],
            ['type'=>'low_data', 'name'=> '25 MB', 'volume'=> '25'],
            ['type'=>'low_data', 'name'=> '50 MB', 'volume'=> '50'],
            ['type'=>'low_data', 'name'=> '100 MB', 'volume'=> '100'],
            ['type'=>'low_data', 'name'=> '250 MB', 'volume'=> '250'],
            ['type'=>'low_data', 'name'=> '500 MB', 'volume'=> '500'],
            ['type'=>'low_data', 'name'=> '1000 MB', 'volume'=> '1000']
        ];
        foreach($low as $l){
            $dv = new DataVolume($l);
            $dv->save();
        }

        $high = [
            ['type'=>'high_data', 'name'=> '1 GB', 'volume'=> '1'],
            ['type'=>'high_data', 'name'=> '3 GB', 'volume'=> '3'],
            ['type'=>'high_data', 'name'=> '5 GB', 'volume'=> '5'],
            ['type'=>'high_data', 'name'=> '7 GB', 'volume'=> '7'],
            ['type'=>'high_data', 'name'=> '50 GB', 'volume'=> '50'],
            ['type'=>'high_data', 'name'=> '100 GB', 'volume'=> '100'],
        ];
        foreach($high as $h){
            $dv = new DataVolume($h);
            $dv->save();
        }

        $high = [];
        $inc = 130;
        while ($inc <= 1000){
            $high[] = [
                'type' => 'high_data',
                'name' => $inc . ' GB',
                'volume' => $inc,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
            $inc += 30;
        }

        DataVolume::insert($high);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_volumes');
    }
}
