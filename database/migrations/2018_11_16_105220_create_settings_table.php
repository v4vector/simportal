<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Setting;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('setting_key');
            $table->string('setting_val');
            $table->timestamps();
        });

        $set = new Setting;
        $set->setting_key = 'app_name';
        $set->setting_val = 'Water Telecom';
        $set->save();

        $set = new Setting;
        $set->setting_key = 'per_mb_rate';
        $set->setting_val = '0.0061';
        $set->save();

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
