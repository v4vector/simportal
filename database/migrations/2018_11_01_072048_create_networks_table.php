<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Network;

class CreateNetworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('networks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        $nets = [
            ['name'=> 'EE'],
            ['name'=> 'O2'],
            ['name'=> 'Voda'],
            ['name'=> 'EU KPN'],
            ['name'=> 'Manx'],
        ];

        foreach($nets as $net){
            $n = new Network($net);
            $n->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('networks');
    }
}
