<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\SimType;

class CreateSimTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sim_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('price');
            $table->timestamps();
        });

        $types = [
            ['name' => 'Standard Plastic', 'price'=> '2.00'],
            ['name' => 'Standard Plastic M2M', 'price'=> '2.50'],
            ['name' => 'ESIM Plastic', 'price'=> '4.00'],
            ['name' => 'ESIM Chip', 'price'=> '5.50']];

        foreach($types as $type){
            $t = new SimType($type);
            $t->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sim_types');
    }
}
