<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('connections', function (Blueprint $table) {
            $table->increments('id');
            $table->string('iccid');
            $table->string('imsi');
            $table->string('msisdn');
            $table->string('imei');
            $table->string('status');
            $table->string('ratePlan')->nullable();
            $table->string('communicationPlan')->nullable();
            $table->timestamp('dateActivated')->nullable();
            $table->timestamp('dateAdded')->nullable();
            $table->timestamp('dateUpdated')->nullable();
            $table->timestamp('dateShipped')->nullable();
            $table->string('accountId')->nullable();
            $table->string('operatorCustom1')->nullable();
            $table->string('operatorCustom2')->nullable();
            $table->string('accountCustom1')->nullable();
            $table->string('accountCustom2')->nullable();
            $table->string('customerCustom1')->nullable();
            $table->string('customerCustom2')->nullable();
            $table->string('simNotes')->nullable();
            $table->string('euiccid')->nullable();
            $table->string('deviceID')->nullable();
            $table->string('modemID')->nullable();
            $table->string('globalSimType')->nullable();

            $table->integer('device_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('client')->nullable();
            $table->string('name')->nullable();


            $table->double('monthly_data_limit')->unsigned()->nullable();
            $table->double('total_data_limit')->unsigned()->nullable();
            $table->double('deal_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connections');
    }
}
