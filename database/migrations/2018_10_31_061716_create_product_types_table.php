<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\ProductType;

class CreateProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('territory');
            $table->string('networks');
            $table->double('base_price');
            $table->double('hd_rrp');
            $table->double('ld_rrp');
            $table->timestamps();
        });

        $types = [
            [
                'name'=> 'UK SINGLE',
                'territory' => 'UK ONLY',
                'networks' => 'SINGLE',
                'base_price' => '1.50',
                'hd_rrp' => '3.10',
                'ld_rrp' => '0.05'
            ],
            [
                'name'=> 'UK MULTI',
                'territory' => 'UK ONLY',
                'networks' => 'MULTI',
                'base_price' => '2.80',
                'hd_rrp' => '5.25',
                'ld_rrp' => '0.09'
            ],
            [
                'name'=> 'EU28',
                'territory' => 'EU28',
                'networks' => 'MULTI',
                'base_price' => '6.10',
                'hd_rrp' => '8.50',
                'ld_rrp' => '0.15'
            ],
            [
                'name'=> 'GLOBAL',
                'territory' => 'EU28',
                'networks' => 'MULTI',
                'base_price' => '6.10',
                'hd_rrp' => '8.50',
                'ld_rrp' => '0.15'
            ],
            [
                'name'=> 'GLOBAL',
                'territory' => 'ROW',
                'networks' => 'MULTI',
                'base_price' => '16',
                'hd_rrp' => '25',
                'ld_rrp' => '0.25'
            ],
        ];

        foreach($types as $type){
            $obj = new ProductType($type);
            $obj->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_types');
    }
}
