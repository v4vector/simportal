<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('connection_id');
            $table->double('ctdDataUsage');
            $table->double('daily_usage');
            $table->double('ctdSessionCount');
            $table->double('daily_sessions');

            $table->string('deal_base_price')->nullable();
            $table->string('deal_admin_margin')->nullable();
            $table->string('deal_sales_margin')->nullable();
            $table->string('deal_client_price')->nullable();
            $table->string('bill_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usages');
    }
}
