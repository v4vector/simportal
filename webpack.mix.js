const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/scss/styles.scss', 'public/assets/css');
// mix.minify('public/assets/css/styles.css');

mix.combine([
    'resources/js/libraries.js',
    'resources/js/scripts.js'
], 'public/assets/js/main.js');

mix.browserSync({
    proxy: 'http://localhost/jasper-simportal/public/'
});
