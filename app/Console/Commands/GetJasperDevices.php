<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Connection;
use App\Models\JasperApi;
use App\Models\User;
use Carbon\Carbon;

class GetJasperDevices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jasper:getdevices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Devices from the Jasper API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 300);
        $jasper = new JasperApi;
        $devices = $jasper->all_devices();

        $details = $jasper->details($devices);
        foreach ($details as $device) {
            $device = (array)$device;
            $row = [];

            $email = $device['customerCustom2'];
            $user = User::where('email', $email)->first();
            if ($user) {
                $row['user_id'] = $user->id;
                if ($device['dateActivated']) {
                    $row['dateActivated'] = Carbon::parse($device['dateActivated'])->format('Y-m-d H:i:s');
                }
                if ($device['dateAdded']) {
                    $row['dateAdded'] = Carbon::parse($device['dateAdded'])->format('Y-m-d H:i:s');
                }
                if ($device['dateUpdated']) {
                    $row['dateUpdated'] = Carbon::parse($device['dateUpdated'])->format('Y-m-d H:i:s');
                }
                if ($device['dateShipped']) {
                    $row['dateShipped'] = Carbon::parse($device['dateShipped'])->format('Y-m-d H:i:s');
                }
                $row['iccid'] = $device['iccid'];
                $row['imsi'] = $device['imsi'];
                $row['msisdn'] = $device['msisdn'];
                $row['imei'] = $device['imei'];
                $row['status'] = $device['status'];
                $row['ratePlan'] = $device['ratePlan'];
                $row['communicationPlan'] = $device['communicationPlan'];
                $row['accountId'] = $device['accountId'];
                $row['operatorCustom1'] = $device['operatorCustom1'];
                $row['operatorCustom2'] = $device['operatorCustom2'];
                $row['accountCustom1'] = $device['accountCustom1'];
                $row['accountCustom2'] = $device['accountCustom2'];
                $row['customerCustom1'] = $device['customerCustom1'];
                $row['customerCustom2'] = $device['customerCustom2'];
                $row['simNotes'] = $device['simNotes'];
                $row['euiccid'] = $device['euiccid'];
                $row['deviceID'] = $device['deviceID'];
                $row['modemID'] = $device['modemID'];
                $row['globalSimType'] = $device['globalSimType'];

//                dd($row);


                $connection = Connection::where('iccid', $device['iccid'])->first();
                if (!empty($connection->id)) {
                    $connection->update($row);
                } else {
                    $connection = new Connection($row);
                    $connection->save();
                }
            }
        }
    }
}
