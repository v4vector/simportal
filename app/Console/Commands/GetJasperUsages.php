<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Connection;
use App\Models\JasperApi;
use App\Models\Usage;
use Carbon\Carbon;

class GetJasperUsages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jasper:getusages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will get all the usage information from the jasper api/apis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $already_usages = Usage::with('connection')->whereDate('created_at', date("Y-m-d", strtotime('-1 days')))->get();
//        dd($already_usages);

        $usages = [];
        Connection::chunk(3, function ($five_chunk) use ($already_usages, $usages) {
            sleep(3);

            $jasper = new JasperApi;
            $results = $jasper->usages_sessions($five_chunk);


            foreach ($results as $usage) {
                $usage = \GuzzleHttp\json_decode($usage['value']->getBody());
                $daily_usage = $usage->ctdDataUsage ?? 0;
                $daily_sessions = $usage->ctdSessionCount ?? 0;

//                If there yesterday usages is available and date is not 01
                if (!$already_usages->isEmpty() && date('d') != '01') {
                    $check_yesterday = $already_usages->filter(function ($q) use ($usage) {
                        return $q->connection->iccid == $usage->iccid;
                    })->isEmpty();

                    if (!$check_yesterday) {
                        $yesterday = $already_usages->filter(function ($q) use ($usage) {
                            return $q->connection->iccid == $usage->iccid;
                        })->first();
//                        dd($yesterday);
                        $daily_usage = $usage->ctdDataUsage - $yesterday->ctdDataUsage;
                        $daily_sessions = $usage->ctdSessionCount - $yesterday->daily_sessions;
                    }
                }
//                dd('end');

                $usage = [
                    'connection_id' => $five_chunk->where('iccid', $usage->iccid)->first()->id,
                    'ctdDataUsage' => $usage->ctdDataUsage ?? 0,
                    'ctdSessionCount' => $usage->ctdSessionCount ?? 0,
                    'daily_usage' => $daily_usage,
                    'daily_sessions' => $daily_sessions,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
                $usages[] = $usage;
            }
            Usage::insert($usages);
        });
        
    }
}
