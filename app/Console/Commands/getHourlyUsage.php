<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\JasperApi;
use App\Models\Connection;
use App\Models\Usage;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Setting;

class getHourlyUsage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jasper:usage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('max_execution_time', 1000);
        $already_usages = Usage::
        whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->whereRaw('id IN (select MAX(id) FROM `usages` GROUP BY `connection_id`)')
            ->get();


        $usages = [];
        Connection::chunk(3, function ($five_chunk) use ($already_usages, &$usages) {
            sleep(3);

            $jasper = new JasperApi;
            $results = $jasper->usages_sessions($five_chunk);


            foreach ($results as $usage) {
                $usage = \GuzzleHttp\json_decode($usage['value']->getBody());
                $daily_usage = $usage->ctdDataUsage ?? 0;
                $daily_sessions = $usage->ctdSessionCount ?? 0;


//              First day first hour first minute
                if (date('d') == '01' && date('H' == '00' && date('i' <= '30'))) {
                    //Do nothing
                } else {
                    $check_old = $already_usages->filter(function ($q) use ($usage) {
                        return $q->connection->iccid == $usage->iccid;
                    })->first();

                    if ($check_old) {
                        $daily_usage = $usage->ctdDataUsage - $check_old->ctdDataUsage;
                        $daily_sessions = $usage->ctdSessionCount - $check_old->ctdSessionCount;
                    }
                }

                $cons = Connection::get();
                $rate = Setting::where('setting_key', 'per_mb_rate')->first()->setting_val;
                $bill_amount = ($daily_usage == '0') ? '0' : ($rate / 1048576) * $daily_usage;


                $row = [
                    'connection_id' => $five_chunk->where('iccid', $usage->iccid)->first()->id,
                    'ctdDataUsage' => (string) $usage->ctdDataUsage ?? 0,
                    'ctdSessionCount' => $usage->ctdSessionCount ?? 0,
                    'daily_usage' => (string) $daily_usage,
                    'daily_sessions' => $daily_sessions,
                    'bill_amount' => (string) $bill_amount,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];

                $usages[] = $row;
            }
        });
//        DB::enableQueryLog();
        Usage::insert($usages);
//        dd(DB::getQueryLog());
        dd('Usage Successfully inserted!');
    }
}
