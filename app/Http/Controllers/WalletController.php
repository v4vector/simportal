<?php

namespace App\Http\Controllers;

use App\Models\Wallet;
use App\Http\Requests\StoreWalletAmount;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class WalletController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|client');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('wallet.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wallet = new Wallet;
        $clients = User::role('client')->get();
        return view('wallet.create', compact('wallet', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWalletAmount $request)
    {
        $data = $request->except(['_token', 'client']);
        $data['client_id'] = $request->client;
        $data['user_id'] = Auth::user()->id;

        $wl = new Wallet($data);
        $wl->save();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function show(Wallet $wallet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function edit(Wallet $wallet)
    {
        $wallet = Wallet::findOrFail($wallet)->first();
        $clients = User::role('client')->get();
        return view('wallet.edit', compact('wallet', 'clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function update(StoreWalletAmount $request, Wallet $wallet)
    {
        $data = $request->except('_token', '_method', 'client');
        $data['client_id'] = $request->client;

        $wallet->update($data);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Wallet  $wallet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wallet $wallet)
    {
        //
    }
}
