<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SaleAgentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin|superuser');

        view()->share('url', 'saleagent');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = User::role('saleagent')->get();

        return view('saleagent.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new User();

        return view('saleagent.create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required|string|max:50',
            'last_name' => 'required|string|max:50',
            'email' => 'required|email|unique:users,email',
            'mobile' => 'required|numeric',
            'password' => 'required|alpha_num|max:20',
        ]);

        $user = new User($request->except('_token'));
        $user->password = Hash::make($request->password);
        $user->save();

        $user->assignRole('saleagent');

        return redirect()->back()->with('success', 'Sale Agent Created Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = User::role('saleagent')
            ->where('id', $id)
            ->firstOrFail();

        $clients = User::role('client')->where('user_id', $id)->get();

        return view('saleagent.show', compact('model', 'clients'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = User::role('saleagent')->where('id', $id)->firstOrFail();

        return view('saleagent.edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name'    => 'required|string|max:50',
            'last_name'     => 'required|string|max:50',
            'email'         => 'required|email|unique:users,email,'. $id,
            'mobile'        => 'required|numeric',
            'password'      => 'nullable|string|max:20'
        ]);

        $data = $request->except('_token', '_method', 'password');

        if($request->password) {
            $data['password'] = Hash::make($request->password);
        }

        $agent = User::findOrFail($id);
        $agent->update($data);

        return redirect()->back()->with('success', 'Sale Agent Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        return redirect()->back()->with('success', 'Sale Agent Deleted Successfully!');
    }
}
