<?php

namespace App\Http\Controllers;

use function foo\func;
use Illuminate\Http\Request;
use App\Models\Jasper;
use App\Models\Connection;
use App\Models\Device;
use App\Models\Usage;
use App\Models\UsageReport;
use Carbon\Carbon;
use App\role;
use DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $me = Auth::user();
        if($me->hasRole('saleagent')){
            return view('dashboards.sale_agent');
        } /*else if ($me->hasRole('client')){
            return view('dashboards.client');
        }*/ else {
            return view('dashboards.all');

        }

    }
//     public function account_detial($id){

// // $jasper = Connection::where('user_id' , $id)->get();
// // foreach($jasper as $val){
// // $val->msisn;
// // }


// $obj= this->UsageReport->day_wise($id);
// dd($obj);

// }
    
//
    // public function role()
    // {
    //     $role= role::where('id','1')->get();
    //      print($role);
    // }
}
