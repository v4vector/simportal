<?php

namespace App\Http\Controllers;

use App\Http\Middleware\Admin;
use Illuminate\Http\Request;
use App\Models\AdminNotification;
use Illuminate\Support\Facades\Auth;
use App\Models\SaleagentNotification;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin')->only('admin', 'admin_read', 'admin_readall');
        $this->middleware('role:saleagent')->only('saleagent', 'saleagent_read', 'saleagent_readall');
    }


    public function admin()
    {
        $notes = AdminNotification::orderBy('id', 'desc')->get();
        return response()->json($notes);
    }

    public function admin_read($id)
    {
        $noti = AdminNotification::findOrFail($id);
        $noti->status = 'read';
        $noti->save();
        return response()->json($noti);
    }

    public function admin_readall()
    {
        $noti = AdminNotification::where('status', 'unread')->update(['status'=> 'read']);

        return response()->json($noti);
    }

    public function saleagent()
    {
        $notes = SaleagentNotification::where('user_id', Auth::user()->id)->get();
        return response()->json($notes);
    }

    public function saleagent_read($id)
    {
        $noti = SaleagentNotification::where('user_id', Auth::user()->id)->findOrFail($id);
        $noti->status = 'read';
        $noti->save();
        return response()->json($noti);
    }

    public function saleagent_readall()
    {
        $noti = SaleagentNotification::where('user_id', Auth::user()->id)->where('status', 'unread')->update(['status'=> 'read']);

        return response()->json($noti);
    }
}
