<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Requests\StoreDeal;
use Illuminate\Http\Request;
use App\Models\Installment;
use App\Models\ProductType;
use App\Models\DataVolume;
use App\Models\Network;
use App\Models\Quote;
use App\Models\Deal;
use Carbon\Carbon;
use App\Models\AdminNotification;
use App\Models\SaleagentNotification;
use App\Models\User;

class QuoteController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin|saleagent')->only('show', 'index');
        $this->middleware('role:admin|saleagent')->only('send', 'get_volumn', 'get_quote_price', 'get_datavolumn');
        $this->middleware('role:admin')->only('approve');
//        $this->middleware('role:saleagent');
    }
public function index()
    {
        $user = Auth::user();
        $quotes = Quote::
        when($user->hasRole('client'), function ($q) use ($user) {
            return $q->where('client_id', $user->id);
        })
            ->when($user->hasRole('saleagent'), function ($q) use ($user) {
                return $q->where('user_id', $user->id);
            })
            ->get();
        return view('quote.index', compact('quotes'));
    }
        public function get_datavolume($deal_type, $type)
    {
        return response()->json(DataVolume::where('deal_type', $deal_type)->where('type', $type)->get());
    }

    public function get_quote_price($deal_type, $product_type_id, $data_usage)
    {
        $type = 0;
        if ($data_usage == 'high_data') {
            $type = ProductType::where('data_type', $deal_type)->where('id', $product_type_id)->firstOrFail()->hd_rrp;
        } else {
            $type = ProductType::where('data_type', $deal_type)->where('id', $product_type_id)->firstOrFail()->ld_rrp;
        }
        return response()->json($type);
    }

    public function get_volume($deal_type, $id)
    {
        $vol = DataVolume::where('deal_type', $deal_type)->findOrFail($id);
        return response()->json($vol);
    }
    
}