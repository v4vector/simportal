<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Connection;
use Illuminate\Http\Request;

class AssignSimController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|superuser']);
    }

    public function index()
    {
        $connections = Connection::with('user')->get();

        return view('assign.index', ['connections' => $connections]);
    }

    public function create()
    {
        $iccids = Connection::where('user_id', null)->get();
        $clients = User::role('client')->get();
        return view('assign.create', ['iccids' => $iccids, 'clients' => $clients]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'iccid' => 'required|array|min:1',
            'client' => 'required|min:1|integer'
        ]);

        $user = User::findOrFail($request->client);
        if(!$user->hasRole('client')){
            abort(403);
        }

        foreach($request->iccid as $conn):
            $connection = Connection::findOrFail($conn);
            $connection->user_id = $request->client;
            $connection->save();
        endforeach;
        return back()->with('success', 'Sim Assign to Client Successfully!');
    }

    public function edit($id)
    {
        $client = User::findOrFail($id);
        if(!$client->hasRole('client')){
            abort(403);
        }

        $iccids = Connection::where('user_id', $client->id)->get();
        return view('assign.edit', ['client' => $client, 'iccids' => $iccids]);
    }

    public function update(Request $request, $id)
    {
        $client = User::findOrFail($id);
        if(!$client->hasRole('client')){
            abort(403);
        }
        if(!$request->iccid){
            $request->iccid = array();
        }
        $iccids = Connection::where('user_id', $client->id)->get();
        foreach($iccids as $iccid):
            if(!in_array($iccid->id, $request->iccid)){
                $iccid->user_id = null;
                $iccid->save();
            }
        endforeach;

        return redirect()->back()->with('success', 'ICCID Updated Successfully!');
    }

    public function deassign($id)
    {
        $connection = Connection::findOrFail($id);
        $connection->user_id = null;
        $connection->save();
        return back()->with('success', 'Sim Deassign Successfully!');
    }
}
