<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Usage;
use App\Models\Company;
use App\Models\UsageReport;
use Illuminate\Http\Request;
use App\Http\Requests\StoreClient;
use App\Http\Requests\UpdateClient;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ClientController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('role:salesagent|admin|superuser')->only(['edit', 'update', 'destroy', 'create', 'store']);
        view()->share('url', 'client');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->hasRole('client')){
            abort(403);
        }
        $users = User::with('connections', 'devices')->role('client')->when(Auth::user()->hasRole('saleagent'), function ($q){
            return $q->where('user_id', Auth::user()->id);
        })->get();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User();
        return view('users.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClient $request)
    {
        dd($request->all());
        $user = new User($request->only('first_name', 'last_name', 'mobile', 'email', 'password','account_phone' , 'account_email'));
        $user->password = Hash::make($user->password);
        $user->user_id = Auth::user()->id;
        $user->email_verified_at = Carbon::now();
        $user->save();
        $user->assignRole('client');

        $company = new Company($request->only('name', 'address', 'postcode', 'company_number', 'taxID', 'notes'));

        $user->company()->save($company);

        if($request->ajax()){
            return response()->json(['msg' => 'Client Created Successfully']);
        } else {
            return redirect()->route('client.index')->with('success', 'Client Created Successfully!');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->hasRole('client')){
            abort(403);
        }
        $user = User::with('client_quotes', 'connections.usage', 'client_quotes')
            ->when(Auth::user()->hasRole('saleagent'), function ($q){
                return $q->where('user_id', Auth::user()->id);
            })
            ->role('client')
            ->where('id', $id)
            ->firstOrFail();

        $usage_report = new UsageReport;
        $all = $usage_report->one_client($id);

        return view('client.show', compact('user', 'all'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit', ['user' => $user]);
        $start = Carbon::now()->subMonth()->startOfMonth();
        $end = Carbon::now()->endOfMonth();

        $usage_report = new UsageReport;
        $all = $usage_report->search_all($start, $end);
        dd($all);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClient $request, $id)
    {
        $user = User::findOrFail($id);
        if(!$user->hasRole('client')){
            abort(403);
        }

        if($request->has('password') && $request->password != null){
            $user->password = bcrypt($request->password);
        }

        $user->company->name = $request->name;
        $user->company->address = $request->address;
        $user->company->postcode = $request->postcode;
        $user->company->company_number = $request->company_number;
        $user->company->taxID = $request->taxID;
        $user->company->notes = $request->notes;

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->account_phone = $request->account_phone;
        $user->account_email = $request->account_email;


        $user->company->save();
        $user->save();

        return redirect()->route('client.index')->with('success', 'Client Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = User::findOrFail($id);
        if(!$user->hasRole('client')){
            abort(403);
        }
        $company = $user->company;
        $connections = $user->connections;
        $wallets = $user->wallet;
        $clientQuotes = $user->client_quotes;
        $salesagentQuotes = $user->saleagent_quotes;
        $usage = $user->usage;
        // $company->delete();
        // foreach($connections as $connection){
        //     $connection->delete();
        // }
        // foreach($wallets as $wallet){
        //     $wallet->delete();
        // }
        // foreach($clientQuotes as $clientQuote){
        //     $clientQuote->delete();
        // }
        // foreach($salesagentQuotes as $salesagentQuote){
        //     $salesagentQuote->delete();
        // }
        // foreach($usage as $u){
        //     $u->delete();
        // }
        $user->delete();
        return back()->with('success', 'User Deleted Successfully!');
    }

    public function get_usage($client_id)
    {
        $user = User::with('client_quotes', 'connections.usage', 'client_quotes')
            ->when(Auth::user()->hasRole('saleagent','admin'), function ($q){
                return $q->where('user_id', Auth::user()->id);
            })
            ->role('client')
            ->where('id', $client_id)
            ->firstOrFail();

        $usage_report = new UsageReport;
        $all = $usage_report->one_client($client_id);
        dd($all);
    }
}
