<?php

namespace App\Http\Controllers;

use App\Models\Quote;
use function foo\func;
use Illuminate\Support\Facades\Auth;
use App\Models\Device;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\JasperApi;
use App\Models\Connection;
use App\Models\User;
use App\Models\Usage;

use Illuminate\Support\Facades\DB;

class ConnectionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superuser|admin|client');
        $this->middleware('role:admin|superuser')->only('deal_info','deal_info_bulk', 'store_deal_info_bulk', 'get_usage', 'edit');
        view()->share('url', 'connections');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//dd(Carbon::now());
        $devices = Device::
        when(Auth::user()->hasRole('client'), function ($query) {
            return $query->where('user_id', Auth::user()->id);
        })->get();
//dd($devices);
        $connections = Connection::with(['usage' => function ($q) {
            return $q->whereDate('created_at', '>=', Carbon::now()->startOfMonth())->where('created_at', '<=', Carbon::now()->endOfMonth());

        }])
            ->when(Auth::user()->hasRole('client'), function ($query) {
                return $query->where('user_id', Auth::user()->id);
            })
            /*->whereHas('usage', function ($q){
                return $q->whereDate('created_at', '>=', Carbon::now()->startOfMonth())->where('created_at', '<=', Carbon::now()->endOfMonth());
            })*/
            ->get()
            ->sortByDesc(function ($con) {
                return $con->usage->sum('daily_usage');
            });
//dd( $connections);
        return view('connection.connections', compact('connections', 'devices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        ini_set('max_execution_time', 300);
        $jasper = new JasperApi;
        $devices = $jasper->all_devices();

//        $single_details = $jasper->one_device_details('8931088617085028680');
//        dd($single_details);
//        $usage = $jasper->one_usage_details('8931088617085028680');
//        $usage = $jasper->time_usage('8931088617085028680');
//        dd($usage);

        $details = $jasper->details($devices);
        foreach ($details as $device) {
            $device = (array)$device;
            $row = [];

            $email = $device['customerCustom2'];
            $user = User::where('email', $email)->first();
            if ($user) {
                $row['user_id'] = $user->id;
                if ($device['dateActivated']) {
                    $row['dateActivated'] = Carbon::parse($device['dateActivated'])->format('Y-m-d H:i:s');
                }
                if ($device['dateAdded']) {
                    $row['dateAdded'] = Carbon::parse($device['dateAdded'])->format('Y-m-d H:i:s');
                }
                if ($device['dateUpdated']) {
                    $row['dateUpdated'] = Carbon::parse($device['dateUpdated'])->format('Y-m-d H:i:s');
                }
                if ($device['dateShipped']) {
                    $row['dateShipped'] = Carbon::parse($device['dateShipped'])->format('Y-m-d H:i:s');
                }
                $row['iccid'] = $device['iccid'];
                $row['imsi'] = $device['imsi'];
                $row['msisdn'] = $device['msisdn'];
                $row['imei'] = $device['imei'];
                $row['status'] = $device['status'];
                $row['ratePlan'] = $device['ratePlan'];
                $row['communicationPlan'] = $device['communicationPlan'];
                $row['accountId'] = $device['accountId'];
                $row['operatorCustom1'] = $device['operatorCustom1'];
                $row['operatorCustom2'] = $device['operatorCustom2'];
                $row['accountCustom1'] = $device['accountCustom1'];
                $row['accountCustom2'] = $device['accountCustom2'];
                $row['customerCustom1'] = $device['customerCustom1'];
                $row['customerCustom2'] = $device['customerCustom2'];
                $row['simNotes'] = $device['simNotes'];
                $row['euiccid'] = $device['euiccid'];
                $row['deviceID'] = $device['deviceID'];
                $row['modemID'] = $device['modemID'];
                $row['globalSimType'] = $device['globalSimType'];

//                dd($row);


                $connection = Connection::where('iccid', $device['iccid'])->first();
                if (!empty($connection->id)) {
                    $connection->update($row);
                } else {
                    $connection = new Connection($row);
                    $connection->save();
                }
            }
        }
    }

    public function get_usage()
    {
        ini_set('max_execution_time', 300);
        $already_usages = Usage::with('connection')->whereDate('created_at', date("Y-m-d", strtotime('-1 days')))->get();
        dd($already_usages);

        Connection::chunk(3, function ($five_chunk) use ($already_usages) {
            sleep(3);

            $jasper = new JasperApi;
            $results = $jasper->usages_sessions($five_chunk);

            $usages = [];
            foreach ($results as $usage) {
                $usage = \GuzzleHttp\json_decode($usage['value']->getBody());
                $daily_usage = $usage->ctdDataUsage ?? 0;
                $daily_sessions = $usage->ctdSessionCount ?? 0;

                echo $usage->iccid;

//                If there yesterday usages is available and date is not 01
                if (!$already_usages->isEmpty() && date('d') != '01') {
                    $check_yesterday = $already_usages->filter(function ($q) use ($usage) {
                        return $q->connection->iccid == $usage->iccid;
                    })->isEmpty();

                    if (!$check_yesterday) {
                        $yesterday = $already_usages->filter(function ($q) use ($usage) {
                            return $q->connection->iccid == $usage->iccid;
                        })->first();
                        dd($yesterday);
                        $daily_usage = $usage->ctdDataUsage - $yesterday->ctdDataUsage;
                        $daily_sessions = $usage->ctdSessionCount - $yesterday->daily_sessions;
                    }
                }
//                dd('end');

                $usage = [
                    'connection_id' => $five_chunk->where('iccid', $usage->iccid)->first()->id,
                    'ctdDataUsage' => $usage->ctdDataUsage ?? 0,
                    'ctdSessionCount' => $usage->ctdSessionCount ?? 0,
                    'daily_usage' => $daily_usage,
                    'daily_sessions' => $daily_sessions,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
                $usages[] = $usage;
            }
            Usage::insert($usages);
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($iccid)
    {
        $jasper = new JasperApi;
        $session = $jasper->get_session_details($iccid);
        $con = Connection::where('iccid', $iccid)->firstOrFail();

        $day_wise = Usage::select(
            DB::raw('sum(daily_usage) as daily_usage'),
            DB::raw('sum(daily_sessions) as daily_sessions'),
            DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as days")
        )
            ->when(Auth::user()->hasRole('saleagent'), function ($q){
                $q->connection->where('user_id', Auth::user()->id);
            })
            ->whereHas('connection', function ($q) use ($iccid) {
                $q->where('iccid', $iccid);
            })
            ->where('created_at', '>=', Carbon::now()->subDays(15))
            ->groupBy('days')
            ->get()->map(function ($day) {
                $day->daily_usage = round($day->daily_usage / 1048576, 2);
                return $day;
            });

        $month_wise = Usage::select(
            DB::raw('sum(daily_usage) as daily_usage'),
            DB::raw('sum(daily_sessions) as daily_sessions'),
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months")
        )
            ->when(Auth::user()->hasRole('saleagent'), function ($q){
                $q->connection->where('user_id', Auth::user()->id);
            })
            ->whereHas('connection', function ($q) use ($iccid) {
                $q->where('iccid', $iccid);
            })
            ->where('created_at', '>=', Carbon::now()->subYear())
            ->groupBy('months')
            ->get()->map(function ($month) {
                $month->daily_usage = round($month->daily_usage / 1048576, 2);
                return $month;
            });

        return view('connection.details', compact('con', 'day_wise', 'month_wise', 'session'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $con = Connection::findOrFail($id);
        return view('connection.edit', compact('con'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $iccid)
    {
        $status = '';
        if ($request->status == 'ACTIVATED') {
            $status = 'DEACTIVATED';
        } else {
            $status = 'ACTIVATED';
        }
        $jasper = new JasperApi;
        $result = $jasper->change_status($iccid, $status);
        if ($result->getStatusCode() != 200) {
            dd($result);
        } else {
            return (object)json_decode($result->getBody(), true);
        }
    }

    public function deal_info(Request $request, $id)
    {
        $this->validate($request, [
            'deal_base_price' => 'required|numeric|min:0',
            'deal_admin_margin' => 'required|numeric|min:0',
            'deal_sales_margin' => 'required|numeric|min:0',
            'deal_client_price' => 'required|numeric|min:0',
        ]);
        $data = $request->except('_token', '_method');
        $data['deal_info_status'] = 1;

        $con = Connection::findOrFail($id);
        $con->update($data);

        return redirect()->route('connections.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function days_search($iccid, $start_date, $end_date)
    {
        $usg = Usage::wherehas('connection', function ($q) use ($iccid) {
            return $q->where('iccid', $iccid);
        })
            ->where('created_at', '>=', Carbon::parse($start_date)->startOfDay())
            ->where('created_at', '<=', Carbon::parse($end_date)->endOfDay())
            ->get();

        $group_days = $usg->groupBy(function ($row) {
            return $row->created_at->format('Y-m-d');
        });

        if (!$group_days) {
            return response('Nothing Found!', 200)
                ->header('Content-Type', 'text/plain');
        } else {
            $days_usg = $month_usg = [];
            foreach ($group_days as $key => $day) {
                $days_usg[Carbon::parse($key)->format('d M Y')] = $day->sum('daily_usage');
            }
            return response()->json([$days_usg]);
        }
    }

    public function month_search($iccid, $start_date, $end_date)
    {
        $usg = Usage::wherehas('connection', function ($q) use ($iccid) {
            return $q->where('iccid', $iccid);
        })
            ->where('created_at', '>=', Carbon::parse($start_date)->startOfDay())
            ->where('created_at', '<=', Carbon::parse($end_date)->endOfDay())
            ->get();

        $group_months = $usg->groupBy(function ($row) {
            return $row->created_at->format('Y-m');
        });

        if (!$group_months) {
            return response('Nothing Found!', 200)
                ->header('Content-Type', 'text/plain');
        } else {
            $month_usg = [];
            foreach ($group_months as $key => $month) {
                $month_usg[Carbon::parse($key)->format('M Y')] = $month->sum('daily_usage');
            }
            return response()->json([$month_usg]);
        }
    }

    public function attach_deal($con_id)
    {
        $con = Connection::with('client')->where('id', $con_id)->firstOrFail();
        $client = $con->client;
        return view('connection.add-bulk-info', compact('connections'));
    }

    public function client_deals($client_id)
    {
        $quote = Quote::with('deals')->where('client_id', $client_id)->firstOrFail();
        $deals = $quote->deals;
        return response()->json($deals);
    }

    public function store_deal_info_bulk(Request $request)
    {
        $this->validate($request, [
            'deal_base_price' => 'required|numeric|min:0',
            'deal_admin_margin' => 'required|numeric|min:0',
            'deal_sales_margin' => 'required|numeric|min:0',
            'deal_client_price' => 'required|numeric|min:0',
            'connections' => 'required|array|min:1',
        ]);
        $data = $request->except('_token', '_method', 'connections');
        $data['deal_info_status'] = 1;

        $connections = $request->connections;
        foreach ($connections as $con){
            $c = Connection::findOrFail($con);
            $c->update($data);
        }
        return redirect()->back();
    }
}
