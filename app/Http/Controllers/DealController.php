<?php

namespace App\Http\Controllers;

use App\Http\Requests\DealConnectionAttach;
use App\Http\Requests\StorePooledDeal;
use App\Models\Installment;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreDeal;
use Illuminate\Http\Request;
use App\Models\Connection;
use App\Models\Quote;
use App\Models\Deal;
use Carbon\Carbon;

class DealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($quote_id)
    {
        $user = Auth::user();
        $quote = Quote::with('deals')->
        where('id', $quote_id)
            ->when($user->hasAnyRole('saleagent'), function ($q) use ($user) {
                return $q->where('user_id', $user->id);
            })
            ->firstOrFail();

        return view('deal.individual', compact('quote'));
    }

    public function pooled($quote_id = null)
    {
        if ($quote_id) {
            $quote = Quote::findOrFail($quote_id);
        } else {
            $quote = new Quote();
        }

        return view('deal.pooled', compact('quote'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDeal $request, $quote_id)
    {
        $quote = Quote::findOrFail($quote_id);
        $data = $request->except('_token');
        $data['network_preferences'] = implode(',', $request->network_preferences);
        $data['user_id'] = Auth::user()->id;
        $deal = new Deal($data);

        $deal->cost_sims = $cost_sims = $request->sim_volume * $request->sim_type;
        $deal->vat_sims = $vat_sims = (20/100) * $cost_sims;
        $deal->total_sims_cost = $cost_sims + $vat_sims;

        $deal->total_commission = (($request->margin_admin + $request->margin_sales) / 2) * $request->sim_volume * $request->contract_len;
        $deal->deal_amount = $deal_amount = $request->sim_volume * $request->client_price * $request->contract_len;
        $deal->vat = $vat = (20/100) * $deal_amount;
        $deal->total_deal = $total_deal = $deal_amount + $vat;

        if($request->contract_len > 3) {
            $deal->payment_terms = 'monthly';
            $deal->installment = $monthly = $total_deal / $request->contract_len;
            $deal->advance_payment = $monthly * 3;
        } else {
            $deal->payment_terms = 'upfront';
            $deal->advance_payment = $total_deal;
        }
        $quote->deals()->save($deal);

        return redirect()->route('deal.individual', [$quote]);
    }

    public function store_pooled(StorePooledDeal $request, $quote_id)
    {
        $quote = Quote::findOrFail($quote_id);

        $data = $request->except('_token');
        $data['network_preferences'] = implode(',', $request->network_preferences);
        $data['user_id'] = Auth::user()->id;
        $deal = new Deal($data);

        $deal->cost_sims = $cost_sims = ($request->sim_volume * 0.50) + (($request->sim_volume * 2.50) * 12);
        $deal->vat_sims = $vat_sims = $cost_sims * 0.20;
        $deal->total_sims_cost = $cost_sims + $vat_sims;

        $deal->total_commission = (($request->client_price - $request->base_price) / 2) * $request->sim_volume * $request->contract_len;
        $deal->deal_amount = $deal_amount = $request->client_price;
        $deal->vat = $vat = (20 / 100) * $deal_amount;
        $deal->total_deal = $total_deal = $deal_amount + $vat;

        $deal->payment_terms = 'upfront';
        $deal->advance_payment = $total_deal;

        $quote->deals()->save($deal);

        return redirect()->route('deal.pooled', [$quote]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($quote_id, $deal_id)
    {

        $user = Auth::user();
        $quote = Quote::with('deals')->
        where('id', $quote_id)
            ->when($user->hasAnyRole('saleagent'), function ($q) use ($user) {
                return $q->where('user_id', $user->id);
            })
            ->firstOrFail();


        $deal = Deal::
        where('id', $deal_id)
            ->when($user->hasRole('saleagent'), function ($q) use ($user) {
                return $q->where('user_id', $user->id);
            })
            ->firstOrFail();

        return view('deal.edit-deal', compact('quote', 'deal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDeal $request, $quote_id, $deal_id)
    {
        $deal = Deal::findOrFail($deal_id);


        $data = $request->except('_token' , '_method');
        $data['network_preferences'] = implode(',', $request->network_preferences);

        $deal->cost_sims = $cost_sims = $request->sim_volume * $request->sim_type;
        $deal->vat_sims = $vat_sims = (20/100) * $cost_sims;
        $deal->total_sims_cost = $cost_sims + $vat_sims;

        $deal->total_commission = (($request->margin_admin + $request->margin_sales) / 2) * $request->sim_volume * $request->contract_len;
        $deal->deal_amount = $deal_amount = $request->sim_volume * $request->client_price * $request->contract_len;
        $deal->vat = $vat = (20/100) * $deal_amount;
        $deal->total_deal = $total_deal = $deal_amount + $vat;

        if($request->contract_len > 3) {
            $deal->installment = $monthly = $total_deal / $request->contract_len;
            $deal->advance_payment = $monthly * 3;
        } else {
            $deal->advance_payment = $total_deal;
        }

        $deal->save();
        return redirect()->back();
        // dd($deal);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function attach_connections($quote_id)
    {
        $quote = Quote::with('deals')->where('status', 'Approved')->findOrFail($quote_id);
        $connections = Connection::where('deal_id', NULL)->where('user_id', $quote->client_id)->get();

        return view('deal.put-connections', compact('quote', 'connections'));
    }

    public function store_attach_connections(DealConnectionAttach $request, $quote_id)
    {
        $deal = Deal::findOrFail($request->deal);
        $check = count($request->connections);
        $already = Connection::where('deal_id', $request->deal)->count();
        $check += $already;
        $max = $deal->sim_volumn - $already;

        $this->validate($request, [
            'connections' => 'max:'.$max,
        ], [
            'connections.max' => 'This deal cannot have more than '.$deal->sim_volumn.' connections. '.$already.' already added.'
        ]);

        $cons = Connection::whereIn('id', $request->connections)->update(['deal_id' => $deal->id]);
        return redirect()->back();
    }
}
