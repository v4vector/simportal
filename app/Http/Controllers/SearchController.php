<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UsageReport;
use Carbon\Carbon;

class SearchController extends Controller
{
    public function index($start = null, $end = null)
    {
        dd('all');
        $usage_report = new UsageReport;
        $all = $usage_report->search_all($start, $end);

        return response()->json($all);
    }

    public function search_day($start= null, $end = null)
    {
        $usage_report = new UsageReport;
        $all = $usage_report->day_wise_search($start, $end);

        return response()->json($all);
    }

    public function search_month($start= null, $end = null)
    {
        $usage_report = new UsageReport;
        $all = $usage_report->month_wise_search($start, $end);

        return response()->json($all);
    }

    public function search_top_metrics($start= null, $end = null)
    {
        $usage_report = new UsageReport;
        $all = $usage_report->top_metric_search($start, $end);

        return response()->json($all);
    }

    public function search_connections()
    {
        $usage_report = new UsageReport;
        $all = $usage_report->search_connections();

        return response()->json($all);
    }

    public function default_dates()
    {
        $usage_report = new UsageReport;
        $all = $usage_report->default_dates();

        return response()->json($all);
    }
}
