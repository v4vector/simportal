<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Requests\StoreDeal;
use Illuminate\Http\Request;
use App\Models\Installment;
use App\Models\ProductType;
use App\Models\DataVolume;
use App\Models\Network;
use App\Models\Quote;
use App\Models\Deal;
use Carbon\Carbon;
use App\Models\AdminNotification;
use App\Models\SaleagentNotification;
use App\Models\User;

class QuoteController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin|saleagent|superuser')->only('show', 'index');
        $this->middleware('role:admin|saleagent|superuser')->only('send', 'get_volumn', 'get_quote_price', 'get_datavolumn');
        $this->middleware('role:admin|superuser')->only('approve');
//        $this->middleware('role:saleagent');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $quotes = Quote::
        when($user->hasRole('client'), function ($q) use ($user) {
            return $q->where('client_id', $user->id);
        })
            ->when($user->hasRole('saleagent'), function ($q) use ($user) {
                return $q->where('user_id', $user->id);
            })
            ->get();
        return view('quote.index', compact('quotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quote = new Quote;
        return view('quote.create', compact('quote'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'client_id' => 'required|integer|min:1',
            'order_number' => 'nullable|string'
        ]);
        $quote_data = [
            'client_id' => $request->client_id,
            'order_number' => $request->order_number,
            'user_id' => Auth::user()->id
        ];
        $quote = new Quote($quote_data);
        $quote->save();

        if($request->deal_type == 'Pooled'){
            return redirect()->route('deal.pooled', [$quote]);
        } else {
            return redirect()->route('deal.individual', [$quote]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $quote = Quote::with('deals')->
        where('id', $id)
            ->when($user->hasAnyRole('saleagent'), function ($q) use ($user) {
                return $q->where('user_id', $user->id);
            })
            ->when($user->hasAnyRole('client'), function ($q) use ($user) {
                return $q->where('client_id', $user->id);
            })
            ->firstOrFail();

        return view('quote.printable', compact('quote'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_datavolume($deal_type, $type)
    {
        return response()->json(DataVolume::where('deal_type', $deal_type)->where('type', $type)->get());
    }

    public function get_quote_price($deal_type, $product_type_id, $data_usage)
    {
        $type = 0;
        if ($data_usage == 'high_data') {
            $type = ProductType::where('data_type', $deal_type)->where('id', $product_type_id)->firstOrFail()->hd_rrp;
        } else {
            $type = ProductType::where('data_type', $deal_type)->where('id', $product_type_id)->firstOrFail()->ld_rrp;
        }
        return response()->json($type);
    }

    public function get_volume($deal_type, $id)
    {
        $vol = DataVolume::where('deal_type', $deal_type)->findOrFail($id);
        return response()->json($vol);
    }

    public function send($quote_id)
    {
        set_time_limit(500);
        $quote = Quote::with('deals')->findOrFail($quote_id);
        $client = $quote->client;

        $pdf = PDF::loadView('quote.pdf', compact('quote'));
        Mail::send('quote.email', ['quote' => $quote], function ($mail) use ($client, $pdf) {
            $mail->from('quotebuddy@watertelecom.com', 'Water Telecom');

            $mail->to($client->email)->subject('Water Telecom Sims Quote');

            $mail->attachData($pdf->output(), 'Water PDF.pdf');
        });
        if (Mail::failures()) {
            // return response showing failed emails
        } else {
            $quote->status = 'Sent';
            $quote->save();
        }
        $alert = 'Quote Sent Sucessfully to ' . strtolower($client->email);

        return redirect()->route('quote.index')->with('success', $alert);
    }

    public function save_paymentplan(Request $request, $quote_id)
    {
        $quote = Quote::findOrFail($quote_id);
        if ($request->payment_plan == 'monthly') {
//            dd('save checked');
            $quote->payment_plan = 'monthly';
            $quote->save();
        }
        return redirect()->back();
    }

    public function approve($quote_id)
    {
        $user = Auth::user();
        $quote = Quote::with('deals')->
        where('id', $quote_id)
            ->when($user->hasAnyRole('saleagent'), function ($q) use ($user) {
                return $q->where('user_id', $user->id);
            })
            ->firstOrFail();


        /*foreach ($quote->deals as $deal) {
            if ($deal->payment_terms == 'monthly') {

                $installment = $deal->installment;
                $con_months = $deal->contract_len - 3;

                $date = Carbon::now()->addMonths(3)->startOfMonth();
                for ($i = 1; $i <= $con_months; $i++) {
                    $date = $date->addMonth();


                    $ins = new Installment;
                    $ins->date = $date;
                    $ins->amount = $installment;
                    $deal->installments()->save($ins);
                }
            }
        }*/

        $quote->status = 'Approved';
        $quote->approved_date = Carbon::now();
        $quote->save();

        $noti = new AdminNotification([
            'message' => 'A new Quote Approved',
            'link' => route('quote.show', $quote->id),
        ]);
        $noti->save();

        $noti = new SaleagentNotification([
            'message' => 'A new Quote Approved',
            'link' => route('quote.show', $quote->id),
            'user_id' => $quote->user->id
        ]);
        $noti->save();

        $admins = User::role('admin')->get();
        foreach($admins as $admin){
            Mail::raw('A new Quote Approved', function ($message) use ($admin) {
                $message->subject('New Quote');
                $message->to($admin->email);
            });
        }
        Mail::raw('A new Quote Approved', function ($message) use ($quote) {
            $message->subject('New Quote');
            $message->to($quote->user->email);
        });


        return redirect()->back();
    }
}
