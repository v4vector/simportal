<?php

namespace App\Http\Controllers;

use App\Notifications\Users\ActivateEmailNotification;
use function foo\func;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreClient;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UsageReport;
use Carbon\Carbon;
use DB;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superuser|admin|saleagent')->only('show_profile');
        view()->share('url', 'clients');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function edit_account()
    {
        $user = User::with('company')->findOrFail(Auth::user()->id);
        return view('users.edit')->with('user', $user);
    }

    public function update_account(Request $request)
    {
        $this->validate($request, [
            'first_name'    => 'required|string|max:190',
            'last_name'     => 'required|string|max:190',
            'email'         => 'required|email',
            'phone'         => 'required|numeric',
            'details'       => 'nullable|string|max:190',
        ]);
        $user = Auth::user();
        $user->update($request->all());
        return redirect()->back();
    }

    public function show_profile($user_id)
    {
        $logged = Auth::user();
        $user = User::
            when($logged->hasRole('saleagent'), function ($q){
                return $q->role('client');
            })
            ->findOrFail($user_id);
        return view('users.show', compact('user'));
    }


}
