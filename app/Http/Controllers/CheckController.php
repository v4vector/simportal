<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Models\Connection;
use App\Models\JasperApi;
use App\Models\Quote;
use App\Models\Usage;
use Carbon\Carbon;

class CheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$row = [
        'connection_id' => 8,
        'ctdDataUsage' => 4182252544,
        'ctdSessionCount' => 10,
        'daily_usage' => 4182252544,
        'daily_sessions' => 10
//                    'created_at' => Carbon::now(),
//                    'updated_at' => Carbon::now()
    ];
        $a = new Usage($row);
        $a->connection_id = '8';
        $a->ctdDataUsage = '4182252544';
        $a->ctdSessionCount = '10';
        $a->daily_usage = '4182252544';
        $a->daily_sessions = '10';

        $a->save();
        print_r($row);
        dd($a);*/

        ini_set('max_execution_time', 1000);
        $already_usages = Usage::
        whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->whereRaw('id IN (select MAX(id) FROM `usages` GROUP BY `connection_id`)')
            ->get();


        $usages = [];
        Connection::chunk(3, function ($five_chunk) use ($already_usages, &$usages) {
            sleep(3);

            $jasper = new JasperApi;
            $results = $jasper->usages_sessions($five_chunk);


            foreach ($results as $usage) {
                $usage = \GuzzleHttp\json_decode($usage['value']->getBody());
                $daily_usage = $usage->ctdDataUsage;
                dd($daily_usage);
                $daily_sessions = $usage->ctdSessionCount;


//              First day first hour first minute
                if (date('d') == '01' && date('H' == '00' && date('i' <= '30'))) {
                    //Do nothing
                } else {
                    $check_old = $already_usages->filter(function ($q) use ($usage) {
                        return $q->connection->iccid == $usage->iccid;
                    })->first();

                    if ($check_old) {
                        $daily_usage = $usage->ctdDataUsage - $check_old->ctdDataUsage;
                        $daily_sessions = $usage->ctdSessionCount - $check_old->ctdSessionCount;
                    }
                }

                $cons = Connection::get();


                $row = [
                    'connection_id' => $five_chunk->where('iccid', $usage->iccid)->first()->id,
                    'ctdDataUsage' => (string) $usage->ctdDataUsage,
                    'ctdSessionCount' => $usage->ctdSessionCount,
                    'daily_usage' => (string) $daily_usage,
                    'daily_sessions' => $daily_sessions,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];

                $usages[] = $row;
            }
        });
        DB::enableQueryLog();
        Usage::insert($usages);
        dd(DB::getQueryLog());
        dd($usages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        set_time_limit(500);
        $quote = Quote::with('deals')->findOrFail(1);

//        $pdf = PDF::loadView('quote.pdf', compact('quote'));
//        dd($pdf);
//        return $pdf->download("a.pdf");
        return view('quote.pdf', compact('quote'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
