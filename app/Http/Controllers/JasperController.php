<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use function foo\func;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Collection;

class JasperController extends Controller
{
    protected $base_uri = [];
    protected $authentication = [];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

        // $this->base_uri = [
        //     'base_uri' => 'https://restapi3.jasper.com/rws/api/v1/'
        // ];
        // $this->authentication = [
        //     'Authorization' => 'Basic '. base64_encode('broadcastsim:28f93b54-3c03-4295-81ec-187e57d8ed17')
        // ];
    }
   // public function test(){
   //      $test_temp = $device->get('https://restapi3.jasper.com/rws/api/v1/devices?modifiedSince=2018-04-18T17%3A31%3A34%2B00%3A00&pageSize=50&pageNumber=1');
   //      dd($test_temp);
   //  }
    public function test()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,"https://restapi3.jasper.com/rws/api/v1/devices/89462038005003097574/ctdUsages");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $username = 'broadcastsim';
        $api_key = '28f93b54-3c03-4295-81ec-187e57d8ed17';
        $headers = [
            "Accept: application/json",
            "Authorization: Basic " . base64_encode($username . ':' . $api_key)
        ];

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec ($curl);
        curl_close ($curl);

        dd($response);
    }

    public function connections()
    {
//      Get all Available Devices
        $available = $this->get_all_devices();

//        Get Details of individual Device
        $devices_details = $this->get_devices_details($available);

//        Get Usage of individual Device
        $usages = $this->get_device_usage($devices_details);
        $one = $devices_details;
//        dd($one);


        return view('connections', compact('info', 'devices_details', 'usages'));
    }

    public function device_details()
    {
        return view('device_details');
    }

    public function connection_details($iccid)
    {
        $device = $this->one_device_details($iccid);
        $usage_details = $this->one_usage_details($iccid);

        return view('connection_details', compact('device', 'usage_details'));
    }

    public function get_all_devices()
    {
        $current = Carbon::now()->startOfYear()->format('Y-m-dTH:i:sP');
        $current = urlencode(str_replace('UTC', 'T', $current));
        //        2018-01-01T00%3A00%3A00%2B00%3A00

        $client = new \GuzzleHttp\Client($this->base_uri);
        $response = $client->request('GET', 'devices?modifiedSince='.$current, [
            'headers' => $this->authentication
        ]);
        if ($response->getStatusCode() != 200){
            return false;
        } else {
            return Collection::make(json_decode($response->getBody(), true));
        }
    }

    public function get_devices_details($result)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);
        $promises = [];
        foreach ($result['devices'] as $device){
            $promises[$device['iccid']] = $client->getAsync('devices/'.$device['iccid'], ['headers' => $this->authentication]);
        }
        $results = \GuzzleHttp\Promise\unwrap($promises);
        $results = \GuzzleHttp\Promise\settle($promises)->wait();

        $devices_details = collect();
        foreach ($results as $result){
            $dev = json_decode($result['value']->getBody());
            $devices_details->push($dev);
        }
        return $devices_details;
    }

    public function get_device_usage($devices)
    {
        $client = new Client($this->base_uri);
        $promises = [];
        foreach ($devices as $device){
            $promises[$device->iccid] = $client->getAsync('devices/'.$device->iccid.'/ctdUsages', ['headers' => $this->authentication]);
        }
        $usage_results = \GuzzleHttp\Promise\unwrap($promises);
        $usage_results = \GuzzleHttp\Promise\settle($promises)->wait();
        $usages = collect();
        foreach ($usage_results as $usage){
            $usg = json_decode($usage['value']->getBody());
            $usages->push($usg);
        }
        return $usages;
    }

    public function one_device_details($iccid)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);
        $response = $client->request('GET', 'devices/'.$iccid, [
            'headers' => $this->authentication
        ]);
        if ($response->getStatusCode() != 200){
            return false;
        } else {
            return (object) json_decode($response->getBody(), true);
        }
    }

    public function one_usage_details($iccid)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);
        $response = $client->request('GET', 'devices/'.$iccid.'/ctdUsages', [
            'headers' => $this->authentication
        ]);
        if ($response->getStatusCode() != 200){
            return false;
        } else {
            return (object) json_decode($response->getBody(), true);
        }
    }
}
