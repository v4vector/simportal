<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreDevice;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Auth;
use App\Models\Connection;
use App\Models\JasperApi;
use App\Models\Device;
use App\Models\Usage;
use App\Models\User;
use Carbon\Carbon;
use DB;

class DeviceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:superuser|admin|client');
        $this->middleware('role:superuser|admin|client')->only('sync_devices', 'sync_devices_ajax');//added client
        view()->share('url', 'devices');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $devices = Device::all();
        return view('device.index', compact('devices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $device = new Device;
        return view('device.create', compact('device'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDevice $request)
    {
        $device = new Device($request->except('_token'));
        $device->slug = str_slug($request->name);
        if (Device::where('slug', $device->slug)->exists()) {
            $device->slug = $device->slug . '-' . time();
        }
        $device->user_id = Auth::user()->id;
        $device->save();
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $device = Device::with(['connections.usage' => function ($q) {
            return $q->where('created_at', '>=', Carbon::now()->subDays(15));
        }])
            ->where('slug', $slug)
            ->when(Auth::user()->hasRole('saleagent'), function ($q){
                $q->where('user_id', Auth::user()->id);
            })
            ->firstOrFail();

        $day_wise = Usage::select(
            DB::raw('sum(daily_usage) as daily_usage'),
            DB::raw('sum(daily_sessions) as daily_sessions'),
            DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as days")
        )
            ->when(Auth::user()->hasRole('saleagent'), function ($q){
                $q->connection->device->where('user_id', Auth::user()->id);
            })
            ->whereHas('connection.device', function ($q) use ($slug) {
                $q->where('slug', $slug);
            })
            ->where('created_at', '>=', Carbon::now()->subDays(15))
            ->groupBy('days')
            ->get()->map(function ($day) {
                $day->daily_usage = round($day->daily_usage / 1048576, 2);
                return $day;
            });

        $month_wise = Usage::select(
            DB::raw('sum(daily_usage) as daily_usage'),
            DB::raw('sum(daily_sessions) as daily_sessions'),
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months")
        )
            ->when(Auth::user()->hasRole('saleagent'), function ($q){
                $q->connection->device->where('user_id', Auth::user()->id);
            })
            ->whereHas('connection.device', function ($q) use ($slug) {
                $q->where('slug', $slug);
            })
            ->where('created_at', '>=', Carbon::now()->subYear())
            ->groupBy('months')
            ->get()->map(function ($month) {
                $month->daily_usage = round($month->daily_usage / 1048576, 2);
                return $month;
            });

            //dd($month_wise);

        return view('device.details', compact('device', 'month_wise', 'day_wise'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $device = Device::where('slug', $slug)->firstOrFail();
        return view('device.edit', compact('device'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDevice $request, $id)
    {
        $device = Device::findOrFail($id);

        $data = $request->except('_token', '_method');
        $data['slug'] = str_slug($request->name);
        if (Device::where('slug', $device->slug)->where('id', '!=', $id)->exists()) {
            $data['slug'] = $data['slug'] . '-' . time();
        }
        $device->update($data);
        return redirect('devices/' . $data['slug'] . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $device = Device::findOrFail($id);
        $device->delete();
        return redirect('devices');
    }

    public function connections($slug)
    {
        $device = Device::where('slug', $slug)
            ->when(Auth::user()->hasRole('saleagent'), function ($q){
                $q->where('user_id', Auth::user()->id);
            })
            ->firstOrFail();

        $assined = Connection::where('device_id', $device->id)->get();
        $unassined = Connection::where('device_id', null)
            ->when(Auth::user()->hasRole('saleagent'), function($q){
                $q->where('user_id', Auth::user()->id);
            })
            ->get();
        return view('device.add-connection', compact('device', 'unassined', 'assined'));
    }

    public function store_connection(Request $request, $id)
    {
        $unassigned = $request->unassigned;
        if ($request->unassigned) {
            foreach ($unassigned as $asgn) {
                Connection::where('id', $asgn)->update(['device_id' => $id]);
            }
        }
        return redirect()->back();
    }

    public function remove_connection(Request $request, $id)
    {
        $assigned = $request->assigned;
        if ($assigned) {
            foreach ($assigned as $asgn) {
                Connection::where('id', $asgn)->update(['device_id' => null]);
            }
        }
        return redirect()->back();
    }

    public function sync_devices()
    {
        return view('device.sync');
    }

    public function sync_devices_ajax()
    {
        ini_set('max_execution_time', 300);
        $jasper = new JasperApi;
        $devices = $jasper->all_devices();

        $chunks = array_chunk($devices, 4);

        foreach ($chunks as $chunk) {
            sleep(2);
            $details = $jasper->chunk_details($chunk);

            foreach ($details as $device) {
                $device = (array)$device;
                $row = [];
                $email = $device['customerCustom2'];
                $user = User::where('email', $email)->first();

                if ($user) {
                    $row['user_id'] = $user->id;
                    if ($device['dateActivated']) {
                        $row['dateActivated'] = Carbon::parse($device['dateActivated'])->format('Y-m-d H:i:s');
                    }
                    if ($device['dateAdded']) {
                        $row['dateAdded'] = Carbon::parse($device['dateAdded'])->format('Y-m-d H:i:s');
                    }
                    if ($device['dateUpdated']) {
                        $row['dateUpdated'] = Carbon::parse($device['dateUpdated'])->format('Y-m-d H:i:s');
                    }
                    if ($device['dateShipped']) {
                        $row['dateShipped'] = Carbon::parse($device['dateShipped'])->format('Y-m-d H:i:s');
                    }
                    $row['iccid'] = $device['iccid'];
                    $row['imsi'] = $device['imsi'];
                    $row['msisdn'] = $device['msisdn'];
                    $row['imei'] = $device['imei'];
                    $row['status'] = $device['status'];
                    $row['ratePlan'] = $device['ratePlan'];
                    $row['communicationPlan'] = $device['communicationPlan'];
                    $row['accountId'] = $device['accountId'];
                    $row['operatorCustom1'] = $device['operatorCustom1'];
                    $row['operatorCustom2'] = $device['operatorCustom2'];
                    $row['accountCustom1'] = $device['accountCustom1'];
                    $row['accountCustom2'] = $device['accountCustom2'];
                    $row['customerCustom1'] = $device['customerCustom1'];
                    $row['customerCustom2'] = $device['customerCustom2'];
                    $row['simNotes'] = $device['simNotes'];
                    $row['euiccid'] = $device['euiccid'];
                    $row['deviceID'] = $device['deviceID'];
                    $row['modemID'] = $device['modemID'];
                    $row['globalSimType'] = $device['globalSimType'];

//                dd($row);
                    $con = Connection::updateOrCreate(
                        ['iccid' => $row['iccid']],
                        $row
                    );
                }
            }
        }
        return response('Devices Sync Successfully', 200)
            ->header('Content-Type', 'text/plain');
    }
}
