<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = auth()->user();
        if($user->hasRole('admin') || $user->hasRole('saleagent') || $user->hasRole('superuser')){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'        => 'required|string|max:190',
            'last_name'         => 'required|string|max:190',
            'mobile'            => 'required|min:11|numeric',
            'email'             => 'required|email|unique:users,email,' . request()->user_id,
            'account_phone'     => 'nullable|min:11|numeric',
            'account_email'     => 'nullable|email',
            'name'              => 'required|string|max:190',
            'address'           => 'required|string|max:190',
            'postcode'          => 'required|min:5|max:10',
            'company_number'    => 'required|string|max:190',
            'taxID'             => 'required|string',
            'password'          => 'nullable|min:6|string',
            'notes'             => 'nullable',
        ];
    }

    public function attributes()
    {
        return [
            'mobile' => 'Contact Mobile',
            'email' => 'Contact Email',
            'name' => 'Company Name',
            'address' => 'Company Address',
            'taxID' => 'Tax ID',
        ];
    }
}
