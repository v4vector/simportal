<?php

namespace App\Http\Requests;

//use Cartalyst\Sentinel\Sentinel;
use Illuminate\Foundation\Http\FormRequest;

use Cartalyst\Sentinel\Native\Facades\Sentinel;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'        => 'required|string|max:190',
            'last_name'         => 'nullable|string|max:190',
            'contact_mobile'    => 'required|min:11|numeric',
            'contact_email'     => 'required|e-mail',
            'phone'             => 'nullable|min:11|numeric',
            'email'             => 'required|e-mail|unique:users',
            'name'              => 'required|string|max:190',
            'address'           => 'required|string|max:190',
            'postcode'          => 'required|alpha_num|min:5|max:10',
            'company_number'    => 'required|string|max:190',
            'taxID'             => 'required|alpha_num',
            'notes'             => 'nullable',
        ];
    }
}
