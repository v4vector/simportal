<?php

namespace App\Http\Requests;

//use Cartalyst\Sentinel\Sentinel;
use Illuminate\Foundation\Http\FormRequest;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Support\Facades\Auth;

class StoreClient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        if($user->hasRole('admin') || $user->hasRole('saleagent') || $user->hasRole('superuser')){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'        => 'required|string|max:190',
            'last_name'         => 'required|string|max:190',
            'mobile'            => 'required|min:11|numeric',
            'email'             => 'required|e-mail|unique:users',
            'account_phone'     => 'nullable|min:11|numeric',
            'account_email'     => 'nullable|e-mail',
            'name'              => 'required|string|max:190',
            'address'           => 'required|string|max:190',
            'postcode'          => 'required|min:5|max:10',
            'company_number'    => 'required|string|max:190',
            'taxID'             => 'required|string',
            'password'          => 'required|string|min:6',
            'notes'             => 'nullable',
        ];
    }

    public function attributes()
    {
        return [
            'mobile' => 'Contact Mobile',
            'email' => 'Contact Email',
            'name' => 'Company Name',
            'address' => 'Company Address',
            'taxID' => 'Tax ID',
        ];
    }
}
