<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDeal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_type_id'       => 'required|numeric|min:1',
            'data_type'             => 'required|string',
            'data'                  => 'required|string',
            'network_preferences'   => 'required|min:0|array',
            'sim_volume'            => 'required|integer|min:1',
            'data_volume_id'        => 'required|numeric|min:1',
            'contract_len'          => 'required|integer|min:1',
            'client_application_id' => 'required|numeric|min:1',
            'base_price'            => 'required|numeric|min:0',
            'margin_sales'          => 'required|numeric|min:0',
            'margin_admin'          => 'required|numeric|min:0',
            'client_price'          => 'required|numeric|min:0.1',
        ];
    }
}
