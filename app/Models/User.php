<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute($value)
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function connections()
    {
        return $this->hasMany(Connection::class);
    }

    public function wallet()
    {
        return $this->hasMany(Wallet::class, 'client_id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'user_id');
    }

    public function client_quotes()
    {
        return $this->hasMany(Quote::class, 'client_id');
    }

    public function saleagent_quotes()
    {
        return $this->hasMany(Quote::class, 'user_id');
    }

    public function usage()
    {
        return $this->hasManyThrough(Usage::class, Connection::class);
    }
}
