<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function connections()
    {
        return $this->hasMany(Connection::class);
    }

    public function client()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function usage()
    {
        return $this->hasManyThrough(Usage::class, Connection::class);
    }
}
