<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Company extends Model
{
    protected $guarded = [];

    public function client()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
