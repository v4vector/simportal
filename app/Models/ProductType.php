<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $guarded = [];

    public function getShowNameAttribute()
    {
        return $this->name . ' (' .$this->territory. ')';
    }
}
