<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;
use App\Models\Usage;
use Carbon\Carbon;

class JasperApi
{
    protected $base_uri = [];
    protected $authentication = [];

    public function __construct()
    {
        //        For Tele2
        // $this->base_uri = [
        //     'base_uri' => 'https://restapi3.jasper.com/rws/api/v1/'
        // ];

        // $this->authentication = [
        //     'Authorization' => 'Basic' . base64_encode('broadcastsim:28f93b54-3c03-4295-81ec-187e57d8ed17')
        // ];

//        old connections
        // $this->base_uri = [
        //     'base_uri' => 'https://restapi2.jasper.com/rws/api/v1/'
        // ];
        // $this->authentication = [
        //     'Authorization' => 'Basic ' . base64_encode('watertelecom:91bb65d4-955f-4103-a6e4-9a3f8b9aa7d4')
        // ];
        // new api credentials 
        //$this->base_uri = [
        //     'base_uri' => 'https://restapi2.jasper.com/rws/api/v1/'
        // ];
        // $this->authentication = [
        //     'Authorization' => 'Basic ' . base64_encode('JunaidHabib:c87e056b-f2c2-4e7c-8d9a-6dd0fd880ee9')
        // ];
    }

    public function all_devices()
    {
        $current = Carbon::now()->startOfYear()->format('Y-m-dTH:i:sP');
        $current = urlencode(str_replace('UTC', 'T', $current));
        //        2018-01-01T00%3A00%3A00%2B00%3A00

        //guzzle intergrates APi with code

        $client = new \GuzzleHttp\Client($this->base_uri);
        $response = $client->request('GET', 'devices?modifiedSince=' . $current, [
            'headers' => $this->authentication
        ]);
        //returning all the devices from the API search
        sleep(1);
        if ($response->getStatusCode() != 200) {
            return false;
        } else {
            $devices = [];
            $result = json_decode($response->getBody(), true);
            foreach ($result['devices'] as $dev) {
                //dd($dev);
                array_push($devices, $dev);
            }
            return $devices;
        }
    }
    dd(all_devices);

    //devices details iccid, imsi,imei status etc getting from this function

    public function details($devices)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);

        $devices_details = [];
        $chunks = array_chunk($devices, 4);
        foreach ($chunks as $chunk) {
            sleep(2);
            $promises = [];
            foreach ($chunk as $device) {
                $promises[$device['iccid']] = $client->getAsync('devices/' . $device['iccid'], ['headers' => $this->authentication]);
            }
            $results = \GuzzleHttp\Promise\unwrap($promises); 
            $results = \GuzzleHttp\Promise\settle($promises)->wait();
            foreach ($results as $device) {
                $device = \GuzzleHttp\json_decode($device['value']->getBody());
                array_push($devices_details, $device);
            }
        }
        return $devices_details;
    }

    //getting devices usage from this function 

    public function usages($devices)
    {
        $client = new Client($this->base_uri);
        $promises = [];
        foreach ($devices as $device) {
            $promises[$device->iccid] = $client->getAsync('devices/' . $device->iccid . '/ctdUsages', ['headers' => $this->authentication]);
        }
        $usage_results = \GuzzleHttp\Promise\unwrap($promises);
        $usage_results = \GuzzleHttp\Promise\settle($promises)->wait();
        $usages = collect();
        foreach ($usage_results as $usage) {
            $usg = json_decode($usage['value']->getBody());
            $usages->push($usg);
        }
        return $usages;
    }

    public function one_device_details($iccid)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);
        $response = $client->request('GET', 'devices/' . $iccid, [
            'headers' => $this->authentication
        ]);
        if ($response->getStatusCode() != 200) {
            return false;
        } else {
            return (object)json_decode($response->getBody(), true);
        }
    }

    public function one_usage_details($iccid)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);
        $response = $client->request('GET', 'devices/' . $iccid . '/ctdUsages', [
            'headers' => $this->authentication
        ]);
        if ($response->getStatusCode() != 200) {
            return false;
        } else {
            return (object)json_decode($response->getBody(), true);
        }
    }

    public function change_status($iccid, $status)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);
        $response = $client->put('devices/' . $iccid, [
            'headers' => $this->authentication,
            'form_params' => [
                'status' => $status
            ]
        ]);
        $response = $response->send();
        return $response;
    }

    public function usages_sessions($five_chunk)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);
        $promises = [];

        foreach ($five_chunk as $conn) {
            $promises[$conn->iccid] = $client->getAsync('devices/' . $conn->iccid . '/ctdUsages', ['headers' => $this->authentication]);
        }
        $results = \GuzzleHttp\Promise\unwrap($promises);
        $results = \GuzzleHttp\Promise\settle($promises)->wait();
        return $results;
    }

    public function get_session_details($iccid)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);
        $response = $client->get('devices/' . $iccid . '/sessionInfo', [
            'headers' => $this->authentication
        ]);
        $result = \GuzzleHttp\json_decode($response->getBody(), true);
       dd($result);
        return $result;
    }

    public function chunk_details($chunk)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);

        $devices_details = [];
        foreach ($chunk as $device) {
            $promises[$device['iccid']] = $client->getAsync('devices/' . $device['iccid'], ['headers' => $this->authentication]);
        }
        $results = \GuzzleHttp\Promise\unwrap($promises);
        $results = \GuzzleHttp\Promise\settle($promises)->wait();
        foreach ($results as $device) {
            $device = \GuzzleHttp\json_decode($device['value']->getBody());
            array_push($devices_details, $device);
        }
        return $devices_details;
    }
}
