<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deal extends Model
{
    protected $guarded = [];
    protected $dates = ['installment_end'];

    public function quote()
    {
        return $this->belongsTo(Quote::class, 'quote_id');
    }

    public function product_type()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id');
    }

    public function client_application()
    {
        return $this->belongsTo(ClientApplication::class, 'client_application_id');
    }

    public function volume()
    {
        return $this->belongsTo(DataVolume::class, 'data_volume_id');
    }

    public function installments()
    {
        return $this->hasMany(Installment::class);
    }
}
