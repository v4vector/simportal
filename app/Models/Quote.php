<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $guarded = [];
    protected $dates = ['approved_date'];

    public function client()
    {
        return $this->belongsTo(User::class, 'client_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function deals()
    {
        return $this->hasMany(Deal::class, 'quote_id');
    }

    public function installments()
    {
        return $this->hasManyThrough(Installment::class, Deal::class);
    }
}
