<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Connection;

class Usage extends Model
{
    protected $guarded = [];

    public function connection()
    {
        return $this->belongsTo(Connection::class, 'connection_id');
    }
}
