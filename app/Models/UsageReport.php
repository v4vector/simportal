<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;
use App\Models\Usage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Collection;

class UsageReport
{
    protected $monthly_start = '';
    protected $daily_start = '';

    public function __construct()
    {
        $this->daily_start = Carbon::now();
        $this->monthly_start = Carbon::now()->subYear();
    }

    public function day_wise($client_id)
    {
        $day_wise = Usage::select(
            DB::raw('sum(daily_usage) as daily_usage'),
            DB::raw('sum(daily_sessions) as daily_sessions'),
            DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as days")
        )
            ->whereHas('connection', function ($q) use ($client_id) {
                $q->where('user_id', $client_id);
            })
            ->where('created_at', '>=', $this->daily_start)
            ->groupBy('days')
            ->get()->map(function ($day) {
                $day->daily_usage = round($day->daily_usage / 1048576, 2);
                return $day;
            });
////
        return $day_wise;
    }

    public function month_wise($client_id)
    {
        $month_wise = Usage::select(
            DB::raw('sum(daily_usage) as daily_usage'),
            DB::raw('sum(daily_sessions) as daily_sessions'),
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months")
        )
            ->whereHas('connection', function ($q) use ($client_id) {
                $q->where('user_id', $client_id);
            })
            ->where('created_at', '>=', $this->monthly_start)
            ->groupBy('months')
            ->get()->map(function ($month) {
                $month->daily_usage = round($month->daily_usage / 1048576, 2);
                return $month;
            });

        return $month_wise;
    }

    public function top_metrics($id)
    {
        $top_metrics = Usage::
        with('connection')
            ->whereHas('connection', function ($q) use ($id) {
                return $q->where('user_id', $id);
            })
            ->whereMonth('created_at', Carbon::now()->format('m'))
            ->whereYear('created_at', Carbon::now()->format('Y'))
            ->get();
        $top_metrics = $top_metrics->sortByDesc('ctdDataUsage')->groupBy(function ($q) {
            return $q->connection_id;
        });

        return $top_metrics;
    }

    public function one_client($client_id)
    {
        $top_metrics = $this->top_metrics($client_id);
        $day_data = $this->day_wise($client_id);
        $month_data = $this->month_wise($client_id);

        return [
            'top_metrics' => $top_metrics,
            'day_data' => $day_data,
            'month_data' => $month_data,
        ];
    }

//    Search Functions Goes Here

    public function day_wise_search($start = null, $end = null)
    {
        if($start && $end){
            $start = Carbon::parse($start);
            $end = Carbon::parse($end);
        } else {
            $start = $this->daily_start;
            $end = Carbon::now();
        }

        $day_wise = Usage::select(
            DB::raw('sum(daily_usage) as daily_usage'),
            DB::raw('sum(daily_sessions) as daily_sessions'),
            DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as days")
        )
            ->when(Auth::user()->hasRole('client'), function ($q) {
                $q->whereHas('connection', function ($a) {
                    return $a->where('user_id', Auth::user()->id);
                });
            })
            ->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)
            ->groupBy('days')
            ->get()->map(function ($day) {
                $day->daily_usage = round($day->daily_usage / 1048576, 2);
                return $day;
            });

        return $day_wise;
    }

    public function month_wise_search($start= null, $end= null)
    {
        if($start && $end){
            $start = Carbon::parse($start);
            $end = Carbon::parse($end);
        } else {
            $start = $this->monthly_start;
            $end = Carbon::now();
        }

        $month_wise = Usage::select(
            DB::raw('sum(daily_usage) as daily_usage'),
            DB::raw('sum(daily_sessions) as daily_sessions'),
            DB::raw("DATE_FORMAT(created_at,'%M %Y') as months")
        )
            ->when(Auth::user()->hasRole('client'), function ($q) {
                $q->whereHas('connection', function ($a) {
                    return $a->where('user_id', Auth::user()->id);
                });
            })
            ->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)
            ->groupBy('months')
            ->orderBy('created_at', 'asc')
            ->get()->map(function ($month) {
                $month->daily_usage = round($month->daily_usage / 1048576, 2);
                return $month;
            });

        return $month_wise;
    }

    public function top_metric_search($start= null, $end= null)
    {
        if($start && $end){
            $start = Carbon::parse($start);
            $end = Carbon::parse($end);
        } else {
            $start = Carbon::now()->subMonth();
            $end = Carbon::now();
        }

        $top_metrics = Usage::
        select(
            DB::raw('sum(daily_usage) as daily_usage'),
            DB::raw('sum(daily_sessions) as daily_sessions'),
            DB::raw("connection_id")
        )
        ->with('connection')
            ->when(Auth::user()->hasRole('client'), function ($q) {
                $q->whereHas('connection', function ($a) {
                    return $a->where('user_id', Auth::user()->id);
                });
            })
            ->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)
            ->groupBy('connection_id')
            ->orderBy('daily_usage', 'desc')
            ->get();

        return $top_metrics;
    }

    public function search_connections()
    {
        $cons = Connection::
            when(Auth::user()->hasRole('client'), function ($q) {
                    return $q->where('user_id', Auth::user()->id);
            })
        ->get();
        $connection = [
            'Online' => 0,
            'Offline' => 0
        ];
        if($cons){
            foreach ($cons as $con){
                $con->status == 'ACTIVATED' ? $connection['Online']++ : $connection['Offline']++;
            }
        }
        return $connection;
    }

    public function default_dates()
    {
        return [
            'start' => $this->daily_start->format('d M Y'),
            'end' => Carbon::now()->format('d M Y')
        ];
    }

    public function search_all($start = null, $end = null)
    {
        $day_data = $this->day_wise_search($start, $end);
        $month_data = $this->month_wise_search($start, $end);
        $top_metrics = $this->top_metric_search($start, $end);

        $arr = [
            'top_metrics' => $top_metrics,
            'day_data' => $day_data,
            'month_data' => $month_data
        ];
        return $arr;
    }
}
