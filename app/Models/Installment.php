<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Installment extends Model
{
    protected $guarded = [];
    protected $dates = ['date'];

    public function deal()
    {
        return $this->belongsTo(Installment::class, 'deal_id');
    }

}
