<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class Jasper
{
    protected $base_uri = [];
    protected $authentication = [];

    public function __construct()
    {
        $this->base_uri = [
            'base_uri' => 'https://restapi3.jasper.com/rws/api/v1/'
        ];
        $this->authentication = [
            'Authorization' => 'Basic ' . base64_encode('broadcastsim:28f93b54-3c03-4295-81ec-187e57d8ed17')
        ];
    }

    public function all_devices()
    {
        $current = Carbon::now()->startOfYear()->format('Y-m-dTH:i:sP');
        $current = urlencode(str_replace('UTC', 'T', $current));
        //        2018-01-01T00%3A00%3A00%2B00%3A00

        $client = new \GuzzleHttp\Client($this->base_uri);
        $response = $client->request('GET', 'devices?modifiedSince=' . $current, [
            'headers' => $this->authentication
        ]);
        if ($response->getStatusCode() != 200) {
            return false;
        } else {
            $devices = Collection::make();
            $result = json_decode($response->getBody(), true);
            foreach ($result['devices'] as $dev) {
                $dev = (object)$dev;
//                dd($dev);
                $devices->push($dev);
            }
            return $devices;
            dd($devices);
        }
    }

    public function details($devices)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);
        $promises = [];
        foreach ($devices as $device) {
            $promises[$device->iccid] = $client->getAsync('devices/' . $device->iccid, ['headers' => $this->authentication]);
        }
        $results = \GuzzleHttp\Promise\unwrap($promises);
        $results = \GuzzleHttp\Promise\settle($promises)->wait();

        $devices_details = collect();
        foreach ($results as $result) {
            $dev = json_decode($result['value']->getBody());
//            dd($dev);

//            Add email check here
            $devices_details->push($dev);
        }
        if (Auth::user()->hasRole('client')){
            return $devices_details->where('accountCustom2', Auth::user()->email);
        } else {
            return $devices_details;
        }
    }

    public function usages($devices)
    {
        $client = new Client($this->base_uri);
        $promises = [];
        foreach ($devices as $device) {
            $promises[$device->iccid] = $client->getAsync('devices/' . $device->iccid . '/ctdUsages', ['headers' => $this->authentication]);
        }
        $usage_results = \GuzzleHttp\Promise\unwrap($promises);
        $usage_results = \GuzzleHttp\Promise\settle($promises)->wait();
        $usages = collect();
        foreach ($usage_results as $usage) {
            $usg = json_decode($usage['value']->getBody());
            $usages->push($usg);
        }
        return $usages;
    }

    public function one_device_details($iccid)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);
        $response = $client->request('GET', 'devices/' . $iccid, [
            'headers' => $this->authentication
        ]);
        if ($response->getStatusCode() != 200) {
            return false;
        } else {
            return (object)json_decode($response->getBody(), true);
        }
    }

    public function one_usage_details($iccid)
    {
        $client = new \GuzzleHttp\Client($this->base_uri);
        $response = $client->request('GET', 'devices/' . $iccid . '/ctdUsages', [
            'headers' => $this->authentication
        ]);
        if ($response->getStatusCode() != 200) {
            return false;
        } else {
            return (object)json_decode($response->getBody(), true);
        }
    }
}
