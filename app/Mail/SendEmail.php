<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Model\User;
Use DB;
class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $pass;
    public function __construct($password)
    {
        $this->$pass=$password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $e_pass=$this->pass;
        return.$this->view('mail.sendmail',compact("e_pass"));



        // $user_email=user::where('id','$id')->get();
        // dd($user_email);
        //  return $this->from('nfayyaz950@gmail.com')
        //         ->view('emails.orders.shipped');
       
    }
}
